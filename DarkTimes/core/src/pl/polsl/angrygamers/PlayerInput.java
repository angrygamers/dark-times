package pl.polsl.angrygamers;

import box2dLight.RayHandler;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;
import pl.polsl.angrygamers.enums.Direction;
import pl.polsl.angrygamers.objects.gameObjects.Enemy;
import pl.polsl.angrygamers.objects.gameObjects.GameObject;
import pl.polsl.angrygamers.objects.gameObjects.Player;
import pl.polsl.angrygamers.shaders.MistShader;
import pl.polsl.angrygamers.utils.InventoryManager;
import pl.polsl.angrygamers.utils.WorldCamera;
import pl.polsl.angrygamers.utils.dialogues.DialogueManager;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Player input.
 */
public class PlayerInput implements InputProcessor {

    /**
     * The constant PLAYER_INPUT_SESSION.
     */
    public static final PlayerInput PLAYER_INPUT_SESSION = new PlayerInput();
    private MistShader mistShader;
    private Player player;
    private RayHandler rayHandler;
    private Enemy enemy;
    private DarkTimesApplication game;
    private Camera camera;

    private List<GameObject> gameObjects = new ArrayList<>();
    private final boolean showingCollisionLayer = false;


    @Override
    public boolean mouseMoved(final int screenX, final int screenY) {
        final Vector3 mousePosition = new Vector3(screenX, screenY, 0);
        WorldCamera.getInstance().getCamera().unproject(mousePosition);
        float rotation = (float) Math.atan2(mousePosition.y - player.getSprite().getY(), mousePosition.x - player.getSprite().getX());
        rotation = (float) Math.toDegrees(rotation);
        if (rotation < 0) rotation += 360;
        player.getSprite().setRotation(rotation);
        return false;
    }

    @Override
    public boolean keyDown(final int keycode) {
        if (keycode == Input.Keys.ENTER) {
            if (DialogueManager.getInstance().isReady()) {
                DialogueManager.getInstance().closeCurrentDialogue();
            }
        }

        if(keycode == Input.Keys.E) {
            System.out.println(String.format(("%.2f, %.2f"), player.getPosition().x, player.getPosition().y));
        }

        if (keycode == Input.Keys.I) {
            InventoryManager.getInstance().open();
        }

        if (keycode == Input.Keys.W) {
            if (DialogueManager.getInstance().isReady()) {
                DialogueManager.getInstance().selectPrevOption();
            } else {
                player.setMoving(true);
                if (player.getDirection() == Direction.LEFT || player.getDirection() == Direction.UP_LEFT) {
                    player.setDirection(Direction.UP_LEFT);
                } else if (player.getDirection() == Direction.RIGHT || player.getDirection() == Direction.UP_RIGHT) {
                    player.setDirection(Direction.UP_RIGHT);
                } else {
                    player.setDirection(Direction.UP);
                }
            }
        }

        if (keycode == Input.Keys.S) {
            if (DialogueManager.getInstance().isReady()) {
                DialogueManager.getInstance().selectNextOption();
            } else {
                player.setMoving(true);
                if (player.getDirection() == Direction.LEFT || player.getDirection() == Direction.DOWN_LEFT) {
                    player.setDirection(Direction.DOWN_LEFT);
                } else if (player.getDirection() == Direction.RIGHT || player.getDirection() == Direction.DOWN_RIGHT) {
                    player.setDirection(Direction.DOWN_RIGHT);
                } else {
                    player.setDirection(Direction.DOWN);
                }
            }
        }

        if (keycode == Input.Keys.A) {
            if (!DialogueManager.getInstance().isReady()) {
                player.setMoving(true);
                if (player.getDirection() == Direction.UP || player.getDirection() == Direction.UP_LEFT) {
                    player.setDirection(Direction.UP_LEFT);
                } else if (player.getDirection() == Direction.DOWN || player.getDirection() == Direction.DOWN_LEFT) {
                    player.setDirection(Direction.DOWN_LEFT);
                } else {
                    player.setDirection(Direction.LEFT);
                }
            }
        }

        if (keycode == Input.Keys.D) {
            if (!DialogueManager.getInstance().isReady()) {
                player.setMoving(true);
                if (player.getDirection() == Direction.UP || player.getDirection() == Direction.UP_RIGHT) {
                    player.setDirection(Direction.UP_RIGHT);
                } else if (player.getDirection() == Direction.DOWN || player.getDirection() == Direction.DOWN_RIGHT) {
                    player.setDirection(Direction.DOWN_RIGHT);
                } else {
                    player.setDirection(Direction.RIGHT);
                }
            }
        }

//        if (keycode == Input.Keys.C) {
//            showingCollisionLayer = !showingCollisionLayer;
//        }
        return false;
    }

    @Override
    public boolean keyUp(final int keycode) {
        if (keycode == Input.Keys.W) {
            if (player.getDirection() == Direction.UP_LEFT) {
                player.setDirection(Direction.LEFT);
            } else if (player.getDirection() == Direction.UP_RIGHT) {
                player.setDirection(Direction.RIGHT);
            } else {
                player.setMoving(false);
                player.setDirection(null);
            }
        }

        if (keycode == Input.Keys.S) {
            player.setMoving(true);
            if (player.getDirection() == Direction.DOWN_LEFT) {
                player.setDirection(Direction.LEFT);
            } else if (player.getDirection() == Direction.DOWN_RIGHT) {
                player.setDirection(Direction.RIGHT);
            } else {
                player.setMoving(false);
                player.setDirection(null);
            }
        }

        if (keycode == Input.Keys.A) {
            player.setMoving(true);
            if (player.getDirection() == Direction.UP_LEFT) {
                player.setDirection(Direction.UP);
            } else if (player.getDirection() == Direction.DOWN_LEFT) {
                player.setDirection(Direction.DOWN);
            } else {
                player.setMoving(false);
                player.setDirection(null);
            }
        }

        if (keycode == Input.Keys.D) {
            player.setMoving(true);
            if (player.getDirection() == Direction.UP_RIGHT) {
                player.setDirection(Direction.UP);
            } else if (player.getDirection() == Direction.DOWN_RIGHT) {
                player.setDirection(Direction.DOWN);
            } else {
                player.setMoving(false);
                player.setDirection(null);
            }
        }

        return false;
    }

    @Override
    public boolean keyTyped(final char character) {
        return false;
    }

    private boolean collides(final GameObject obj, final float x, final float y) {
        return obj.getSprite().getBoundingRectangle().contains(x, y);
    }

    @Override
    public boolean touchDown(final int screenX, final int screenY, final int pointer, final int button) {
        final Vector3 worldCoords = WorldCamera.getInstance().getCamera().unproject(new Vector3(screenX, screenY, 0));

        for (final GameObject obj : gameObjects) {
            if (collides(obj, worldCoords.x, worldCoords.y)) {
                System.out.println("Object touched");
                obj.setTouched(true);
            }
        }
        return false;
    }

    @Override
    public boolean touchUp(final int screenX, final int screenY, final int pointer, final int button) {
        return false;
    }

    @Override
    public boolean touchDragged(final int screenX, final int screenY, final int pointer) {
        return false;
    }

    @Override
    public boolean scrolled(final int amount) {
        return false;
    }

    /**
     * Sets mist shader.
     *
     * @param mistShader the mist shader
     */
    void setMistShader(final MistShader mistShader) {
        this.mistShader = mistShader;
    }

    /**
     * Sets player.
     *
     * @param player the player
     */
    public void setPlayer(final Player player) {
        this.player = player;
    }

    /**
     * Sets enemy.
     *
     * @param enemy the enemy
     */
    public void setEnemy(final Enemy enemy) {
        this.enemy = enemy;
    }

    /**
     * Sets game.
     *
     * @param game the game
     */
    public void setGame(final DarkTimesApplication game) {
        this.game = game;
    }

    /**
     * Sets camera.
     *
     * @param camera the camera
     */
    public void setCamera(final Camera camera) {
        this.camera = camera;
    }

    /**
     * Sets game objects.
     *
     * @param gameObjects the game objects
     */
    public void setGameObjects(final List<GameObject> gameObjects) {
        this.gameObjects = gameObjects;
    }

    /**
     * Sets ray handler.
     *
     * @param rayHandler the ray handler
     */
    public void setRayHandler(final RayHandler rayHandler) {
        this.rayHandler = rayHandler;
    }

    /**
     * Gets game objects.
     *
     * @return the game objects
     */
    public List<GameObject> getGameObjects() {
        return gameObjects;
    }

    /**
     * Is showing collision layer boolean.
     *
     * @return the boolean
     */
    public boolean isShowingCollisionLayer() {
        return showingCollisionLayer;
    }
}
