package pl.polsl.angrygamers.utils;

/**
 * The type Coordinate.
 */
public class Coordinate {
    private float x;
    private float y;

    /**
     * Instantiates a new Coordinate.
     *
     * @param x the x
     * @param y the y
     */
    public Coordinate(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Gets x.
     *
     * @return the x
     */
    public float getX() {
        return x;
    }

    /**
     * Sets x.
     *
     * @param x the x
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * Gets y.
     *
     * @return the y
     */
    public float getY() {
        return y;
    }

    /**
     * Sets y.
     *
     * @param y the y
     */
    public void setY(float y) {
        this.y = y;
    }
}
