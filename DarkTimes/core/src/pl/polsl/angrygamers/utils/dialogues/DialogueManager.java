package pl.polsl.angrygamers.utils.dialogues;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * The type Dialogue manager.
 */
public class DialogueManager {

    private static final DialogueManager CURRENT_DIALOGUE_MANAGER = new DialogueManager();

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static DialogueManager getInstance() {
        return CURRENT_DIALOGUE_MANAGER;
    }

    private boolean ready = false;

    private Dialogue currentlyDisplayedDialogue;
    private int currentlySelectedOption;
    private Label textLabel;
    private List<Label> optionLabels;

    private final FreeTypeFontGenerator generator;
    private Table dialogueTable;
    private final Stage stage;
    private final Skin tableSkin;
    private final Pixmap bgPixmap;
    private final TextureRegionDrawable backgroundDrawable;

    private final LabelStyle classicLabelStyle;
    private final LabelStyle specialLabelStyle;
    private final LabelStyle pickedLabelStyle;
    private final LabelStyle pickedSpecialLabelStyle;

    private float deltaSum;
    private final float progressAnimationTime = 0.0013f;
    private int currentLength = 0;

    private DialogueManager() {
        bgPixmap = new Pixmap(1, 1, Pixmap.Format.RGB565);
        bgPixmap.setColor(Color.SLATE);
        bgPixmap.fill();
        backgroundDrawable = new TextureRegionDrawable(new TextureRegion(new Texture(bgPixmap)));

        this.tableSkin = new Skin();
        generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/prstart.ttf"));
        final FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 20;
        parameter.color = Color.WHITE;
        BitmapFont font = generator.generateFont(parameter);
        tableSkin.add("font", font);
        tableSkin.add("background", backgroundDrawable);
        classicLabelStyle = new LabelStyle(font, Color.WHITE);
        specialLabelStyle = new LabelStyle(font, Color.TEAL);
        parameter.borderColor = Color.FIREBRICK;
        parameter.borderWidth = 1;
        font = generator.generateFont(parameter);
        tableSkin.add("fontPicked", font);
        pickedLabelStyle = new LabelStyle(font, Color.WHITE);
        pickedSpecialLabelStyle = new LabelStyle(font, Color.TEAL);
        tableSkin.add("default", classicLabelStyle);

        stage = new Stage();
    }

    /**
     * Display dialogue.
     *
     * @param dialogue the dialogue
     */
    public void displayDialogue(final Dialogue dialogue) {
        currentlyDisplayedDialogue = dialogue;
        updateTable();
    }

    /**
     * Display dialogue.
     *
     * @param text the text
     */
    public void displayDialogue(String text) {
        displayDialogue(new Dialogue(text));
    }

    private void updateTable() {
        dialogueTable = new Table();
        dialogueTable.setWidth(Gdx.graphics.getWidth());

        dialogueTable.setBackground(backgroundDrawable);
        dialogueTable.setHeight(40.0f + currentlyDisplayedDialogue.getOptions().size() * 40.0f);
        dialogueTable.defaults().pad(8.0f).padLeft(10.0f).fillX().expandX();
        textLabel = new Label(currentlyDisplayedDialogue.getText(), tableSkin, "font", Color.LIGHT_GRAY);
        textLabel.setAlignment(Align.left);
        dialogueTable.add(textLabel);

        optionLabels = new ArrayList<>();
        for (final DialogueOption option : currentlyDisplayedDialogue.getOptions()) {
            dialogueTable.row().padLeft(25.0f);

            if (option.isSpecial()) {
                final Label specialLabel = new Label(option.getText(), specialLabelStyle);
                specialLabel.setAlignment(Align.left);

                optionLabels.add(specialLabel);
                dialogueTable.add(specialLabel);
            } else {
                final Label optionLabel = new Label(option.getText(), classicLabelStyle);
                optionLabel.setAlignment(Align.left);

                optionLabels.add(optionLabel);
                dialogueTable.add(optionLabel);
            }
        }

        stage.addActor(dialogueTable);
    }

    /**
     * Close current dialogue.
     *
     * @return the currently selected option
     */
    public int closeCurrentDialogue() {
        ready = false;
        this.currentlyDisplayedDialogue = null;
        stage.clear();
        textLabel = null;
        optionLabels = null;
        currentLength = 0;

        return currentlySelectedOption;
    }

    /**
     * Select next option.
     */
    public void selectNextOption() {
        if (optionLabels != null && !optionLabels.isEmpty()) {
            if (optionLabels.get(currentlySelectedOption).getStyle().equals(pickedLabelStyle)) {
                optionLabels.get(currentlySelectedOption).setStyle(classicLabelStyle);
            } else {
                optionLabels.get(currentlySelectedOption).setStyle(specialLabelStyle);
            }

            currentlySelectedOption++;
            if (currentlySelectedOption >= currentlyDisplayedDialogue.getOptions().size()) {
                currentlySelectedOption = 0;
            }
            if (optionLabels.get(currentlySelectedOption).getStyle().equals(classicLabelStyle)) {
                optionLabels.get(currentlySelectedOption).setStyle(pickedLabelStyle);
            } else {
                optionLabels.get(currentlySelectedOption).setStyle(pickedSpecialLabelStyle);
            }
        }
    }

    /**
     * Select prev option.
     */
    public void selectPrevOption() {
        if (optionLabels != null && !optionLabels.isEmpty()) {
            if (optionLabels.get(currentlySelectedOption).getStyle().equals(pickedLabelStyle)) {
                optionLabels.get(currentlySelectedOption).setStyle(classicLabelStyle);
            } else {
                optionLabels.get(currentlySelectedOption).setStyle(specialLabelStyle);
            }

            currentlySelectedOption--;
            if (currentlySelectedOption < 0) {
                currentlySelectedOption = currentlyDisplayedDialogue.getOptions().size() - 1;
            }
            if (optionLabels.get(currentlySelectedOption).getStyle().equals(classicLabelStyle)) {
                optionLabels.get(currentlySelectedOption).setStyle(pickedLabelStyle);
            } else {
                optionLabels.get(currentlySelectedOption).setStyle(pickedSpecialLabelStyle);
            }
        }
    }

    /**
     * Gets currently selected option.
     *
     * @return the currently selected option
     */
    public int getCurrentlySelectedOption() {
        return currentlySelectedOption;
    }

    /**
     * Gets dialogue stage.
     *
     * @return the dialogue stage
     */
    public Stage getDialogueStage() {
        return stage;
    }

    /**
     * Gets currently displayed dialogue.
     *
     * @return the currently displayed dialogue
     */
    public Optional<Dialogue> getCurrentlyDisplayedDialogue() {
        return Optional.ofNullable(currentlyDisplayedDialogue);
    }

    /**
     * Resize.
     *
     * @param width  the width
     * @param height the height
     */
    public void resize(final int width, final int height) {

    }

    /**
     * Gets dialogue table.
     *
     * @return the dialogue table
     */
    public Table getDialogueTable() {
        return dialogueTable;
    }

    /**
     * Is ready boolean.
     *
     * @return the boolean
     */
    public boolean isReady() {
        return ready;
    }

    /**
     * Is displaying boolean.
     *
     * @return the boolean
     */
    public boolean isDisplaying() {
        return this.currentlyDisplayedDialogue != null;
    }

    /**
     * Draw.
     *
     * @param deltaTime the delta time
     */
    public void draw(final float deltaTime) {
        deltaSum += deltaTime;

        final List<DialogueOption> options = currentlyDisplayedDialogue.getOptions();
        final String completeText = currentlyDisplayedDialogue.getText();
        final int textLength = completeText.length();

        for (final Label optionLabel : optionLabels) {
            optionLabel.setText(null);
        }

        if (currentLength < textLength && deltaSum > progressAnimationTime) {
            currentLength++;
            textLabel.setText(completeText.substring(0, currentLength));
            deltaSum = 0;
        }

        if (currentLength >= textLength) {
            textLabel.setText(completeText);
            for (int i = 0; i < optionLabels.size(); i++) {
                optionLabels.get(i).setText(options.get(i).getText());
                if (i == currentlySelectedOption) {
                    if (optionLabels.get(i).getStyle().equals(classicLabelStyle) || optionLabels.get(i).getStyle().equals(pickedLabelStyle)) {
                        optionLabels.get(i).setStyle(pickedLabelStyle);
                    } else {
                        optionLabels.get(i).setStyle(pickedSpecialLabelStyle);
                    }
                }
            }
            ready = true;
        }

        getDialogueTable().setPosition(0, 0);
        getDialogueStage().draw();
    }
}
