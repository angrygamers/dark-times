package pl.polsl.angrygamers.utils.dialogues;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Dialogue.
 */
public class Dialogue {

    private final String text;
    private final List<DialogueOption> options;
    private final DialogueType type;

    /**
     * Instantiates a new Dialogue.
     *
     * @param text    the text
     * @param options the options
     */
    public Dialogue(final String text, final List<DialogueOption> options) {
        this.text = text;
        this.options = options;
        this.type = DialogueType.INTERACTIVE;
    }

    /**
     * Instantiates a new Dialogue.
     *
     * @param text the text
     */
    public Dialogue(final String text) {
        this.text = text;
        this.options = new ArrayList<>();
        this.type = DialogueType.INFORMATIONAL;
    }

    /**
     * Gets dialogue text.
     *
     * @return the text
     */
    String getText() {
        return text;
    }

    /**
     * Gets dialogue options.
     *
     * @return the options
     */
    List<DialogueOption> getOptions() {
        return options;
    }

    /**
     * Gets dialogue type.
     *
     * @return the type
     */
    public DialogueType getType() {
        return type;
    }
}
