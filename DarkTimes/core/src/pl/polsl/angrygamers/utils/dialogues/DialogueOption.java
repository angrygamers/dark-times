package pl.polsl.angrygamers.utils.dialogues;

/**
 * The type Dialogue option.
 */
public class DialogueOption {

    private final String text;
    private final Boolean special;

    /**
     * Instantiates a new Dialogue option.
     *
     * @param text    the text
     * @param special the special
     */
    private DialogueOption(final String text, final Boolean special) {
        this.text = text;
        this.special = special;
    }

    /**
     * Instantiates a new Dialogue option.
     *
     * @param text the text
     */
    public DialogueOption(final String text) {
        this(text, false);
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * Is special dialogue.
     *
     * @return the boolean
     */
    public Boolean isSpecial() {
        return special;
    }
}
