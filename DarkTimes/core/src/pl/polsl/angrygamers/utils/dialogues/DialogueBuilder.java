package pl.polsl.angrygamers.utils.dialogues;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The type Dialogue builder.
 */
public class DialogueBuilder {

    private final Dialogue dialogue;
    private OptionSelectedListener optionSelectedListener;

    /**
     * Instantiates a new Dialogue builder.
     *
     * @param text    the text
     * @param answers the answers
     */
    public DialogueBuilder(String text, String... answers) {
        this(new Dialogue(text, Arrays.stream(answers)
                .map(DialogueOption::new).collect(Collectors.toList())));
    }

    private DialogueBuilder(Dialogue dialogue) {
        this.dialogue = dialogue;
    }

    /**
     * Gets dialogue.
     *
     * @return the dialogue
     */
    public Dialogue getDialogue() {
        return dialogue;
    }

    /**
     * Gets dialogue builder on selected.
     *
     * @param optionSelectedListener the option selected listener
     * @return the dialogue builder
     */
    public DialogueBuilder onSelected(OptionSelectedListener optionSelectedListener) {
        this.optionSelectedListener = optionSelectedListener;
        return this;
    }

    /**
     * Gets dialogue builder by selected option.
     *
     * @param option the option
     * @return the dialogue builder
     */
    public DialogueBuilder optionSelected(int option) {
        final OptionSelectedListener optionSelectedListener = this.optionSelectedListener;
        if(optionSelectedListener != null) {
            return optionSelectedListener.optionSelected(option);
        }
        return null;
    }

    /**
     * The selected option listener interface.
     */
    public interface OptionSelectedListener {
        /**
         * Option selected dialogue builder.
         *
         * @param option the option
         * @return the dialogue builder
         */
        DialogueBuilder optionSelected(int option);
    }
}
