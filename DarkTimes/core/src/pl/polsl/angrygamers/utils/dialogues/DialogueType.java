package pl.polsl.angrygamers.utils.dialogues;

/**
 * The enum Dialogue type.
 */
public enum DialogueType {

    /**
     * Interactive dialogue type.
     */
    INTERACTIVE,
    /**
     * Informational dialogue type.
     */
    INFORMATIONAL

}
