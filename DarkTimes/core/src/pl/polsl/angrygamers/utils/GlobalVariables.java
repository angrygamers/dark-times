package pl.polsl.angrygamers.utils;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.objects.Inventory;
import pl.polsl.angrygamers.objects.gameObjects.Player;
import pl.polsl.angrygamers.shaders.MistShader;
import pl.polsl.angrygamers.shaders.MistShaderImpl;

import static pl.polsl.angrygamers.PlayerInput.PLAYER_INPUT_SESSION;

/**
 * The type Global variables.
 */
public class GlobalVariables {

    /**
     * The constant SPRITE_BATCH.
     */
// general
    public static final SpriteBatch SPRITE_BATCH = new SpriteBatch(1000);

    // player speed
    private static final Float PLAYER_SPEED = 4f;
    /**
     * The constant DIRECTION_CHANGE_INTERVAL.
     */
    public static final Integer DIRECTION_CHANGE_INTERVAL = 10;
    /**
     * The constant PLAYER.
     */
    public static final Player PLAYER = new Player(new Vector2(452, 3576),
        PLAYER_SPEED, new Size(90f, 66.24f), new TextureData("textures/flashlight_walk.png", 4, 5, 0.30f));

    /**
     * The constant INVENTORY.
     */
    public static final Inventory INVENTORY = new Inventory();
    /**
     * The constant MIST_SHADER.
     */
// active night
    public static MistShader MIST_SHADER = new MistShaderImpl();

    /**
     * The Input multiplexer.
     */
    static InputMultiplexer INPUT_MULTIPLEXER = new InputMultiplexer(PLAYER_INPUT_SESSION);

}
