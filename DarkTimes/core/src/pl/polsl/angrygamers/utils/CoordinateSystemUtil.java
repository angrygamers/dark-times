package pl.polsl.angrygamers.utils;

import com.badlogic.gdx.Gdx;

/**
 * The type Coordinate system util.
 */
public class CoordinateSystemUtil {

    /**
     * From touch to normalized ortho coordinate.
     *
     * @param x the x
     * @param y the y
     * @return the coordinate
     */
    public static Coordinate fromTouchToNormalizedOrtho(float x, float y) {
        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        float normalizedX = x / width;
        float normalizedY = (y / height * -1.0f) + 1.0f;

        return new Coordinate(normalizedX, normalizedY);
    }
}
