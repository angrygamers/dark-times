package pl.polsl.angrygamers.utils;

import com.badlogic.gdx.math.Vector2;

import java.util.Random;

/**
 * The type Vector utils.
 */
public class VectorUtils {
    /**
     * Gets random vector in distance.
     *
     * @param basePosition the base position
     * @param distance     the distance
     * @return the random vector in distance
     */
    public static Vector2 getRandomVectorInDistance(Vector2 basePosition, float distance) {
        final Random random = new Random();
        float x = random.nextFloat() * distance * 2 - distance;
        float y = (float) Math.sqrt(distance * distance - x * x);
        return basePosition.cpy().add(x, y);
    }
}
