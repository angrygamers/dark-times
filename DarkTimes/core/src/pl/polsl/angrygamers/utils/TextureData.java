package pl.polsl.angrygamers.utils;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * The type Texture data.
 */
public class TextureData {
    private String texturePath;
    private Integer cols;
    private Integer rows;
    private Float frameRate;
    private Integer frameStart;
    private Integer frameEnd;

    /**
     * Get frames texture region [ ].
     *
     * @param texture the texture
     * @param data    the data
     * @return the texture region [ ]
     */
    public static TextureRegion[] getFrames(Texture texture, TextureData data) {
        final TextureRegion[][] tmp = TextureRegion.split(texture, texture.getWidth()/data.getCols(),
                texture.getHeight() / data.getRows());

        int startIndex = data.frameStart == null ? 0 : data.frameStart;
        int endIndex = data.frameEnd == null ? data.getFramesCount() - 1 : data.frameEnd;

        final TextureRegion[] frames = new TextureRegion[endIndex - startIndex + 1];

        int index = 0;

        for (int i = 0; i < data.getRows(); i++) {
            for (int j = 0; j < data.getCols(); j++) {
                if(index >= startIndex && index <= endIndex) {
                    frames[index - startIndex] = tmp[i][j];
                }
                index++;
            }
        }

        return frames;
    }

    /**
     * Instantiates a new Texture data.
     */
    public TextureData() {
    }

    /**
     * Instantiates a new Texture data.
     *
     * @param texturePath the texture path
     * @param cols        the cols
     * @param rows        the rows
     * @param frameRate   the frame rate
     */
    public TextureData(String texturePath, Integer cols, Integer rows, Float frameRate) {
        this.texturePath = texturePath;
        this.cols = cols;
        this.rows = rows;
        this.frameRate = frameRate;
    }

    /**
     * Gets texture path.
     *
     * @return the texture path
     */
    public String getTexturePath() {
        return texturePath;
    }

    /**
     * Sets texture path.
     *
     * @param texturePath the texture path
     */
    public void setTexturePath(String texturePath) {
        this.texturePath = texturePath;
    }

    /**
     * Gets cols.
     *
     * @return the cols
     */
    public Integer getCols() {
        return cols;
    }

    /**
     * Sets cols.
     *
     * @param cols the cols
     */
    public void setCols(Integer cols) {
        this.cols = cols;
    }

    /**
     * Gets rows.
     *
     * @return the rows
     */
    public Integer getRows() {
        return rows;
    }

    /**
     * Sets rows.
     *
     * @param rows the rows
     */
    public void setRows(Integer rows) {
        this.rows = rows;
    }

    /**
     * Gets frame rate.
     *
     * @return the frame rate
     */
    public Float getFrameRate() {
        return frameRate;
    }

    /**
     * Sets frame rate.
     *
     * @param frameRate the frame rate
     */
    public void setFrameRate(Float frameRate) {
        this.frameRate = frameRate;
    }

    /**
     * Gets frames count.
     *
     * @return the frames count
     */
    public int getFramesCount() {
        return getRows() * getCols();
    }

    /**
     * Sets frame start.
     *
     * @param frameStart the frame start
     */
    public void setFrameStart(Integer frameStart) {
        this.frameStart = frameStart;
    }

    /**
     * Get regions texture region [ ].
     *
     * @return the texture region [ ]
     */
    public TextureRegion[] getRegions() {
        return new TextureRegion[0];
    }

    /**
     * Sets frame end.
     *
     * @param frameEnd the frame end
     */
    public void setFrameEnd(Integer frameEnd) {
        this.frameEnd = frameEnd;
    }
}
