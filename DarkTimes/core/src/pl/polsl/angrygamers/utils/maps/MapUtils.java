package pl.polsl.angrygamers.utils.maps;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;

/**
 * The type Map utils.
 */
public class MapUtils {

    /**
     * Gets collision layer.
     *
     * @param map the map
     * @return the collision layer
     */
    public static MapObjects getCollisionLayer(TiledMap map) {
        final MapLayer collisionLayer = map.getLayers().get("collision");
        return collisionLayer == null ? new MapObjects() : collisionLayer.getObjects();
    }
}
