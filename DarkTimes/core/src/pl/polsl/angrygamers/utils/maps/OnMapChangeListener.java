package pl.polsl.angrygamers.utils.maps;

/**
 * The interface On map change listener.
 */
public interface OnMapChangeListener {
    /**
     * On map changed.
     *
     * @param mapRenderer the map renderer
     * @param map         the map
     */
    void onMapChanged(MapRenderer mapRenderer, MapEnum map);
}
