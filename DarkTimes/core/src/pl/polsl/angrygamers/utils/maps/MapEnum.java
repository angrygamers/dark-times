package pl.polsl.angrygamers.utils.maps;

/**
 * The enum Map enum.
 */
public enum MapEnum {
    /**
     * Chapter 1 map enum.
     */
    CHAPTER_1("maps/Chapter1.tmx"),
    /**
     * Chapter 2 map enum.
     */
    CHAPTER_2("maps/Chapter2.tmx"),
    /**
     * Chapter 3 map enum.
     */
    CHAPTER_3("maps/Chapter3.tmx"),
    /**
     * Chapter 4 map enum.
     */
    CHAPTER_4("maps/Chapter4.tmx"),
    /**
     * Chapter 5 and 6 map enum.
     */
    CHAPTER_5_AND_6("maps/Chapter5&6.tmx");

    /**
     * The File name.
     */
    final String fileName;

    MapEnum(String fileName) {
        this.fileName = fileName;
    }
}
