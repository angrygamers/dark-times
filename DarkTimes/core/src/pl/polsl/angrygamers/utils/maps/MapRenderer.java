package pl.polsl.angrygamers.utils.maps;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import pl.polsl.angrygamers.utils.WorldCamera;

import static pl.polsl.angrygamers.utils.GlobalVariables.SPRITE_BATCH;

/**
 * The type Map renderer.
 */
public class MapRenderer {

    private static final TiledMap EMPTY_MAP = new TiledMap();
    private static final MapRenderer MAP_RENDERER
            = new MapRenderer(SPRITE_BATCH, WorldCamera.getInstance().getCamera());

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static MapRenderer getInstance() {
        return MAP_RENDERER;
    }

    private final OrthographicCamera camera;
    private final TmxMapLoader mapLoader;
    private OnMapChangeListener onMapChangeListener;
    private OrthogonalTiledMapRenderer tiledMapRenderer;

    private MapRenderer(final SpriteBatch batch, final OrthographicCamera camera) {
        this.camera = camera;
        this.mapLoader = new TmxMapLoader();
        tiledMapRenderer = new OrthogonalTiledMapRenderer(EMPTY_MAP, batch);
    }

    /**
     * Render.
     */
    public void render() {
        tiledMapRenderer.setView(camera);
        tiledMapRenderer.render();
    }

    /**
     * Change map.
     *
     * @param map the map
     */
    public void changeMap(final MapEnum map) {
        final TiledMap tiledMap = loadMap(map);
        tiledMapRenderer.setMap(tiledMap);
        onMapChanged(map);
    }

    private TiledMap loadMap(final MapEnum map) {
        return mapLoader.load(map.fileName);
    }

    /**
     * Gets map.
     *
     * @return the map
     */
    public TiledMap getMap() {
        return tiledMapRenderer.getMap();
    }

    /**
     * Sets on map changed listener.
     *
     * @param onMapChangeListener the on map change listener
     */
    public void setOnMapChangedListener(OnMapChangeListener onMapChangeListener) {
        this.onMapChangeListener = onMapChangeListener;
    }

    private void onMapChanged(final MapEnum map) {
        final OnMapChangeListener onMapChangeListener = this.onMapChangeListener;
        if(onMapChangeListener != null) {
            onMapChangeListener.onMapChanged(this, map);
        }
    }
}
