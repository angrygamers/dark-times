package pl.polsl.angrygamers.utils.resources;

import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.enums.Direction;
import pl.polsl.angrygamers.objects.gameObjects.Enemy;
import pl.polsl.angrygamers.objects.gameObjects.GameObject;
import pl.polsl.angrygamers.utils.CircularArrayList;
import pl.polsl.angrygamers.utils.Size;
import pl.polsl.angrygamers.utils.TextureData;

/**
 * The type Enemies factory.
 */
public class EnemiesFactory {

    /**
     * Create gloo enemy game object.
     *
     * @param position the position
     * @return the game object
     */
    public GameObject createGlooEnemy(Vector2 position) {
        final CircularArrayList<Direction> path = new CircularArrayList<>();
        path.add(Direction.DOWN);
        path.add(Direction.DOWN_LEFT);
        path.add(Direction.LEFT);
        path.add(Direction.UP_LEFT);
        path.add(Direction.UP);
        path.add(Direction.UP_RIGHT);
        path.add(Direction.RIGHT);
        path.add(Direction.DOWN_RIGHT);

        return new Enemy(
                new Size(32f, 32f),
                new TextureData("textures/glooRotated.png", 2, 1, 0.025f),
                new Vector2(450.0f, 2500.0f), 1f, path);
    }
}
