package pl.polsl.angrygamers.utils.resources;

import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.objects.gameObjects.GameObject;
import pl.polsl.angrygamers.objects.gameObjects.NPC;
import pl.polsl.angrygamers.utils.CircularArrayList;
import pl.polsl.angrygamers.utils.Size;
import pl.polsl.angrygamers.utils.TextureData;

import java.util.Random;

/**
 * The type Npc factory.
 */
public class NPCFactory {

    private static Random random = new Random();
    private static String[] npcTextures = new String[] {
            "textures/knife_idle.png", "textures/gun_idle.png"
    };

    /**
     * Create game object.
     *
     * @param position the position
     * @return the game object
     */
    public GameObject create(Vector2 position) {

        final String texturePath = npcTextures[random.nextInt(npcTextures.length)];
        final TextureData textureData = new TextureData(texturePath, 4, 5, 2.0f);
        textureData.setFrameStart(7);
        textureData.setFrameEnd(7);

        return new NPC(position,
                0.0f,
                new Size(60.0f, 80.0f),
                textureData,
                new CircularArrayList<>());
    }
}
