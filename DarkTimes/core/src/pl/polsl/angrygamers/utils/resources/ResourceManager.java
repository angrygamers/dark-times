package pl.polsl.angrygamers.utils.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Disposable;
import pl.polsl.angrygamers.objects.animations.AnimationsState;

/**
 * The type Resource manager.
 */
public class ResourceManager implements Disposable {

    private AssetManager assetManager;
    private Skin skin;
    private TextureAtlas atlas;
    private BitmapFont font12;
    private BitmapFont font18;
    private BitmapFont font22;

    /**
     * Instantiates a new Resource manager.
     */
    public ResourceManager() {
        assetManager = new AssetManager();
        assetManager.load("skins/dialog.atlas", TextureAtlas.class);
        assetManager.finishLoading();

        FileHandle fontFile = Gdx.files.internal("fonts/pixelart_modified.otf");

        FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(fontFile);
        FreeTypeFontGenerator.FreeTypeFontParameter fontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontParameter.size = 18;
        fontParameter.color = Color.WHITE;
        fontParameter.borderColor = Color.BLACK;
        fontParameter.borderWidth = 2.0f;
        font18 = fontGenerator.generateFont(fontParameter);

        fontParameter.size = 22;
        font22 = fontGenerator.generateFont(fontParameter);

        fontParameter.size = 12;
        font12 = fontGenerator.generateFont(fontParameter);

        fontGenerator.dispose();

        atlas = assetManager.get("skins/dialog.atlas", TextureAtlas.class);

        skin = new Skin(atlas);
        skin.add("small-font", font12, BitmapFont.class);
        skin.add("default-font", font18, BitmapFont.class);
        skin.add("big-font", font22, BitmapFont.class);
        skin.load(Gdx.files.internal("skins/dialog.json"));

        Texture heartTexture = new Texture(Gdx.files.internal("heartsh.png"));
        skin.add("heart", new TextureRegion(heartTexture), TextureRegion.class);

        AnimationsState animationsState = new AnimationsState("dice.png", 1, 5);
        animationsState.setFrameDuration(0.8f);
        animationsState.getCurrentAnimation().setPlayMode(Animation.PlayMode.LOOP);
        skin.add("dice_animation", animationsState);

        ParticleEffect hurtParticleEffect = new ParticleEffect();
        hurtParticleEffect.load(Gdx.files.internal("particles/hurt_particles.particle"), Gdx.files.internal("particles"));
        skin.add("hurt_particles", hurtParticleEffect, ParticleEffect.class);
    }

    /**
     * Gets skin.
     *
     * @return the skin
     */
    public Skin getSkin() {
        return skin;
    }

    @Override
    public void dispose() {
        assetManager.dispose();
        atlas.dispose();
        skin.dispose();
    }
}