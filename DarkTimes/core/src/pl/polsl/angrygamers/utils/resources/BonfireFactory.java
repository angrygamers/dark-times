package pl.polsl.angrygamers.utils.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;

/**
 * The type Bonfire factory.
 */
public class BonfireFactory {
    /**
     * Create particle effect.
     *
     * @param position the position
     * @return the particle effect
     */
    public ParticleEffect create(Vector2 position) {
        ParticleEffect effect = new ParticleEffect();
        effect.load(Gdx.files.internal("particles/bonfire.particle"),
                Gdx.files.internal("particles"));

        effect.start();
        effect.getEmitters().first().setContinuous(true);
        effect.setPosition(position.x, position.y);
        return effect;
    }
}