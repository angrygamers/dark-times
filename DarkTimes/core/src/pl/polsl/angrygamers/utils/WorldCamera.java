package pl.polsl.angrygamers.utils;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import pl.polsl.angrygamers.objects.gameObjects.GameObject;

/**
 * The type World camera.
 */
public class WorldCamera {

    private static final WorldCamera CURRENT_CAMERA = new WorldCamera();

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static WorldCamera getInstance() {
        return CURRENT_CAMERA;
    }

    /**
     * The constant LERP.
     */
    public static float LERP = 2.5f;

    /**
     * The constant PIXELS_PER_TILE.
     */
    public static final float PIXELS_PER_TILE = 64;
    /**
     * The constant WORLD_WIDTH.
     */
    public static final float WORLD_WIDTH = 32 * PIXELS_PER_TILE;
    /**
     * The constant WORLD_HEIGHT.
     */
    public static final float WORLD_HEIGHT = 18 * PIXELS_PER_TILE;
    private static final float MAP_HEIGHT = 64 * PIXELS_PER_TILE;
    private static final float MAP_WIDTH = 64 * PIXELS_PER_TILE;

    private final OrthographicCamera camera;
    private final Viewport viewport;

    private WorldCamera() {
        camera = new OrthographicCamera();
        viewport = new FillViewport(WORLD_WIDTH, WORLD_HEIGHT, camera);
        viewport.apply();

        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
    }

    /**
     * Follow game object.
     *
     * @param gameObject the game object
     * @param delta      the delta
     */
    public void followGameObject(final GameObject gameObject, final float delta) {
        viewport.getCamera().translate((gameObject.getPosition().x - viewport.getCamera().position.x) * LERP * delta,
            (gameObject.getPosition().y - viewport.getCamera().position.y) * LERP * delta, 0);

        final float halfOfViewportWidth = camera.viewportWidth / 2;
        final float halfOfViewportHeight = camera.viewportHeight / 2;

        if (viewport.getCamera().position.x < halfOfViewportWidth) {
            viewport.getCamera().position.x = halfOfViewportWidth;
        }

        if (viewport.getCamera().position.y < halfOfViewportHeight) {
            viewport.getCamera().position.y = halfOfViewportHeight;
        }

        if (viewport.getCamera().position.x > MAP_WIDTH - halfOfViewportWidth) {
            viewport.getCamera().position.x = MAP_WIDTH - halfOfViewportWidth;
        }

        if (viewport.getCamera().position.y > MAP_HEIGHT - halfOfViewportHeight) {
            viewport.getCamera().position.y = MAP_HEIGHT - halfOfViewportHeight;
        }
    }

    /**
     * Update.
     */
    public void update() {
        camera.update();
    }

    /**
     * Gets camera.
     *
     * @return the camera
     */
    public OrthographicCamera getCamera() {
        return camera;
    }

    /**
     * Resize.
     *
     * @param width  the width
     * @param height the height
     */
    public void resize(final int width, final int height) {
        viewport.update(width, height);
    }
}
