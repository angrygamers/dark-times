package pl.polsl.angrygamers.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import pl.polsl.angrygamers.enums.ItemType;
import pl.polsl.angrygamers.enums.ItemUse;
import pl.polsl.angrygamers.objects.Inventory;
import pl.polsl.angrygamers.objects.Item;

import java.util.List;

import static pl.polsl.angrygamers.utils.GlobalVariables.*;

/**
 * The type Inventory manager.
 */
public class InventoryManager {

    private static final InventoryManager CURRENT_INVENTORY_MANAGER = new InventoryManager();

    static {
        InventoryManager.getInstance().setInventory(INVENTORY);
    }

    private Sound openInventorySound = Gdx.audio.newSound(Gdx.files.internal("sounds/open_inventory.wav"));
    private Sound closeInventorySound = Gdx.audio.newSound(Gdx.files.internal("sounds/close_inventory.wav"));
    private Sound consume = Gdx.audio.newSound(Gdx.files.internal("sounds/consume.wav"));
    private Sound equip = Gdx.audio.newSound(Gdx.files.internal("sounds/equip.wav"));
    private Sound takeOff = Gdx.audio.newSound(Gdx.files.internal("sounds/take_off.wav"));
    private Sound throwAway = Gdx.audio.newSound(Gdx.files.internal("sounds/throw_away.wav"));

    private Inventory inventory;

    private Table inventoryTable;
    /**
     * The Is opened.
     */
    public boolean isOpened;

    private final Stage stage;
    private final Skin tableSkin;
    private final TextureRegionDrawable backgroundDrawable;

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static InventoryManager getInstance() {
        return CURRENT_INVENTORY_MANAGER;
    }

    private InventoryManager() {
        isOpened = false;
        Pixmap bgPixmap = new Pixmap(1, 1, Pixmap.Format.RGB565);
        bgPixmap.setColor(Color.SLATE);
        bgPixmap.fill();
        backgroundDrawable = new TextureRegionDrawable(new TextureRegion(new Texture(bgPixmap)));

        this.tableSkin = new Skin();
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/prstart.ttf"));
        final FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 70;
        parameter.color = Color.WHITE;
        BitmapFont titleFont = generator.generateFont(parameter);

        parameter.size = 10;
        parameter.color = Color.WHITE;
        BitmapFont textFont = generator.generateFont(parameter);
        tableSkin.add("titleFont", titleFont);
        tableSkin.add("textFont", textFont);

        tableSkin.add("background", backgroundDrawable);

        parameter.borderColor = Color.FIREBRICK;
        parameter.borderWidth = 1;
        tableSkin.add("fontPicked", titleFont);


        stage = new Stage();
    }

    private void displayInventory(final Inventory inventory) {
        this.inventory = inventory;
        updateTable();
        INPUT_MULTIPLEXER.addProcessor(stage);
        Gdx.input.setInputProcessor(INPUT_MULTIPLEXER);
    }

    private void closeInventory() {
        stage.clear();
        INPUT_MULTIPLEXER.removeProcessor(stage);
        Gdx.input.setInputProcessor(INPUT_MULTIPLEXER);
    }

    private void updateTable() {
        inventoryTable = new Table();
        inventoryTable.setWidth(Gdx.graphics.getWidth());
        inventoryTable.setHeight(Gdx.graphics.getHeight());

        inventoryTable.setBackground(backgroundDrawable);
        Label titleLabel = new Label("Inventory", tableSkin, "titleFont", Color.WHITE);
        titleLabel.setAlignment(Align.top);
        inventoryTable.add(titleLabel).colspan(8).fillX();
        Label freeSpace = new Label(inventory.getFreeSlotsNumber() == 0 ? "Inventory full" : "Free slots: " + inventory.getFreeSlotsNumber(), tableSkin, "textFont", Color.WHITE);
        inventoryTable.row();
        freeSpace.setAlignment(Align.center);
        inventoryTable.add(freeSpace).colspan(8);

        List<Item> itemsInInventory = inventory.getItems();
        for (final Item item : itemsInInventory) {
            inventoryTable.row().padBottom(15.0f).padTop(15.0f).padLeft(0.0f).padRight(0.0f);
            Label name = new Label(item.getName(), tableSkin, "textFont", Color.WHITE);
            name.setAlignment(Align.right);
            inventoryTable.add(name).colspan(1);
            inventoryTable.add(item.getImage()).colspan(2);
            Label description = new Label(item.getDescription(), tableSkin, "textFont", Color.LIGHT_GRAY);
            description.setAlignment(Align.left);
            inventoryTable.add(description).colspan(1);


            TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
            textButtonStyle.font = tableSkin.getFont("textFont");
            textButtonStyle.font.setColor(Color.WHITE);
            TextButton button;
            if (!item.getType().equals(ItemType.MISSION_ITEM)) {
                if (item.isEquipped() && item.getType().equals(ItemType.WEARABLE)) {
                    button = new TextButton("UNEQUIP", textButtonStyle);
                    inventoryTable.add(button);
                    button.addListener(new ChangeListener() {
                        @Override
                        public void changed(ChangeEvent event, Actor actor) {
                            takeOff.play(1.0f);
                            if(item.getUsage().equals(ItemUse.WEARABLE_SPEED_PLUS10)) {
                                PLAYER.setSpeed(PLAYER.getSpeed() * 0.91f);
                            } else if (item.getUsage().equals(ItemUse.WEARABLE_PLUS25)) {
                                PLAYER.setStrength(PLAYER.getStrength() - 25);
                            } else if (item.getUsage().equals(ItemUse.WEARABLE_PLUS50)) {
                                PLAYER.setStrength(PLAYER.getStrength() - 50);
                            }
                            item.setEquipped(false);
                            PLAYER.setItemEquipped(false);
                            updateTable();
                        }
                    });
                } else if (!item.isEquipped() && item.getType().equals(ItemType.WEARABLE)) {
                    button = new TextButton("EQUIP", textButtonStyle);
                    inventoryTable.add(button);
                    button.addListener(new ChangeListener() {
                        @Override
                        public void changed(ChangeEvent event, Actor actor) {
                            if (!PLAYER.isItemEquipped()) {
                                equip.play(1.0f);
                                if(item.getUsage().equals(ItemUse.WEARABLE_SPEED_PLUS10)) {
                                    PLAYER.setSpeed(PLAYER.getSpeed() * 1.1f);
                                }
                                else if (item.getUsage().equals(ItemUse.WEARABLE_PLUS25)) {
                                    PLAYER.setStrength(PLAYER.getStrength() + 25);
                                } else if (item.getUsage().equals(ItemUse.WEARABLE_PLUS50)) {
                                    PLAYER.setStrength(PLAYER.getStrength() + 50);
                                }
                                item.setEquipped(true);
                                PLAYER.setItemEquipped(true);
                                updateTable();
                            }
                        }
                    });
                } else if (item.getType().equals(ItemType.CONSUMABLE)) {
                    button = new TextButton("USE", textButtonStyle);
                    inventoryTable.add(button);
                    button.addListener(new ChangeListener() {
                        @Override
                        public void changed(ChangeEvent event, Actor actor) {
                            consume.play(1.0f);
                            if (item.getUsage().equals(ItemUse.RESTORE_FULL_HEALTH)) {
                                PLAYER.setHealth(100);
                            } else if (item.getUsage().equals(ItemUse.RESTORE_HEALTH50)) {
                                int newHealth = PLAYER.getHealth() + 50;
                                PLAYER.setHealth(Math.min(newHealth, 100));
                            } else if (item.getUsage().equals(ItemUse.RESTORE_HEALTH25)) {
                                int newHealth = PLAYER.getHealth() + 25;
                                PLAYER.setHealth(Math.min(newHealth, 100));
                            }
                            inventory.getItems().remove(item);
                            itemsInInventory.remove(item);
                            updateTable();
                        }
                    });
                }

                button = new TextButton("THROW AWAY", textButtonStyle);
                inventoryTable.add(button);
                button.addListener(new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        throwAway.play(1.0f);
                        if (item.getType().equals(ItemType.WEARABLE)) {
                            PLAYER.setItemEquipped(false);
                            if (item.getUsage().equals(ItemUse.WEARABLE_PLUS25)) {
                                PLAYER.setStrength(PLAYER.getStrength() - 25);
                            } else if (item.getUsage().equals(ItemUse.WEARABLE_PLUS50)) {
                                PLAYER.setStrength(PLAYER.getStrength() - 50);
                            }
                        }
                        inventory.getItems().remove(item);
                        itemsInInventory.remove(item);
                        updateTable();
                    }
                });
            }
        }
        stage.addActor(inventoryTable);
    }

    /**
     * Gets inventory stage.
     *
     * @return the inventory stage
     */
    public Stage getInventoryStage() {
        return stage;
    }

    /**
     * Gets inventory table.
     *
     * @return the inventory table
     */
    public Table getInventoryTable() {
        return inventoryTable;
    }

    /**
     * Open inventory.
     */
    public void open() {
        if (!this.isOpened) {
            this.isOpened = true;
            displayInventory(inventory);
            openInventorySound.play(1.0f);
            System.out.println("Inventory opens!"); //debug
        } else {
            this.isOpened = false;
            closeInventory();
            closeInventorySound.play(1.0f);
            System.out.println("Inventory closing!"); //debug
        }
    }

    /**
     * Gets inventory.
     *
     * @return the inventory
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Sets inventory.
     *
     * @param inventory the inventory
     */
    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    /**
     * Gets table skin.
     *
     * @return the table skin
     */
    public Skin getTableSkin() {
        return tableSkin;
    }
}

