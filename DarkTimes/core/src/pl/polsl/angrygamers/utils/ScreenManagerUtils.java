package pl.polsl.angrygamers.utils;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;
import pl.polsl.angrygamers.DarkTimesApplication;
import pl.polsl.angrygamers.enums.GameScreenEnum;
import pl.polsl.angrygamers.objects.animations.AnimatedBehaviour;
import pl.polsl.angrygamers.objects.animations.AnimationsState;
import pl.polsl.angrygamers.objects.duel.DogAdapter;
import pl.polsl.angrygamers.objects.duel.PlayerAdapter;
import pl.polsl.angrygamers.screens.ChapterOne;
import pl.polsl.angrygamers.screens.GameScreen;
import pl.polsl.angrygamers.screens.MenuScreen;
import pl.polsl.angrygamers.screens.duel.DuelScreen;
import pl.polsl.angrygamers.screens.plot.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static pl.polsl.angrygamers.utils.GlobalVariables.PLAYER;

/**
 * The type Screen manager utils.
 */
public class ScreenManagerUtils {
    private static final ChapterOne CHAPTER_ONE_SCREEN = new ChapterOne();

    private static Map<GameScreenEnum, ScreenAdapter> screenMap;

    /**
     * Init screen manager.
     *
     * @param game the game
     */
    public static void init(final DarkTimesApplication game) {
        screenMap = new HashMap<GameScreenEnum, ScreenAdapter>() {{
            put(GameScreenEnum.MENU_SCREEN, new MenuScreen(game));
            put(GameScreenEnum.GAME_SCREEN, new GameScreen());
            put(GameScreenEnum.CHAPTER_ONE, CHAPTER_ONE_SCREEN);
            put(GameScreenEnum.CHAPTER_ONE_DUEL, new DuelScreen(new PlayerAdapter(PLAYER,
                            new Texture("dad.png"),
                            new AnimatedBehaviour(
                                    new AnimationsState("adventurer-v1.5-Sheet.png", 16, 7, 5,
                                            Arrays.asList(
                                                    new AnimationsState.StateEntry("missed", 0, 5),
                                                    new AnimationsState.StateEntry("fighting", 0, 5),
                                                    new AnimationsState.StateEntry("fighting-fists", 0, 5),
                                                    new AnimationsState.StateEntry("hit", 0, 5),
                                                    new AnimationsState.StateEntry("defeated", 0, 5)
                                            )).timing(0.5f),
                                    "fighting"
                            )
                    ),
                            new DogAdapter(PLAYER,
                                    new Texture("angry_dog.png"),
                                    new AnimatedBehaviour(
                                            new AnimationsState("textures/dogRight.png", 1, 5, 5,
                                                    Arrays.asList(
                                                            new AnimationsState.StateEntry("missed", 0, 5),
                                                            new AnimationsState.StateEntry("fighting", 0, 5),
                                                            new AnimationsState.StateEntry("fighting-fists", 0, 5),
                                                            new AnimationsState.StateEntry("hit", 0, 5),
                                                            new AnimationsState.StateEntry("defeated", 0, 5)
                                                    )).timing(0.5f),
                                            "fighting"
                                    )
                            ), GameScreenEnum.CHAPTER_ONE
                    )
            );
            put(GameScreenEnum.CHAPTER_TWO, new ChapterTwo());
            put(GameScreenEnum.CHAPTER_THREE, new ChapterThree());
            put(GameScreenEnum.CHAPTER_FOUR, new ChapterFour());
            put(GameScreenEnum.CHAPTER_FIVE_AND_SIX, new ChapterFiveAndSix());
            put(GameScreenEnum.CHAPTER_SEVEN, new ChapterSeven());
        }};
    }

    /**
     * Gets screen.
     *
     * @param key the key
     * @return the screen
     */
    public static ScreenAdapter getScreen(final GameScreenEnum key) {
        return screenMap.get(key);
    }
}
