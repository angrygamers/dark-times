package pl.polsl.angrygamers.utils;

/**
 * The type Size.
 */
public class Size {
    private Float width;
    private Float height;

    /**
     * Instantiates a new Size.
     *
     * @param width  the width
     * @param height the height
     */
    public Size(Float width, Float height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Instantiates a new Size.
     */
    public Size() {
    }

    /**
     * Gets width.
     *
     * @return the width
     */
    public Float getWidth() {
        return width;
    }

    /**
     * Sets width.
     *
     * @param width the width
     */
    public void setWidth(Float width) {
        this.width = width;
    }

    /**
     * Gets height.
     *
     * @return the height
     */
    public Float getHeight() {
        return height;
    }

    /**
     * Sets height.
     *
     * @param height the height
     */
    public void setHeight(Float height) {
        this.height = height;
    }
}
