package pl.polsl.angrygamers.utils;

/**
 * The type Range.
 */
public class Range {

    private float from;
    private float until;

    /**
     * Instantiates a new Range.
     *
     * @param from  the from
     * @param until the until
     */
    public Range(float from, float until) {
        if(until < from) {
            throw new IllegalArgumentException("Until value should not be less than from value!");
        }

        this.from = from;
        this.until = until;
    }

    /**
     * Gets from.
     *
     * @return the from
     */
    public float getFrom() {
        return from;
    }

    /**
     * Gets until.
     *
     * @return the until
     */
    public float getUntil() {
        return until;
    }
}
