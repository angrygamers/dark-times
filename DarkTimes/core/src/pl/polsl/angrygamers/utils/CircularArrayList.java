package pl.polsl.angrygamers.utils;

import java.util.ArrayList;

/**
 * The type Circular array list.
 *
 * @param <E> the Object type parameter
 */
public class CircularArrayList<E> extends ArrayList<E> {
    private Integer currentIndex = 0;
    @Override
    public E get(int index) {
        if(index < 0)
            index += size();
        if(index >= size()){
            index = size() % index;
        }
        return super.get(index);
    }

    /**
     * Next element.
     *
     * @return the element
     */
    public E next(){
        if(currentIndex >= size())
            currentIndex = 0;
        if(currentIndex < 0)
            currentIndex += size();

        return get(currentIndex++);
    }

    /**
     * Get element.
     *
     * @return the element
     */
    public E get(){
        return get(currentIndex);
    }
}
