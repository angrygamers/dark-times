package pl.polsl.angrygamers.screens;

import box2dLight.ConeLight;
import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.CircleMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import pl.polsl.angrygamers.enums.Direction;
import pl.polsl.angrygamers.enums.ItemType;
import pl.polsl.angrygamers.enums.ItemUse;
import pl.polsl.angrygamers.objects.Item;
import pl.polsl.angrygamers.objects.gameObjects.Enemy;
import pl.polsl.angrygamers.objects.gameObjects.Ghost;
import pl.polsl.angrygamers.utils.*;
import pl.polsl.angrygamers.utils.dialogues.DialogueManager;
import pl.polsl.angrygamers.utils.maps.MapEnum;
import pl.polsl.angrygamers.utils.maps.MapRenderer;
import pl.polsl.angrygamers.utils.maps.MapUtils;
import pl.polsl.angrygamers.utils.maps.OnMapChangeListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static pl.polsl.angrygamers.PlayerInput.PLAYER_INPUT_SESSION;
import static pl.polsl.angrygamers.utils.GlobalVariables.*;
import static pl.polsl.angrygamers.utils.WorldCamera.WORLD_HEIGHT;
import static pl.polsl.angrygamers.utils.WorldCamera.WORLD_WIDTH;

/**
 * The type Game screen.
 */
public class GameScreen extends ScreenAdapter implements OnMapChangeListener {

    private InventoryManager inventoryManager;
    private SpriteBatch inventoryBatch;

    private SpriteBatch dialogueBatch;
    private DialogueManager dialogueManager;
    private WorldCamera worldCamera;
    private MapRenderer mapRenderer;

    private static final List<Ghost> ghosts = new ArrayList<>();

    private static final List<Item> items = new ArrayList<>();

    private Enemy enemy;

    private RayHandler rayHandler;
    private ConeLight coneLight;

    private World world;

    private final ShapeRenderer shapeRenderer = new ShapeRenderer();

    private Sound pickItemSound = Gdx.audio.newSound(Gdx.files.internal("sounds/pick_item.wav"));


    /**
     * Instantiates a new Game screen.
     */
    public GameScreen() {
        super();
    }

    @Override
    public void show() {
        super.show();
        worldCamera = WorldCamera.getInstance();
        mapRenderer = MapRenderer.getInstance();
        inventoryManager = InventoryManager.getInstance();
        inventoryBatch = new SpriteBatch();
        inventoryManager.setInventory(INVENTORY);

        dialogueManager = DialogueManager.getInstance();
        dialogueBatch = new SpriteBatch();

        final CircularArrayList<Direction> path = new CircularArrayList<>();
        path.add(Direction.DOWN);
        path.add(Direction.DOWN_LEFT);
        path.add(Direction.LEFT);
        path.add(Direction.UP_LEFT);
        path.add(Direction.UP);
        path.add(Direction.UP_RIGHT);
        path.add(Direction.RIGHT);
        path.add(Direction.DOWN_RIGHT);

        enemy = new Enemy(new Size(32f, 32f), new TextureData("textures/glooRotated.png", 2, 1, 0.025f),
            new Vector2(worldCamera.getCamera().position.x + 100f, worldCamera.getCamera().position.y + 100f), 1f, path);
        PLAYER_INPUT_SESSION.setGameObjects(new ArrayList<>(Arrays.asList(PLAYER, enemy)));

        world = new World(new Vector2(0, 0), true);
        rayHandler = new RayHandler(world);
        rayHandler.removeAll();
        rayHandler.setShadows(true);
        rayHandler.setAmbientLight(1f, 2f, 33f, 0.1f);

        coneLight = new ConeLight(rayHandler, 60, Color.GRAY, 1000, PLAYER.getPosition().x, PLAYER.getPosition().y, PLAYER.getRotation(), 20);
        coneLight.setStaticLight(false);
        coneLight.setSoft(true);
        coneLight.setSoftnessLength(80);

        //creating new items
        Item item = new Item(new Vector2(worldCamera.getCamera().position.x - 100f, worldCamera.getCamera().position.y - 100f), new Size(48f, 48f), new Texture("textures/Bandage.png"), ItemType.CONSUMABLE, ItemUse.RESTORE_HEALTH50, "Bandage", "Useful to heal");
        Item item2 = new Item(new Vector2(worldCamera.getCamera().position.x - 100f, worldCamera.getCamera().position.y - 150f), new Size(48f, 48f), new Texture("textures/CannedFood.png"), ItemType.CONSUMABLE, ItemUse.RESTORE_HEALTH25, "Canned food", "Not so tasty, but it's ok");
        Item item3 = new Item(new Vector2(worldCamera.getCamera().position.x - 200f, worldCamera.getCamera().position.y - 200f), new Size(48f, 48f), new Texture("textures/Machete.png"), ItemType.WEARABLE, ItemUse.WEARABLE_PLUS50, "Machete", "Now you can survive in Krakow");
        Item item4 = new Item(new Vector2(worldCamera.getCamera().position.x - 270f, worldCamera.getCamera().position.y - 200f), new Size(48f, 48f), new Texture("textures/BaseballBat.png"), ItemType.WEARABLE, ItemUse.WEARABLE_PLUS25, "Baseball bat", "Hit'em bois");
        Item item5 = new Item(new Vector2(worldCamera.getCamera().position.x - 270f, worldCamera.getCamera().position.y - 270f), new Size(48f, 48f), new Texture("textures/HikingBoots.png"), ItemType.MISSION_ITEM, ItemUse.MISSION_ITEM, "Mission item", "It's important so you can't throw it away");


        PLAYER_INPUT_SESSION.setGameObjects(new ArrayList<>(Arrays.asList(PLAYER, enemy)));
        //adding items to list
        items.add(item);
        items.add(item2);
        items.add(item3);
        items.add(item4);
        items.add(item5);
    }

    private static PolygonShape getRectangle(final RectangleMapObject rectangleObject) {
        final Rectangle rectangle = rectangleObject.getRectangle();
        final PolygonShape polygon = new PolygonShape();
        final Vector2 size = new Vector2((rectangle.x + rectangle.width / 2), (rectangle.y + rectangle.height / 2));
        polygon.setAsBox(rectangle.width, rectangle.height, size, 0.0f);
        return polygon;
    }

    private boolean isInLight(final Rectangle rectangle) {
        return rayHandler.pointAtLight(rectangle.x, rectangle.y);
    }

    @Override
    public void render(final float delta) {
        super.render(delta);
        Gdx.gl.glClearColor(1, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (MIST_SHADER.isActive()) {
            displayActiveNight(delta);
        } else {
            PLAYER.update();
            enemy.update(PLAYER);
            worldCamera.followGameObject(PLAYER, delta);

            worldCamera.update();
            mapRenderer.render();
            SPRITE_BATCH.setProjectionMatrix(worldCamera.getCamera().combined);

            //check if player is taking item and add it to inventory (if there is space)
            for (Item item : items) {
                if (PLAYER.getSpriteRectangle().overlaps(item.getSpriteRectangle()) && !INVENTORY.tooFull) {
                    pickItemSound.play(1.0f);
                    INVENTORY.addItem(item);
                }
            }

            // collision debug
            if (PLAYER_INPUT_SESSION.isShowingCollisionLayer()) {
                drawCollisionLayer();
            }

            world.step(delta, 8, 3);
            coneLight.setPosition(PLAYER.getPosition().x + 5, PLAYER.getPosition().y);
            coneLight.setDirection(PLAYER.getRotation());
            rayHandler.setCombinedMatrix(worldCamera.getCamera().combined);
            rayHandler.updateAndRender();
            SPRITE_BATCH.begin();
            PLAYER.draw(SPRITE_BATCH);
            //drawing only if is in flashlight
            if (isInLight(enemy.getSpriteRectangle())) {
                enemy.draw(SPRITE_BATCH);
            }

            //drawing only if is in flashlight
            for (Item item : items) {
                if (!item.isInInventory() && isInLight(item.getSpriteRectangle())) {
                    item.getSprite().draw(SPRITE_BATCH);
                }
            }
            SPRITE_BATCH.end();

            dialogueBatch.begin();
            if (dialogueManager.getCurrentlyDisplayedDialogue().isPresent()) {
                dialogueManager.getDialogueTable().setPosition(0, 0);
                dialogueManager.getDialogueStage().draw();
            }
            dialogueBatch.end();

            inventoryBatch.begin();
            if (inventoryManager.isOpened) {
                inventoryManager.getInventoryTable().setPosition(0, 0);
                inventoryManager.getInventoryStage().draw();
            }
            inventoryBatch.end();
        }
    }

    private void drawCollisionLayer() {
        final TiledMap map = mapRenderer.getMap();
        final MapObjects collisionLayer = MapUtils.getCollisionLayer(map);
        shapeRenderer.setProjectionMatrix(worldCamera.getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.GREEN);
        shapeRenderer.rect(PLAYER.getSpriteRectangle().x, PLAYER.getSpriteRectangle().y,
            PLAYER.getSpriteRectangle().width, PLAYER.getSpriteRectangle().height);
        shapeRenderer.end();

        for (final RectangleMapObject rectangle : collisionLayer.getByType(RectangleMapObject.class)) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.RED);
            shapeRenderer.rect(rectangle.getRectangle().x, rectangle.getRectangle().y,
                rectangle.getRectangle().width, rectangle.getRectangle().height);
            shapeRenderer.end();
        }

        for (final CircleMapObject circle : collisionLayer.getByType(CircleMapObject.class)) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.RED);
            shapeRenderer.circle(circle.getCircle().x, circle.getCircle().y, circle.getCircle().radius);
            shapeRenderer.end();
        }

        for (final PolygonMapObject polygon : collisionLayer.getByType(PolygonMapObject.class)) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.RED);
            shapeRenderer.polygon(polygon.getPolygon().getTransformedVertices());
            shapeRenderer.end();
        }
    }

    private void displayActiveNight(final float delta) {
        final int ghostCount = 3;

        if (ghosts.size() < ghostCount && !MIST_SHADER.isStopping()) {
            final Vector2 ghostPosition = new Vector2(
                (float) ThreadLocalRandom.current().nextDouble(PLAYER.getPosition().x - WORLD_WIDTH / 2, PLAYER.getPosition().x + WORLD_WIDTH / 2),
                (float) ThreadLocalRandom.current().nextDouble(PLAYER.getPosition().y - WORLD_HEIGHT / 2, PLAYER.getPosition().y + WORLD_HEIGHT / 2));
            while (ghostPosition.x > PLAYER.getPosition().x - 4 * WorldCamera.PIXELS_PER_TILE &&
                ghostPosition.x < PLAYER.getPosition().x) {
                ghostPosition.x -= WorldCamera.PIXELS_PER_TILE;
            }
            while (ghostPosition.x < PLAYER.getPosition().x + 4 * WorldCamera.PIXELS_PER_TILE &&
                ghostPosition.x > PLAYER.getPosition().x) {
                ghostPosition.x += WorldCamera.PIXELS_PER_TILE;
            }
            final Ghost ghost = new Ghost(new Size(100f, 100f), new TextureData("textures/enemy-sheet.png", 3, 5, 0.2f), ghostPosition, 1f, null);
            ghosts.add(ghost);
            PLAYER_INPUT_SESSION.getGameObjects().add(ghost);
        }

        if (MIST_SHADER.isStopping()) {
            GameScreen.killGhosts();
        }

        MIST_SHADER.displayMistShader();

        SPRITE_BATCH.setProjectionMatrix(worldCamera.getCamera().combined);
        worldCamera.followGameObject(PLAYER, delta);
        worldCamera.update();
        mapRenderer.render();
        SPRITE_BATCH.begin();
        ghosts.forEach(ghost -> {
            ghost.update(PLAYER);
            if (ghost.checkIsPlayerAttacked()) {
                MIST_SHADER.progressMist(0.1f, 1500);
            }
            ghost.draw(SPRITE_BATCH);
        });
        PLAYER.draw(SPRITE_BATCH);
        SPRITE_BATCH.end();
        ghosts.removeIf(ghost -> {
            final boolean dismissed = ghost.checkIsGhostDismissed();
            if (dismissed) PLAYER_INPUT_SESSION.getGameObjects().remove(ghost);
            return dismissed;
        });
    }

    @Override
    public void resize(final int width, final int height) {
        super.resize(width, height);
        worldCamera.resize(width, height);
        dialogueManager.resize(width, height);
        MIST_SHADER.resize(width, height);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void dispose() {
        super.dispose();
        enemy.dispose();
        coneLight.dispose();
    }

    private static void killGhosts() {
        ghosts.removeIf(ghost -> {
            PLAYER_INPUT_SESSION.getGameObjects().remove(ghost);
            return true;
        });
    }

    @Override
    public void onMapChanged(MapRenderer mapRenderer, MapEnum map) {
        final TiledMap tiledMap = mapRenderer.getMap();
        updateCollisionObjects(tiledMap);
        updateLightSources(map);
    }
    private void updateCollisionObjects(TiledMap tiledMap) {
        final MapObjects objects = MapUtils.getCollisionLayer(tiledMap);
        removeOldCollisionObjects();
        addCollisionObjects(objects);
    }

    private void updateLightSources(MapEnum map) {
        //rayHandler.removeAll();
        //coneLight.add(rayHandler);

        PointLight p = new PointLight(rayHandler, 10, Color.FIREBRICK, 120.0f, PLAYER.getPosition().x, PLAYER.getPosition().y);
        p.setStaticLight(true);
        p.setSoft(true);

        p.setSoftnessLength(120.0f);
        // TODO: get light sources for chapter and apply
    }

    private void removeOldCollisionObjects() {
        Array<Body> bodies = new Array<>();
        world.getBodies(bodies);
        for(Body body : bodies){
            world.destroyBody(body);
        }
    }

    private void addCollisionObjects(MapObjects objects) {
        for (final RectangleMapObject rectangle : objects.getByType(RectangleMapObject.class)) {
            if (rectangle.getName() == null) {
                final Shape shape = getRectangle(rectangle);
                final BodyDef bd = new BodyDef();
                bd.type = BodyDef.BodyType.StaticBody;
                final Body body = world.createBody(bd);
                body.createFixture(shape, 1);
                shape.dispose();
            }
        }
    }
}
