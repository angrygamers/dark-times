package pl.polsl.angrygamers.screens;

import box2dLight.ConeLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import pl.polsl.angrygamers.DarkTimesApplication;
import pl.polsl.angrygamers.enums.*;
import pl.polsl.angrygamers.objects.Item;
import pl.polsl.angrygamers.objects.gameObjects.Enemy;
import pl.polsl.angrygamers.objects.gameObjects.Ghost;
import pl.polsl.angrygamers.screens.plot.triggers.ChangeMapTrigger;
import pl.polsl.angrygamers.screens.plot.triggers.Trigger;
import pl.polsl.angrygamers.utils.*;
import pl.polsl.angrygamers.utils.dialogues.Dialogue;
import pl.polsl.angrygamers.utils.dialogues.DialogueManager;
import pl.polsl.angrygamers.utils.dialogues.DialogueOption;
import pl.polsl.angrygamers.utils.maps.MapEnum;
import pl.polsl.angrygamers.utils.maps.MapRenderer;
import pl.polsl.angrygamers.utils.maps.MapUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static pl.polsl.angrygamers.PlayerInput.PLAYER_INPUT_SESSION;
import static pl.polsl.angrygamers.utils.GlobalVariables.*;

/**
 * The type Chapter one.
 */
public class ChapterOne extends ScreenAdapter {

    private Boolean intro;
    private Boolean lightsUp;

    private InventoryManager inventoryManager;
    private SpriteBatch inventoryBatch;

    private SpriteBatch dialogueBatch;
    private DialogueManager dialogueManager;
    private WorldCamera worldCamera;

    private static final List<Ghost> ghosts = new ArrayList<>();

    private static final List<Item> items = new ArrayList<>();

    private Enemy enemyDog;
    private Rectangle dogTriggerRectWest;
    private Rectangle dogTriggerRectSouth;

    private RayHandler rayHandler;
    private ConeLight coneLight;
    private List<ConeLight> introLight;

    private World world;

    private final ShapeRenderer shapeRenderer = new ShapeRenderer();

    private final Sound pickItemSound = Gdx.audio.newSound(Gdx.files.internal("sounds/pick_item.wav"));

    private boolean firstDialogueShown = true;
    private boolean secondDialogueShown = true;
    private boolean chapterInitialized = false;

    /**
     * The Change map Trigger.
     */
    final Trigger trigger = new ChangeMapTrigger(new Vector2(3934f, 2406f), GameScreenEnum.CHAPTER_TWO);

    /**
     * Instantiates a new Chapter one.
     */
    public ChapterOne() {
        super();
    }

    @Override
    public void show() {
        super.show();
        MapRenderer.getInstance().changeMap(MapEnum.CHAPTER_1);
        final InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(PLAYER_INPUT_SESSION);
        Gdx.input.setInputProcessor(multiplexer);
        if (!chapterInitialized) {
            chapterInitialized = true;
            intro = true;
            worldCamera = WorldCamera.getInstance();
            inventoryManager = InventoryManager.getInstance();
            inventoryBatch = new SpriteBatch();
            inventoryManager.setInventory(INVENTORY);

            dialogueManager = DialogueManager.getInstance();
            dialogueBatch = new SpriteBatch();

            final CircularArrayList<Direction> path = new CircularArrayList<>();
            path.add(Direction.DOWN);
            path.add(Direction.DOWN);
            path.add(Direction.DOWN);
            path.add(Direction.LEFT);
            path.add(Direction.LEFT);
            path.add(Direction.LEFT);
            path.add(Direction.UP);
            path.add(Direction.UP);
            path.add(Direction.UP);
            path.add(Direction.RIGHT);
            path.add(Direction.RIGHT);
            path.add(Direction.RIGHT);

            final List<TextureData> animations = new ArrayList<>();
            animations.add(new TextureData("textures/dogUp.png", 5, 1, 0.1f));
            animations.add(new TextureData("textures/dogRight.png", 5, 1, 0.1f));
            animations.add(new TextureData("textures/dogDown.png", 5, 1, 0.1f));
            animations.add(new TextureData("textures/dogLeft.png", 5, 1, 0.1f));
            animations.add(new TextureData("textures/dogUpBorder.png", 5, 1, 0.1f));
            animations.add(new TextureData("textures/dogRightBorder.png", 5, 1, 0.1f));
            animations.add(new TextureData("textures/dogDownBorder.png", 5, 1, 0.1f));
            animations.add(new TextureData("textures/dogLeftBorder.png", 5, 1, 0.1f));

            enemyDog = new Enemy(new Size(64f, 64f), new Size(64f, 32f), new Size(32f, 64f), animations,
                new Vector2(2258, 2358), 5f, path);
            dogTriggerRectWest = new Rectangle(1910, 2298, 306, 314);
            dogTriggerRectSouth = new Rectangle(2430, 1992, 370, 102);
            PLAYER_INPUT_SESSION.setGameObjects(new ArrayList<>(Arrays.asList(PLAYER, enemyDog)));

            world = new World(new Vector2(0, 0), true);
            rayHandler = new RayHandler(world);
            rayHandler.setShadows(true);
            rayHandler.setAmbientLight(1f, 2f, 33f, 0.1f);

            final MapObjects objects = MapUtils.getCollisionLayer(MapRenderer.getInstance().getMap());

            // This adds all collision objects from TiledMap to box2d world
            for (final RectangleMapObject rectangle : objects.getByType(RectangleMapObject.class)) {
                if (rectangle.getName() == null) {
                    final Shape shape = getRectangle(rectangle);
                    final BodyDef bd = new BodyDef();
                    bd.type = BodyDef.BodyType.StaticBody;
                    final Body body = world.createBody(bd);
                    body.createFixture(shape, 1);
                    shape.dispose();
                }
            }
            coneLight = new ConeLight(rayHandler, 60, Color.GRAY, 1000, PLAYER.getPosition().x, PLAYER.getPosition().y, PLAYER.getRotation(), 20);
            coneLight.setStaticLight(false);
            coneLight.setSoft(true);
            coneLight.setSoftnessLength(80);

            //creating new items
            final Item bat = new Item(
                    new Vector2(2402, 1738),
                    new Size(48f, 48f),
                    new Texture("textures/BaseballBat.png"),
                    ItemType.WEARABLE,
                    ItemUse.WEARABLE_PLUS25, "Baseball bat", "Hit'em bois");

            PLAYER_INPUT_SESSION.setGameObjects(new ArrayList<>(Arrays.asList(PLAYER, enemyDog)));
            //adding items to list
            items.add(bat);

            // intro
            intro = true;
            lightsUp = true;
            introLight = new ArrayList<>();
            worldCamera.getCamera().position.set(3934, 2406, 0);
            WorldCamera.LERP = 0.15f;
            rayHandler.setAmbientLight(1f, 2f, 33f, 0.01f);

            final ConeLight dLetter1 = new ConeLight(rayHandler, 60, Color.GOLD, 350,
                worldCamera.getCamera().position.x - 1000, worldCamera.getCamera().position.y - 150, 90, 5);
            final ConeLight dLetter2 = new ConeLight(rayHandler, 60, Color.GOLD, 300,
                worldCamera.getCamera().position.x - 1000, worldCamera.getCamera().position.y - 150, 45, 5);
            final ConeLight dLetter3 = new ConeLight(rayHandler, 60, Color.GOLD, 300,
                worldCamera.getCamera().position.x - 1000, worldCamera.getCamera().position.y + 150, -45, 5);
            introLight.addAll(Arrays.asList(dLetter1, dLetter2, dLetter3));

            final ConeLight aLetter1 = new ConeLight(rayHandler, 60, Color.GOLD, 300,
                worldCamera.getCamera().position.x - 850, worldCamera.getCamera().position.y, 0, 5);
            final ConeLight aLetter2 = new ConeLight(rayHandler, 60, Color.GOLD, 350,
                worldCamera.getCamera().position.x - 850, worldCamera.getCamera().position.y - 150, 60, 5);
            final ConeLight aLetter3 = new ConeLight(rayHandler, 60, Color.GOLD, 350,
                worldCamera.getCamera().position.x - 750, worldCamera.getCamera().position.y + 150, -60, 5);
            introLight.addAll(Arrays.asList(aLetter1, aLetter2, aLetter3));

            final ConeLight rLetter1 = new ConeLight(rayHandler, 60, Color.GOLD, 350,
                worldCamera.getCamera().position.x - 600, worldCamera.getCamera().position.y - 150, 90, 5);
            final ConeLight rLetter2 = new ConeLight(rayHandler, 60, Color.GOLD, 200,
                worldCamera.getCamera().position.x - 600, worldCamera.getCamera().position.y + 120, -45, 5);
            final ConeLight rLetter3 = new ConeLight(rayHandler, 60, Color.GOLD, 200,
                worldCamera.getCamera().position.x - 600, worldCamera.getCamera().position.y - 40, 45, 5);
            final ConeLight rLetter4 = new ConeLight(rayHandler, 60, Color.GOLD, 250,
                worldCamera.getCamera().position.x - 600, worldCamera.getCamera().position.y, -45, 5);
            introLight.addAll(Arrays.asList(rLetter1, rLetter2, rLetter3, rLetter4));

            final ConeLight kLetter1 = new ConeLight(rayHandler, 60, Color.GOLD, 350,
                worldCamera.getCamera().position.x - 450, worldCamera.getCamera().position.y - 150, 90, 5);
            final ConeLight kLetter2 = new ConeLight(rayHandler, 60, Color.GOLD, 250,
                worldCamera.getCamera().position.x - 450, worldCamera.getCamera().position.y, -45, 5);
            final ConeLight kLetter3 = new ConeLight(rayHandler, 60, Color.GOLD, 250,
                worldCamera.getCamera().position.x - 450, worldCamera.getCamera().position.y, 45, 5);
            introLight.addAll(Arrays.asList(kLetter1, kLetter2, kLetter3));

            final ConeLight tLetter1 = new ConeLight(rayHandler, 60, Color.GOLD, 350,
                worldCamera.getCamera().position.x - 50, worldCamera.getCamera().position.y - 150, 90, 5);
            final ConeLight tLetter2 = new ConeLight(rayHandler, 60, Color.GOLD, 350,
                worldCamera.getCamera().position.x - 200, worldCamera.getCamera().position.y + 120, 0, 5);
            introLight.addAll(Arrays.asList(tLetter1, tLetter2));

            final ConeLight iLetter1 = new ConeLight(rayHandler, 60, Color.GOLD, 350,
                worldCamera.getCamera().position.x + 150, worldCamera.getCamera().position.y + 150, -90, 5);
            introLight.addAll(Collections.singletonList(iLetter1));

            final ConeLight mLetter1 = new ConeLight(rayHandler, 60, Color.GOLD, 350,
                worldCamera.getCamera().position.x + 250, worldCamera.getCamera().position.y - 150, 90, 5);
            final ConeLight mLetter2 = new ConeLight(rayHandler, 60, Color.GOLD, 250,
                worldCamera.getCamera().position.x + 250, worldCamera.getCamera().position.y + 120, -45, 5);
            final ConeLight mLetter3 = new ConeLight(rayHandler, 60, Color.GOLD, 250,
                worldCamera.getCamera().position.x + 450, worldCamera.getCamera().position.y + 120, -135, 5);
            final ConeLight mLetter4 = new ConeLight(rayHandler, 60, Color.GOLD, 350,
                worldCamera.getCamera().position.x + 450, worldCamera.getCamera().position.y - 150, 90, 5);
            introLight.addAll(Arrays.asList(mLetter1, mLetter2, mLetter3, mLetter4));

            final ConeLight eLetter1 = new ConeLight(rayHandler, 60, Color.GOLD, 350,
                worldCamera.getCamera().position.x + 550, worldCamera.getCamera().position.y - 150, 90, 5);
            final ConeLight eLetter2 = new ConeLight(rayHandler, 60, Color.GOLD, 250,
                worldCamera.getCamera().position.x + 520, worldCamera.getCamera().position.y + 120, 0, 5);
            final ConeLight eLetter3 = new ConeLight(rayHandler, 60, Color.GOLD, 250,
                worldCamera.getCamera().position.x + 520, worldCamera.getCamera().position.y, 0, 5);
            final ConeLight eLetter4 = new ConeLight(rayHandler, 60, Color.GOLD, 250,
                worldCamera.getCamera().position.x + 520, worldCamera.getCamera().position.y - 150, 0, 5);
            introLight.addAll(Arrays.asList(eLetter1, eLetter2, eLetter3, eLetter4));

            final ConeLight sLetter1 = new ConeLight(rayHandler, 60, Color.GOLD, 250,
                worldCamera.getCamera().position.x + 750, worldCamera.getCamera().position.y + 20, 45, 5);
            final ConeLight sLetter2 = new ConeLight(rayHandler, 60, Color.GOLD, 200,
                worldCamera.getCamera().position.x + 750, worldCamera.getCamera().position.y + 40, -45, 5);
            final ConeLight sLetter3 = new ConeLight(rayHandler, 60, Color.GOLD, 200,
                worldCamera.getCamera().position.x + 750, worldCamera.getCamera().position.y - 150, 45, 5);
            introLight.addAll(Arrays.asList(sLetter1, sLetter2, sLetter3));

            for (final ConeLight light : introLight) {
                light.setStaticLight(true);
                light.setSoft(true);
                light.setSoftnessLength(80);
                light.setDistance(1f);
            }
        }
    }

    private static PolygonShape getRectangle(final RectangleMapObject rectangleObject) {
        final Rectangle rectangle = rectangleObject.getRectangle();
        final PolygonShape polygon = new PolygonShape();
        final Vector2 size = new Vector2((rectangle.x + rectangle.width / 2), (rectangle.y + rectangle.height / 2));
        polygon.setAsBox(rectangle.width, rectangle.height, size, 0.0f);
        return polygon;
    }

    private boolean isInLight(final Rectangle rectangle) {
        return coneLight.contains(rectangle.x, rectangle.y);
    }

    @Override
    public void render(final float delta) {
        super.render(delta);
        Gdx.gl.glClearColor(1, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        PLAYER.update();
        enemyDog.update(PLAYER);
        worldCamera.followGameObject(PLAYER, delta);
        trigger.update(delta);

        worldCamera.update();
        MapRenderer.getInstance().render();
        SPRITE_BATCH.setProjectionMatrix(worldCamera.getCamera().combined);

        if (!firstDialogueShown && !DialogueManager.getInstance().getCurrentlyDisplayedDialogue().isPresent()) {
            firstDialogueShown = true;
            final DialogueOption optionA = new DialogueOption("Nic sie nie dzieje, to tylko przeziebienie, tatus pojdzie po leki do miasta");
            final DialogueOption optionB = new DialogueOption("Jestes powaznie chora, tatus musi isc po specjalne lekarstwo do lekarza");
            final Dialogue dialogue = new Dialogue("Bardzo zle sie czuje tato", Arrays.asList(optionA, optionB));
            DialogueManager.getInstance().displayDialogue(dialogue);
            secondDialogueShown = false;
        }

        if (!secondDialogueShown && !DialogueManager.getInstance().getCurrentlyDisplayedDialogue().isPresent()) {
            secondDialogueShown = true;
            final Dialogue dialogue = new Dialogue("*dziecko zasnelo*");
            DialogueManager.getInstance().displayDialogue(dialogue);
        }

        if (intro) {
            if (lightsUp) {
                for (final ConeLight light : introLight) {
                    light.setDistance(light.getDistance() + 1);
                    if (light.getDistance() > 400) {
                        light.setConeDegree(light.getConeDegree() + 0.45f);
                    }
                }
            }

            // D
            introLight.get(0).setPosition(worldCamera.getCamera().position.x - 1000, worldCamera.getCamera().position.y - 150);
            introLight.get(1).setPosition(worldCamera.getCamera().position.x - 1000, worldCamera.getCamera().position.y - 150);
            introLight.get(2).setPosition(worldCamera.getCamera().position.x - 1000, worldCamera.getCamera().position.y + 150);

            // A
            introLight.get(3).setPosition(worldCamera.getCamera().position.x - 850, worldCamera.getCamera().position.y);
            introLight.get(4).setPosition(worldCamera.getCamera().position.x - 850, worldCamera.getCamera().position.y - 150);
            introLight.get(5).setPosition(worldCamera.getCamera().position.x - 750, worldCamera.getCamera().position.y + 150);

            // R
            introLight.get(6).setPosition(worldCamera.getCamera().position.x - 600, worldCamera.getCamera().position.y - 150);
            introLight.get(7).setPosition(worldCamera.getCamera().position.x - 600, worldCamera.getCamera().position.y + 120);
            introLight.get(8).setPosition(worldCamera.getCamera().position.x - 600, worldCamera.getCamera().position.y - 40);
            introLight.get(9).setPosition(worldCamera.getCamera().position.x - 600, worldCamera.getCamera().position.y);

            // K
            introLight.get(10).setPosition(worldCamera.getCamera().position.x - 450, worldCamera.getCamera().position.y - 150);
            introLight.get(11).setPosition(worldCamera.getCamera().position.x - 450, worldCamera.getCamera().position.y);
            introLight.get(12).setPosition(worldCamera.getCamera().position.x - 450, worldCamera.getCamera().position.y);

            // T
            introLight.get(13).setPosition(worldCamera.getCamera().position.x - 50, worldCamera.getCamera().position.y - 150);
            introLight.get(14).setPosition(worldCamera.getCamera().position.x - 200, worldCamera.getCamera().position.y + 120);

            // I
            introLight.get(15).setPosition(worldCamera.getCamera().position.x + 150, worldCamera.getCamera().position.y + 150);

            // M
            introLight.get(16).setPosition(worldCamera.getCamera().position.x + 250, worldCamera.getCamera().position.y - 150);
            introLight.get(17).setPosition(worldCamera.getCamera().position.x + 250, worldCamera.getCamera().position.y + 120);
            introLight.get(18).setPosition(worldCamera.getCamera().position.x + 450, worldCamera.getCamera().position.y + 120);
            introLight.get(19).setPosition(worldCamera.getCamera().position.x + 450, worldCamera.getCamera().position.y - 150);

            // E
            introLight.get(20).setPosition(worldCamera.getCamera().position.x + 550, worldCamera.getCamera().position.y - 150);
            introLight.get(21).setPosition(worldCamera.getCamera().position.x + 520, worldCamera.getCamera().position.y + 120);
            introLight.get(22).setPosition(worldCamera.getCamera().position.x + 520, worldCamera.getCamera().position.y);
            introLight.get(23).setPosition(worldCamera.getCamera().position.x + 520, worldCamera.getCamera().position.y - 150);

            // S
            introLight.get(24).setPosition(worldCamera.getCamera().position.x + 750, worldCamera.getCamera().position.y + 20);
            introLight.get(25).setPosition(worldCamera.getCamera().position.x + 750, worldCamera.getCamera().position.y + 40);
            introLight.get(26).setPosition(worldCamera.getCamera().position.x + 750, worldCamera.getCamera().position.y - 150);

            for (final ConeLight light : introLight) {
                if (light.getConeDegree() > 80f) {
                    lightsUp = false;
                    light.setDistance(light.getDistance() - 4f);
                    rayHandler.setAmbientLight(1f, 2f, 33f, 0.05f);
                }
            }

            if (!lightsUp && introLight.get(0).getDistance() < 10f) {
                intro = false;
                for (final ConeLight light : introLight) {
                    light.setActive(false);
                }
                WorldCamera.LERP = 2.5f;
                rayHandler.setAmbientLight(1f, 2f, 33f, 0.1f);
                final Dialogue dialogue = new Dialogue("Nie przejmuj sie skarbenku, tatus wszystko naprawi...");
                DialogueManager.getInstance().displayDialogue(dialogue);
                firstDialogueShown = false;
            }
        }

        //check if player is taking item and add it to inventory (if there is space)
        for (final Item item : items) {
            if (PLAYER.getSpriteRectangle().overlaps(item.getSpriteRectangle()) && !INVENTORY.tooFull) {
                pickItemSound.play(1.0f);
                INVENTORY.addItem(item);
            }
        }

        world.step(delta, 8, 3);
        coneLight.setPosition(PLAYER.getPosition().x + 5, PLAYER.getPosition().y);
        coneLight.setDirection(PLAYER.getRotation());
        rayHandler.setCombinedMatrix(worldCamera.getCamera().combined);
        rayHandler.updateAndRender();
        SPRITE_BATCH.begin();
        PLAYER.draw(SPRITE_BATCH);

        //check if dog should get triggered
        if (PLAYER.getSpriteRectangle().overlaps(dogTriggerRectWest) || PLAYER.getSpriteRectangle().overlaps(dogTriggerRectSouth)) {
            enemyDog.setEnemyMode(EnemyMode.ANGRY);
        }

        //check if collided with a dog
        if (enemyDog.isPlayerHit()) {
            if (enemyDog.isVisible()) {
                enemyDog.setVisible(false);
                enemyDog.setEnemyMode(EnemyMode.NORMAL);
                enemyDog.setPosition(new Vector2(0, 0));
                DarkTimesApplication.APPLICATION.setScreen(ScreenManagerUtils.getScreen(GameScreenEnum.CHAPTER_ONE_DUEL));
            }
        }

        //drawing only if is in flashlight
        if (isInLight(enemyDog.getSpriteRectangle())) {
            enemyDog.draw(SPRITE_BATCH);
            enemyDog.setVisible(true);
        } else {
            enemyDog.setVisible(false);
            if (enemyDog.getEnemyMode() == EnemyMode.ANGRY) {
                enemyDog.draw(SPRITE_BATCH);
            }
        }

        //drawing only if is in flashlight
        for (final Item item : items) {
            if (!item.isInInventory() && isInLight(item.getSpriteRectangle())) {
                item.getSprite().draw(SPRITE_BATCH);
            }
        }
        SPRITE_BATCH.end();

        dialogueBatch.begin();
        if (dialogueManager.getCurrentlyDisplayedDialogue().isPresent()) {
            dialogueManager.draw(delta);
        }
        dialogueBatch.end();

        inventoryBatch.begin();
        if (inventoryManager.isOpened) {
            inventoryManager.getInventoryTable().setPosition(0, 0);
            inventoryManager.getInventoryStage().draw();
        }
        inventoryBatch.end();
    }

    @Override
    public void resize(final int width, final int height) {
        super.resize(width, height);
        worldCamera.resize(width, height);
        dialogueManager.resize(width, height);
        MIST_SHADER.resize(width, height);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void dispose() {
        super.dispose();
        enemyDog.dispose();
        coneLight.dispose();
    }
}
