package pl.polsl.angrygamers.screens.plot.triggers;

import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.objects.gameObjects.Enemy;


/**
 * The type Enemy trigger.
 */
public class EnemyTrigger extends GeofenceTrigger {

    private final Enemy enemy;

    /**
     * Instantiates a new Enemy trigger.
     *
     * @param enemy the enemy
     */
    public EnemyTrigger(Enemy enemy) {
        super(enemy.getPosition(), 30.0f);
        this.enemy = enemy;
        this.once();
    }

    protected Vector2 getPosition() {
        return enemy.getPosition();
    }
}
