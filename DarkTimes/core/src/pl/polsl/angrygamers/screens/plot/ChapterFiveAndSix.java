package pl.polsl.angrygamers.screens.plot;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.enums.GameScreenEnum;
import pl.polsl.angrygamers.enums.ItemType;
import pl.polsl.angrygamers.enums.ItemUse;
import pl.polsl.angrygamers.objects.Item;
import pl.polsl.angrygamers.objects.gameObjects.GameObject;
import pl.polsl.angrygamers.screens.plot.triggers.*;
import pl.polsl.angrygamers.utils.InventoryManager;
import pl.polsl.angrygamers.utils.Size;
import pl.polsl.angrygamers.utils.dialogues.DialogueBuilder;
import pl.polsl.angrygamers.utils.maps.MapEnum;
import pl.polsl.angrygamers.utils.resources.BonfireFactory;
import pl.polsl.angrygamers.utils.resources.NPCFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static pl.polsl.angrygamers.utils.GlobalVariables.PLAYER;


/**
 * The type Chapter five and six.
 */
public class ChapterFiveAndSix extends ChapterScreen {

    private final List<Vector2> bonfirePositions = new ArrayList<Vector2>() {
        {
            add(new Vector2(548.0f, 3768.0f));
        }
    };

    private List<ParticleEffect> particles;
    private final Vector2 bandagePosition = new Vector2(3092f, 3572f);
    private final Vector2 sneakersPosition = new Vector2(3716f, 1888f);
    private final Vector2 cannedFoodPosition = new Vector2(2264f, 936f);

    /**
     * Instantiates a new Chapter five and six.
     */
    public ChapterFiveAndSix() {
        particles = new ArrayList<>();
    }

    @Override
    protected MapEnum getMap() {
        return MapEnum.CHAPTER_5_AND_6;
    }

    @Override
    protected Vector2 getPlayerInitialPosition() {
        return new Vector2(314.0f, 3914.0f);
    }

    private final Trigger nightTrigger = new GeofenceTrigger(new Vector2(560.0f, 3868.0f), 200.0f) {
        @Override
        protected boolean triggerAction() {
            startActiveNight(4);
            addTrigger(new ScheduledTrigger(15.0f, () -> {
                stopActiveNight();
                addTrigger(new ScheduledTrigger(2.0f, () -> {
                    addTrigger(new ChangeMapTrigger(PLAYER.getPosition(), GameScreenEnum.CHAPTER_SEVEN));
                    return true;
                }));
                return true;
            }));
            return true;
        }
    }.once();

    private void addNightTrigger() {
        addTrigger(nightTrigger);
    }


    @Override
    protected void addGameObjects(List<GameObject> gameObjects) {
        final NPCFactory npcFactory = new NPCFactory();
        gameObjects.add(npcFactory.create(new Vector2(560.0f, 3868.0f)));

        addTrigger(new DialogueTrigger(new Vector2(560.0f, 3868.0f), 40.0f,
            new DialogueBuilder("Obudziles sie juz?!",
                "Kim jestes?",
                "Czemu mnie uderzyles?") {
                @Override
                public DialogueBuilder optionSelected(int option) {
                    switch (option) {
                        case 0:
                            return new DialogueBuilder("Jestem lokalnym znachorem",
                                "Ok...Ale czemu mnie uderzyles?", "Znachor? Moja corka jest chora, pomoz mi.") {
                                @Override
                                public DialogueBuilder optionSelected(int option) {
                                    switch (option) {
                                        case 0:
                                            return new DialogueBuilder("Tu sie roi od zlych ludzi, ktorzy chca zabic lokalnego znachora jak ja.",
                                                "Jestes znachorem? Mozesz mi pomoc?", "Potrzebuje lekarstwa...") {
                                                @Override
                                                public DialogueBuilder optionSelected(int option) {
                                                    return new DialogueBuilder("Niech zgadne... Masz chora corke. Ok pomoge") {
                                                        @Override
                                                        public DialogueBuilder optionSelected(int option) {
                                                            return new DialogueBuilder("Idz w las, poszukaj dla mnie przedmiotow. Tutaj masz liste. Wroc z nimi.", "Ok", "Tak jest szefie").onSelected(option1 ->{
                                                                InventoryManager.getInstance().getInventory().addItem(new Item(bandagePosition, new Size(48f, 48f), new Texture("textures/List.png"), ItemType.MISSION_ITEM, ItemUse.MISSION_ITEM, "Lista", "Potrzebne: 1. Stary bucior 2. Bandaze 3. Metalowe puszki"));
                                                                return null;
                                                            });
                                                        }
                                                    };
                                                }
                                            };
                                        case 1:
                                            return new DialogueBuilder("Chora corka, he? Ciezkie czasy na wychowanie dziecka... Pomoge ci.") {
                                                @Override
                                                public DialogueBuilder optionSelected(int option) {
                                                    return new DialogueBuilder("Idz w las, poszukaj dla mnie przedmiotow. Tutaj masz liste. Wroc z nimi.", "Ok", "Juz sie robi").onSelected(option1 -> {
                                                        InventoryManager.getInstance().getInventory().addItem(new Item(bandagePosition, new Size(48f, 48f), new Texture("textures/List.png"), ItemType.MISSION_ITEM, ItemUse.MISSION_ITEM, "Lista", "Potrzebne: 1. Stary bucior 2. Bandaze 3. Metalowe puszki"));
                                                        return null;
                                                    });
                                                }
                                            };
                                        default:
                                            return null;
                                    }
                                }

                            };
                        case 1:
                            return new DialogueBuilder("Heeeheee myslalem, ze chcesz mnie zabic, nie gniewaj sie",
                                "Eh... Dzieki", "Boliiiiii...") {
                                @Override
                                public DialogueBuilder optionSelected(int option) {
                                    return new DialogueBuilder("No dobra dobra, odwdziecze ci sie. Jestem znachorem. Potrzebujesz czegos?",
                                        "Potrzebuje lekarstwa, mam chora corke.", "Hmmm... tak, leku. Zaraza dopadla moja corke") {
                                        @Override
                                        public DialogueBuilder optionSelected(int option) {
                                            return new DialogueBuilder("Corke? Chora? Ok w takim razie ci pomoge...") {
                                                @Override
                                                public DialogueBuilder optionSelected(int option) {
                                                    return new DialogueBuilder("Masz tu liste, idz w las, znajdz mi te rzeczy i wroc tu z nimi", "Ok", "Dobrze").onSelected(option1 -> {
                                                        InventoryManager.getInstance().getInventory().addItem(new Item(bandagePosition, new Size(48f, 48f), new Texture("textures/List.png"), ItemType.MISSION_ITEM, ItemUse.MISSION_ITEM, "Lista", "Potrzebne: 1. Stary bucior 2. Bandaze 3. Metalowe puszki"));
                                                        return null;
                                                    });
                                                }
                                            };
                                        }
                                    };
                                }
                            };
                        default:
                            return null;
                    }
                }

            }).once());

        addTrigger(new LatchTrigger(Arrays.asList(bandagePosition, cannedFoodPosition, sneakersPosition),
            100.0f) {
            @Override
            protected boolean triggerAction() {
                addTrigger(new DialogueTrigger(new Vector2(560.0f, 3868.0f), 40.0f,
                    new DialogueBuilder("Masz juz przedmioty! Swietnie. Daj mi je.",
                        "Ok", "Prosze, uratuj moja corke") {
                        @Override
                        public DialogueBuilder optionSelected(int option) {
                            addNightTrigger();
                            return new DialogueBuilder("Powinienes isc spac. To troche zajmie. Odpocznij.", "Dobrze", "W sumie drzemka nie zaszkodzi");
                        }
                    }).once());
                return true;
            }
        });
    }


    @Override
    protected void addItems(List<Item> items) {
        //creating new items
        Item item = new Item(bandagePosition, new Size(48f, 48f), new Texture("textures/Bandage.png"), ItemType.MISSION_ITEM, ItemUse.POTION_ITEM, "Stary bandaz", "Znachorowi jest potrzebny do stworzenia lekarstwa... Ale czemu?");
        Item item2 = new Item(sneakersPosition, new Size(48f, 48f), new Texture("textures/Sneakers.png"), ItemType.MISSION_ITEM, ItemUse.POTION_ITEM, "Bucior", "Po co mu... jakis but?");
        Item item3 = new Item(cannedFoodPosition, new Size(48f, 48f), new Texture("textures/CannedFood.png"), ItemType.MISSION_ITEM, ItemUse.POTION_ITEM, "Puste puszki", "Zrobie wszystko dla mojej corki... Jesli znachor mowi, ze tego potrzeba, to mu przyniose");

        //adding items to list
        items.add(item);
        items.add(item2);
        items.add(item3);
    }

    @Override
    protected void addLightSources(RayHandler rayHandler) {
        for (Vector2 bonfirePosition : bonfirePositions) {
            addBonfire(rayHandler, bonfirePosition);
        }
    }

    private void addBonfire(RayHandler rayHandler, Vector2 pos) {
        PointLight p = new PointLight(rayHandler, 10, Color.FIREBRICK, 200.0f, pos.x, pos.y);
        p.setStaticLight(true);
        p.setSoft(true);

        final BonfireFactory bonfireFactory = new BonfireFactory();
        ParticleEffect particleEffect = bonfireFactory.create(pos);
        particles.add(particleEffect);
    }

    @Override
    protected void renderGameObjects(SpriteBatch batch, float delta) {
        super.renderGameObjects(batch, delta);
        particles.forEach(particleEffect -> particleEffect.draw(batch, delta));
    }

    @Override
    public void dispose() {
        super.dispose();
        particles.forEach(ParticleEffect::dispose);
    }
}
