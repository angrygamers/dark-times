package pl.polsl.angrygamers.screens.plot.triggers;

import com.badlogic.gdx.math.Vector2;

import static pl.polsl.angrygamers.utils.GlobalVariables.PLAYER;

/**
 * The type Geofence trigger.
 */
public class GeofenceTrigger extends BaseTrigger {

    private boolean triggered;
    private final Vector2 position;
    private final float distance;
    private int triggerCount = Integer.MAX_VALUE;

    /**
     * Instantiates a new Geofence trigger.
     *
     * @param position the position
     * @param distance the distance
     */
    public GeofenceTrigger(Vector2 position, float distance) {
        this.position = position;
        this.distance = distance;
    }

    /**
     * Gets position.
     *
     * @return the position
     */
    protected Vector2 getPosition() {
        return this.position;
    }

    @Override
    public void update(float delta) {
        boolean playerInside = PLAYER.getPosition().dst(getPosition()) <= distance;
        if(playerInside && !triggered) {
            if(triggerCount > 0) {
                triggered = triggerAction();
                if(triggered) {
                    triggerCount--;
                }
            }
        } else if(!playerInside) {
            triggered = false;
        }
    }

    /**
     * Once trigger.
     *
     * @return the trigger
     */
    public Trigger once() {
        triggerCount = 1;
        return this;
    }
}
