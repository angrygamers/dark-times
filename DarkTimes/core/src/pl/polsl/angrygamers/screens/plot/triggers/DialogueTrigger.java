package pl.polsl.angrygamers.screens.plot.triggers;

import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.utils.dialogues.DialogueBuilder;
import pl.polsl.angrygamers.utils.dialogues.DialogueManager;

/**
 * The type Dialogue trigger.
 */
public class DialogueTrigger extends GeofenceTrigger {

    private final DialogueBuilder originalDialogueBuilder;
    private DialogueBuilder dialogueBuilder;
    /**
     * The Dialogue manager.
     */
    final DialogueManager dialogueManager = DialogueManager.getInstance();
    private boolean isShowing;

    /**
     * Instantiates a new Dialogue trigger.
     *
     * @param position the position
     * @param distance the distance
     * @param message  the message
     */
    public DialogueTrigger(Vector2 position, float distance, String message) {
        this(position, distance, new DialogueBuilder(message));
    }

    /**
     * Instantiates a new Dialogue trigger.
     *
     * @param position        the position
     * @param distance        the distance
     * @param dialogueBuilder the dialogue builder
     */
    public DialogueTrigger(Vector2 position, float distance, DialogueBuilder dialogueBuilder) {
        super(position, distance);
        this.originalDialogueBuilder = dialogueBuilder;
    }

    @Override
    protected boolean triggerAction() {
        if(isShowing) {
            if(!dialogueManager.isDisplaying()) {
                int selectedOption = dialogueManager.getCurrentlySelectedOption();
                isShowing = false;
                dialogueBuilder = dialogueBuilder.optionSelected(selectedOption);
            }
        } else if(dialogueBuilder != null) {
            isShowing = true;
            dialogueManager.displayDialogue(dialogueBuilder.getDialogue());
        } else {
            dialogueBuilder = originalDialogueBuilder;
        }

        return dialogueBuilder == null;
    }
}
