package pl.polsl.angrygamers.screens.plot.triggers;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import static pl.polsl.angrygamers.utils.GlobalVariables.PLAYER;

/**
 * The type Latch trigger.
 */
public class LatchTrigger extends BaseTrigger {

    private boolean triggered;
    private final List<Vector2> positions;
    private Integer number;
    private float distance;

    /**
     * Instantiates a new Latch trigger.
     *
     * @param positions the positions
     * @param distance  the distance
     */
    public LatchTrigger(List<Vector2> positions, float distance) {
        this.positions = new ArrayList<>(positions);
        this.number = positions.size();
        this.distance = distance;
    }

    @Override
    public void update(float delta) {
        Vector2 positionToRemove = null;

        for (Vector2 position : positions) {
            boolean playerInside = PLAYER.getPosition().dst(position) <= distance;
            if (playerInside) {
                positionToRemove = position;
                number--;
            }
        }

        positions.remove(positionToRemove);

        if (number == 0 && !triggered) {
            triggered = triggerAction();
        }
    }
}
