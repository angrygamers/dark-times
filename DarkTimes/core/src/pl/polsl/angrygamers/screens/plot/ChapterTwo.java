package pl.polsl.angrygamers.screens.plot;

import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.objects.gameObjects.GameObject;
import pl.polsl.angrygamers.screens.plot.triggers.ChangeMapTrigger;
import pl.polsl.angrygamers.screens.plot.triggers.MessageTrigger;
import pl.polsl.angrygamers.screens.plot.triggers.ScheduledTrigger;
import pl.polsl.angrygamers.utils.maps.MapEnum;

import java.util.List;

import static pl.polsl.angrygamers.enums.GameScreenEnum.CHAPTER_THREE;

/**
 * The type Chapter two.
 */
public class ChapterTwo extends ChapterScreen {

    private static final float INIT_DIALOG_TIME_OFFSET = 3.0f;
    private static final float ACTIVE_NIGHT_START_TIME_OFFSET = INIT_DIALOG_TIME_OFFSET + 2.0f;
    private static final float ACTIVE_NIGHT_END_TIME_OFFSET = ACTIVE_NIGHT_START_TIME_OFFSET + 30.0f;
    private static final float CONTINUATION_DIALOG_OFFSET = ACTIVE_NIGHT_END_TIME_OFFSET + 3.0f;

    @Override
    protected MapEnum getMap() {
        return MapEnum.CHAPTER_2;
    }

    @Override
    protected Vector2 getPlayerInitialPosition() {
        return new Vector2(900.0f, 2390.0f);
    }

    @Override
    protected void addGameObjects(List<GameObject> gameObjects) {

        addTrigger(new MessageTrigger(INIT_DIALOG_TIME_OFFSET, "Tyle drogi juz za mna... nie dam rady dalej bez odpoczynku..."));
        addTrigger(new ScheduledTrigger(INIT_DIALOG_TIME_OFFSET, () -> {
            setTorchPower(300.0f);
            return true;
        }));

        addTrigger(new ScheduledTrigger(ACTIVE_NIGHT_START_TIME_OFFSET, () -> {
            startActiveNight();
            return true;
        }));

        addTrigger(new ScheduledTrigger(ACTIVE_NIGHT_END_TIME_OFFSET, () -> {
            stopActiveNight();
            return true;
        }));

        addTrigger(new ScheduledTrigger(ACTIVE_NIGHT_END_TIME_OFFSET, () -> {
            setTorchPower(1000.0f);
            return true;
        }));

        addTrigger(new MessageTrigger(CONTINUATION_DIALOG_OFFSET, "Co za okropna noc... Czas ruszac droga dalej, musze odnalezc lekarstwo!"));

        addTrigger(new ChangeMapTrigger(new Vector2(3762.0f, 2388.0f), CHAPTER_THREE));
    }
}
