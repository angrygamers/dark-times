package pl.polsl.angrygamers.screens.plot;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.enums.GameScreenEnum;
import pl.polsl.angrygamers.enums.ItemType;
import pl.polsl.angrygamers.enums.ItemUse;
import pl.polsl.angrygamers.objects.Item;
import pl.polsl.angrygamers.objects.gameObjects.GameObject;
import pl.polsl.angrygamers.objects.gameObjects.QuackEnemy;
import pl.polsl.angrygamers.screens.plot.triggers.*;
import pl.polsl.angrygamers.utils.Size;
import pl.polsl.angrygamers.utils.maps.MapEnum;

import java.util.List;

import static pl.polsl.angrygamers.utils.GlobalVariables.PLAYER;

/**
 * The type Chapter four.
 */
public class ChapterFour extends ChapterScreen {

    private final Vector2 shoesPosition = new Vector2(942f, 2326f);
    private final Vector2 backpackPosition = new Vector2(1204f, 1780f);
    private final Vector2 capPosition = new Vector2(1846f, 1514f);

    private Sound hitSound = Gdx.audio.newSound(Gdx.files.internal("sounds/knife_hit.wav"));

    @Override
    protected MapEnum getMap() {
        return MapEnum.CHAPTER_4;
    }

    @Override
    protected Vector2 getPlayerInitialPosition() {
        return new Vector2(116f, 2400f);
    }

    @Override
    protected void addGameObjects(List<GameObject> gameObjects) {
        super.addGameObjects(gameObjects);

        addTrigger(new MessageTrigger(1.5f, "Musze odnalezc tego znachora! Tylko ktoredy mam isc?"));
        addTrigger(new MessageTrigger(3.0f, "Cos mi mowi ze musze podazac za tymi przedmiotami..."));

        addTrigger(new DialogueTrigger(shoesPosition, 80.0f, "Hmmm... Buty sportowe?"));
        addTrigger(new DialogueTrigger(backpackPosition, 80.0f, "Plecak? Skad tu tyle przedmiotow?"));
        addTrigger(new DialogueTrigger(capPosition, 80.0f, "To miejsce musialo kiedys tetnic zyciem!"));

        addTrigger(new GeofenceTrigger(new Vector2(2832f, 1028f), 300.0f) {
            @Override
            protected boolean triggerAction() {
                final Vector2 quackPosition = PLAYER.getPosition().cpy().add(-250f, -200f);
                final QuackEnemy quackEnemy = new QuackEnemy(quackPosition);
                gameObjects.add(quackEnemy);
                addTrigger(new MessageTrigger(1.0f, "Chwila, co się dzieje..."));

                addTrigger(new EnemyTrigger(quackEnemy) {
                    @Override
                    protected boolean triggerAction() {
                        hitSound.play();
                        gameObjects.remove(quackEnemy);
                        setAmbientColor(new Color(0.0f, 0.0f, 0.0f, 0.0f), 1.0f);
                        setTorchPower(0.0f);
                        addTrigger(new ScheduledTrigger(1.0f, () -> {
                            startActiveNight(7);
                            setAmbientColor(new Color(0.0f, 0.0f, 0.0f, 0.2f), 0.5f);
                            setTorchPower(50.0f);

                            addTrigger(new ScheduledTrigger(15.0f, () -> {
                                stopActiveNight();
                                addTrigger(new ScheduledTrigger(2.0f, () -> {
                                    addTrigger(new ChangeMapTrigger(PLAYER.getPosition(), GameScreenEnum.CHAPTER_FIVE_AND_SIX));
                                    return true;
                                }));
                                return true;
                            }));
                            return true;
                        }));
                        return true;
                    }
                });
                return true;
            }
        }.once());

        addTrigger(new ScheduledTrigger(200.0f, () -> true));
    }

    @Override
    protected void addItems(List<Item> items) {
        final Item shoes = new Item(
                shoesPosition,
                new Size(48f, 48f),
                new Texture("textures/RunningShoes.png"),
                ItemType.WEARABLE,
                ItemUse.WEARABLE_SPEED_PLUS10, "Sportowe buty", "Buty firmy Nike");

        final Item backpack = new Item(
                backpackPosition,
                new Size(48f, 48f),
                new Texture("textures/Backpack.png"),
                ItemType.WEARABLE,
                ItemUse.WEARABLE_NO_EFFECT, "Placek", "Plecak firmy Quechua");

        final Item cap = new Item(
                capPosition,
                new Size(48f, 48f),
                new Texture("textures/Cap.png"),
                ItemType.WEARABLE,
                ItemUse.WEARABLE_NO_EFFECT, "Czapka", "Czapka kibica Chicago Bulls");

        items.add(shoes);
        items.add(backpack);
        items.add(cap);
    }

    @Override
    protected void renderGameObjects(SpriteBatch batch, float delta) {
        super.renderGameObjects(batch, delta);
        gameObjects.stream().filter(gameObject -> gameObject instanceof QuackEnemy)
                .forEach(gameObject -> gameObject.draw(batch));
    }
}
