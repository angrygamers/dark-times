package pl.polsl.angrygamers.screens.plot;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.enums.GameScreenEnum;
import pl.polsl.angrygamers.objects.gameObjects.GameObject;
import pl.polsl.angrygamers.screens.plot.triggers.ChangeMapTrigger;
import pl.polsl.angrygamers.screens.plot.triggers.DialogueTrigger;
import pl.polsl.angrygamers.screens.plot.triggers.MessageTrigger;
import pl.polsl.angrygamers.screens.plot.triggers.Trigger;
import pl.polsl.angrygamers.utils.VectorUtils;
import pl.polsl.angrygamers.utils.dialogues.DialogueBuilder;
import pl.polsl.angrygamers.utils.maps.MapEnum;
import pl.polsl.angrygamers.utils.resources.BonfireFactory;
import pl.polsl.angrygamers.utils.resources.NPCFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Chapter three.
 */
public class ChapterThree extends ChapterScreen {

    private final float NPC_BONFIRE_DISTANCE = 90.0f;

    private final List<BonfireInfo> bonfires = new ArrayList<BonfireInfo>() {
        {
            add(new BonfireInfo(new Vector2(782.0f, 2590.0f), 2));
            add(new BonfireInfo(new Vector2(944.0f, 2160.0f), 1));
            add(new BonfireInfo(new Vector2(1696.0f, 2532.0f), 2));
            add(new BonfireInfo(new Vector2(1920.0f, 2644.0f), 0));
            add(new BonfireInfo(new Vector2(2068.0f, 2244.0f), 2));
            add(new BonfireInfo(new Vector2(1714.0f, 2202.0f), 0));
            add(new BonfireInfo(new Vector2(2784.0f, 2152.0f), 2));
            add(new BonfireInfo(new Vector2(2804.0f, 2536.0f), 1));
            add(new BonfireInfo(new Vector2(2656.0f, 2636.0f), 0));
            add(new BonfireInfo(new Vector2(2456.0f, 2544.0f), 1));
            add(new BonfireInfo(new Vector2(3108.0f, 2216.0f), 1));
            add(new BonfireInfo(new Vector2(3466.0f, 2634.0f), 2));
        }
    };

    /**
     * The Leader position.
     */
    final Vector2 leaderPosition = new Vector2(3024.0f, 3384.0f);

    private List<ParticleEffect> particles;

    private final Trigger exitTrigger = new ChangeMapTrigger(new Vector2(3868.0f, 2388.0f),
            GameScreenEnum.CHAPTER_FOUR);

    /**
     * Instantiates a new Chapter three.
     */
    public ChapterThree() {
        particles = new ArrayList<>();
    }

    @Override
    protected MapEnum getMap() {
        return MapEnum.CHAPTER_3;
    }

    @Override
    protected Vector2 getPlayerInitialPosition() {
        return new Vector2(320.0f, 2400.0f);
    }

    @Override
    protected void addGameObjects(List<GameObject> gameObjects) {
        final NPCFactory npcFactory = new NPCFactory();

        for (BonfireInfo bonfireInfo : bonfires) {
            int npcCount = bonfireInfo.npcCount;
            for (int i = 0; i < npcCount; i++) {
                final Vector2 npcPos = VectorUtils.getRandomVectorInDistance(bonfireInfo.position, NPC_BONFIRE_DISTANCE);
                gameObjects.add(npcFactory.create(npcPos));
            }
        }

        gameObjects.add(npcFactory.create(leaderPosition));

        addTrigger(new MessageTrigger(
                3.0f,
                "Wchodzisz do miasta... Teraz juz wiesz co moze wywolac choroba... Czas porozmawiac z mieszkancami.."));

        addTrigger(new DialogueTrigger(bonfires.get(2).position,
                100.0f,
                "Ta choroba jest okropna, zdziesiatkowala nas kompletnie!").once());

        addTrigger(new DialogueTrigger(bonfires.get(4).position,
                130.0f,
                new DialogueBuilder("Gdyby tylko istnial jakis sposob na to, zeby temu wszystkiemu zaradzic...",
                        "Co prawda, to prawda...", "Mam chora corke, pomuz mi...")
                        .onSelected(option -> {
                            if (option == 1) {
                                return new DialogueBuilder("Przepraszam, nie wiedzialem... Odszukaj naszego przywodce, powinien byc w ktoryms z domow. Moze on wie cos wiecej...");
                            }
                            return null;
                        })));

        addTrigger(new DialogueTrigger(leaderPosition,
                100.0f,
                new DialogueBuilder("Czego ode mnie chcesz?",
                        "Mam chora corke...", "Czy ty jestes liderem?")
                        .onSelected(option -> {
                            switch (option) {
                                case 0: return new DialogueBuilder("Bardzo mi przykro...");
                                case 1: return new DialogueBuilder("Tak",
                                        "Tez kiedys bylem liderem!",
                                        "Pomoz mi prosze, nie mam nic do stracenia, moja corka umiera...")
                                        .onSelected(option1 -> {
                                           switch (option1) {
                                               case 1: {
                                                   addExitTrigger();
                                                   return new DialogueBuilder("Slyszalem, ze poza miastem jest znachor, ktory podobno wie jak zwalczac chorobe. Szukaj go na wschodzie...");
                                               }
                                               default: return null;
                                           }
                                        });
                                default: return null;
                            }
                        })));
    }

    private void addExitTrigger() {
        addTrigger(exitTrigger);
    }

    @Override
    protected void addLightSources(RayHandler rayHandler) {
        bonfires.stream().map(BonfireInfo::getPosition).forEach(position -> addBonfire(rayHandler, position));
    }

    private void addBonfire(RayHandler rayHandler, Vector2 pos) {
        PointLight p = new PointLight(rayHandler, 10, Color.FIREBRICK, 200.0f, pos.x, pos.y);
        p.setStaticLight(true);
        p.setSoft(true);

        final BonfireFactory bonfireFactory = new BonfireFactory();
        ParticleEffect particleEffect = bonfireFactory.create(pos);
        particles.add(particleEffect);
    }

    @Override
    protected void renderGameObjects(SpriteBatch batch, float delta) {
        super.renderGameObjects(batch, delta);
        particles.forEach(particleEffect -> particleEffect.draw(batch, delta));
    }

    @Override
    public void dispose() {
        super.dispose();
        particles.forEach(ParticleEffect::dispose);
    }

    private static class BonfireInfo {
        private Vector2 position;
        private int npcCount;

        /**
         * Instantiates a new Bonfire info.
         *
         * @param position the position
         * @param npcCount the npc count
         */
        public BonfireInfo(Vector2 position, int npcCount) {
            this.position = position;
            this.npcCount = npcCount;
        }

        /**
         * Gets position.
         *
         * @return the position
         */
        public Vector2 getPosition() {
            return position;
        }

        /**
         * Sets position.
         *
         * @param position the position
         */
        public void setPosition(Vector2 position) {
            this.position = position;
        }
    }
}
