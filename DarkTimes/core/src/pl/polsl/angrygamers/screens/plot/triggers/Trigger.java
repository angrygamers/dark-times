package pl.polsl.angrygamers.screens.plot.triggers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * The interface Trigger.
 */
public interface Trigger {
    /**
     * Update.
     *
     * @param delta the delta
     */
    void update(float delta);

    /**
     * Render.
     *
     * @param batch the batch
     * @param delta the delta
     */
    void render(SpriteBatch batch, float delta);
}
