package pl.polsl.angrygamers.screens.plot;

import box2dLight.ConeLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.actions.ColorAction;
import pl.polsl.angrygamers.PlayerInput;
import pl.polsl.angrygamers.objects.Item;
import pl.polsl.angrygamers.objects.gameObjects.GameObject;
import pl.polsl.angrygamers.objects.gameObjects.Ghost;
import pl.polsl.angrygamers.objects.gameObjects.PlayerObserver;
import pl.polsl.angrygamers.screens.plot.triggers.Trigger;
import pl.polsl.angrygamers.shaders.MistShader;
import pl.polsl.angrygamers.utils.*;
import pl.polsl.angrygamers.utils.dialogues.DialogueManager;
import pl.polsl.angrygamers.utils.maps.MapEnum;
import pl.polsl.angrygamers.utils.maps.MapRenderer;
import pl.polsl.angrygamers.utils.maps.MapUtils;

import java.util.ArrayList;
import java.util.List;

import static pl.polsl.angrygamers.utils.GlobalVariables.*;


/**
 * The type Chapter screen.
 */
public abstract class ChapterScreen extends ScreenAdapter {

    /**
     * The constant DEFAULT_TORCH_POWER.
     */
    public static final float DEFAULT_TORCH_POWER = 1000.0f;
    private final Sound pickItemSound = Gdx.audio.newSound(Gdx.files.internal("sounds/pick_item.wav"));

    private WorldCamera worldCamera;
    private World world;
    private MapRenderer mapRenderer;

    private final MistShader mistShader;

    private SpriteBatch dialogueBatch;
    private DialogueManager dialogueManager;

    private InventoryManager inventoryManager;
    private SpriteBatch inventoryBatch;

    private RayHandler rayHandler;

    private ConeLight torchLight;
    /**
     * The Game objects.
     */
    protected List<GameObject> gameObjects;
    private List<Item> items;
    private List<Trigger> triggers;
    private List<Trigger> pendingTriggers;
    private float targetTorchPower = DEFAULT_TORCH_POWER;
    private Color ambientColor = new Color(1f, 1.0f, 1.0f, 0.1f);
    private ColorAction colorAction = new ColorAction();
    private boolean rayHandlerEnabled = true;

    /**
     * The Ghost count.
     */
    int ghostCount = 3;

    /**
     * Instantiates a new Chapter screen.
     */
    ChapterScreen() {
        gameObjects = new ArrayList<>();
        items = new ArrayList<>();
        triggers = new ArrayList<>();
        pendingTriggers = new ArrayList<>();
        mistShader = MIST_SHADER;
        colorAction.setColor(ambientColor);
        colorAction.setEndColor(ambientColor);
    }

    /**
     * Sets torch power.
     *
     * @param power the power
     */
    protected void setTorchPower(float power) {
        targetTorchPower = power;
    }

    /**
     * Sets ambient color.
     *
     * @param color    the color
     * @param duration the duration
     */
    protected void setAmbientColor(Color color, float duration) {
        colorAction.reset();
        colorAction.setDuration(duration);
        colorAction.setColor(ambientColor);
        colorAction.setEndColor(color);
    }

    /**
     * Gets dialogue manager.
     *
     * @return the dialogue manager
     */
    protected DialogueManager getDialogueManager() {
        return this.dialogueManager;
    }

    /**
     * Gets map.
     *
     * @return the map
     */
    protected abstract MapEnum getMap();

    /**
     * Gets player initial position.
     *
     * @return the player initial position
     */
    protected abstract Vector2 getPlayerInitialPosition();

    private static PolygonShape getRectangle(final RectangleMapObject rectangleObject) {
        final Rectangle rectangle = rectangleObject.getRectangle();
        final PolygonShape polygon = new PolygonShape();
        final Vector2 size = new Vector2((rectangle.x + rectangle.width / 2), (rectangle.y + rectangle.height / 2));
        polygon.setAsBox(rectangle.width, rectangle.height, size, 0.0f);
        return polygon;
    }

    @Override
    public void show() {
        super.show();
        initializeScreenObjects();
        setupLightSources();
        initializeGameObjects();
        setupMap();
    }

    private void initializeScreenObjects() {
        worldCamera = WorldCamera.getInstance();
        mapRenderer = MapRenderer.getInstance();
        world = new World(new Vector2(0, 0), true);
        dialogueManager = DialogueManager.getInstance();
        dialogueBatch = new SpriteBatch();
        inventoryManager = InventoryManager.getInstance();
        inventoryBatch = new SpriteBatch();
    }

    private void initializeGameObjects() {
        final Vector2 position = getPlayerInitialPosition();
        PLAYER.setPosition(position);
        addGameObjects(gameObjects);
        addItems(items);
        PlayerInput.PLAYER_INPUT_SESSION.setGameObjects(gameObjects);
    }

    /**
     * Add trigger.
     *
     * @param trigger the trigger
     */
    final void addTrigger(final Trigger trigger) {
        this.pendingTriggers.add(trigger);
    }

    /**
     * Add game objects.
     *
     * @param gameObjects the game objects
     */
    protected void addGameObjects(final List<GameObject> gameObjects) {
    }

    /**
     * Add inventory items.
     *
     * @param items the items
     */
    protected void addItems(final List<Item> items) {
    }

    private void setupMap() {
        final MapEnum map = getMap();
        mapRenderer.changeMap(map);
        final MapObjects objects = MapUtils.getCollisionLayer(mapRenderer.getMap());
        addCollisionObjects(objects);
    }

    private void setupLightSources() {
        rayHandler = new RayHandler(world);
        rayHandler.removeAll();
        rayHandler.setShadows(true);
        rayHandler.setAmbientLight(ambientColor);
        rayHandler.setBlurNum(3);

        torchLight = new ConeLight(rayHandler, 60, Color.GRAY, DEFAULT_TORCH_POWER,
                PLAYER.getPosition().x,
                PLAYER.getPosition().y,
                PLAYER.getRotation(), 20);

        torchLight.setStaticLight(false);
        torchLight.setSoft(true);
        torchLight.setSoftnessLength(80);
        addLightSources(rayHandler);
    }

    /**
     * Add light sources.
     *
     * @param rayHandler the ray handler
     */
    protected void addLightSources(final RayHandler rayHandler) {
    }

    private void addCollisionObjects(final MapObjects objects) {
        for (final RectangleMapObject rectangle : objects.getByType(RectangleMapObject.class)) {
            if (rectangle.getName() == null) {
                final Shape shape = getRectangle(rectangle);
                final BodyDef bd = new BodyDef();
                bd.type = BodyDef.BodyType.StaticBody;
                final Body body = world.createBody(bd);
                body.createFixture(shape, 1);
                shape.dispose();
            }
        }
    }

    /**
     * Start active night.
     */
    void startActiveNight() {
        mistShader.startMistShader();
    }

    /**
     * Start active night.
     *
     * @param ghostsCount the ghosts count
     */
    protected void startActiveNight(int ghostsCount) {
        this.ghostCount = ghostsCount;
        startActiveNight();
    }

    /**
     * Stop active night.
     */
    protected void stopActiveNight() { mistShader.stopMistShader(); }

    @Override
    public void render(final float delta) {
        super.render(delta);
        Gdx.gl.glClearColor(1, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        colorAction.act(delta);
        rayHandler.setAmbientLight(colorAction.getColor());
        worldCamera.update();
        mapRenderer.render();
        worldCamera.followGameObject(PLAYER, delta);

        updateGameObjects(delta);
        updateTriggers(delta);

        SPRITE_BATCH.setProjectionMatrix(worldCamera.getCamera().combined);

        world.step(delta, 8, 3);

        torchLight.setPosition(PLAYER.getPosition().x + 5, PLAYER.getPosition().y);
        torchLight.setDirection(PLAYER.getRotation());
        if(torchLight.getDistance() != targetTorchPower) {
            torchLight.setDistance(torchLight.getDistance() + (targetTorchPower - torchLight.getDistance()) * delta);
        }

        if(rayHandlerEnabled) {
            rayHandler.setCombinedMatrix(worldCamera.getCamera().combined);
            rayHandler.updateAndRender();
        }

        SPRITE_BATCH.begin();
        PLAYER.draw(SPRITE_BATCH);
        renderGameObjects(SPRITE_BATCH, delta);
        renderTriggers(SPRITE_BATCH, delta);

        SPRITE_BATCH.end();

        renderActiveNight();

        dialogueBatch.begin();
        if (dialogueManager.getCurrentlyDisplayedDialogue().isPresent()) {
            dialogueManager.draw(delta);
        }
        dialogueBatch.end();

        inventoryBatch.begin();
        if (inventoryManager.isOpened) {
            inventoryManager.getInventoryTable().setPosition(0, 0);
            inventoryManager.getInventoryStage().draw();
        }
        inventoryBatch.end();
    }

    private void updateTriggers(final float delta) {
        triggers.addAll(pendingTriggers);
        pendingTriggers.clear();

        triggers.forEach(trigger -> trigger.update(delta));
    }

    private void updateGameObjects(final float delta) {
        if (!MIST_SHADER.isActive()) {
            PLAYER.update();
        }

        gameObjects.forEach(gameObject -> {
            if (gameObject instanceof PlayerObserver) {
                ((PlayerObserver) gameObject).update(PLAYER);
            }
            if (gameObject instanceof Ghost) {
                if (((Ghost) gameObject).checkIsPlayerAttacked()) {
                    MIST_SHADER.progressMist(0.1f, 1500);
                }
            }
        });

        items.forEach(item -> {
            final boolean canAddItem = PLAYER.getSpriteRectangle().overlaps(item.getSpriteRectangle()) && !INVENTORY.tooFull;
            if (canAddItem) {
                pickItemSound.play(1.0f);
                INVENTORY.addItem(item);
            }
        });
    }

    /**
     * Render game objects.
     *
     * @param batch the batch
     * @param delta the delta
     */
    protected void renderGameObjects(final SpriteBatch batch, final float delta) {

        gameObjects.stream()
            .filter(gameObject -> isInLight(gameObject.getSpriteRectangle()) || gameObject instanceof Ghost)
            .forEach(gameObject -> gameObject.draw(batch));

        items.stream()
            .filter(item -> !item.isInInventory() && isInLight(item.getSpriteRectangle()))
            .forEach(item -> item.getSprite().draw(batch));
    }

    private void renderTriggers(final SpriteBatch spriteBatch, final float delta) {
        triggers.forEach(trigger -> trigger.render(spriteBatch, delta));
    }

    private boolean isInLight(final Rectangle rectangle) {
        return rayHandler.pointAtLight(rectangle.x, rectangle.y);
    }

    /**
     * Render active night.
     */
    protected void renderActiveNight() {
        if (!MIST_SHADER.isActive()) {
            return;
        }

        int currentGhosts = 0;
        for (final GameObject go : gameObjects) {
            if (go instanceof Ghost) {
                currentGhosts++;
            }
        }

        if (currentGhosts < ghostCount && !MIST_SHADER.isStopping()) {
            final Vector2 ghostPosition = VectorUtils.getRandomVectorInDistance(
                PLAYER.getPosition(), 500.0f);

            final Ghost ghost = new Ghost(new Size(100f, 100f),
                new TextureData("textures/enemy-sheet.png",
                    3, 5, 0.2f),
                ghostPosition, 1f, null);
            gameObjects.add(ghost);
        }

        MIST_SHADER.displayMistShader();

        gameObjects.removeIf(gameObject -> {
            if (gameObject instanceof Ghost) {
                return ((Ghost) gameObject).checkIsGhostDismissed();
            }
            return false;
        });

        if (MIST_SHADER.isStopping()) {
            gameObjects.removeIf(gameObject -> gameObject instanceof Ghost);
        }
    }

    @Override
    public void resize(final int width, final int height) {
        super.resize(width, height);
        worldCamera.resize(width, height);
        dialogueManager.resize(width, height);
        MIST_SHADER.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
        torchLight.dispose();
    }

    /**
     * Gets ray handler.
     *
     * @return the ray handler
     */
    RayHandler getRayHandler() {
        return rayHandler;
    }

    /**
     * Is ray handler enabled boolean.
     *
     * @return the boolean
     */
    public boolean isRayHandlerEnabled() {
        return rayHandlerEnabled;
    }

    /**
     * Gets cone light.
     *
     * @return the cone light
     */
    ConeLight getConeLight() {
        return this.torchLight;
    }

    /**
     * Sets ray handler enabled boolean.
     *
     * @param rayHandlerEnabled the ray handler enabled
     * @return the ray handler enabled
     */
    ChapterScreen setRayHandlerEnabled(final boolean rayHandlerEnabled) {
        this.rayHandlerEnabled = rayHandlerEnabled;
        return this;
    }

    /**
     * Gets game objects.
     *
     * @return the game objects
     */
    public List<GameObject> getGameObjects() {
        return gameObjects;
    }
}
