package pl.polsl.angrygamers.screens.plot.triggers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * The type Base trigger.
 */
public abstract class BaseTrigger implements Trigger {

    @Override
    public void update(float delta) {
    }

    @Override
    public void render(SpriteBatch batch, float delta) {
    }

    /**
     * Trigger action boolean.
     *
     * @return Flaga informująca o tym, czy trigger został wywołany.
     */
    protected boolean triggerAction() {
        return true;
    }
}
