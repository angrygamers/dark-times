package pl.polsl.angrygamers.screens.plot.triggers;

/**
 * The type Scheduled trigger.
 */
public class ScheduledTrigger extends BaseTrigger {

    private final Trigger trigger;
    private boolean triggered;
    private float timespan;

    /**
     * Instantiates a new Scheduled trigger.
     *
     * @param timespan the timespan
     * @param trigger  the trigger
     */
    public ScheduledTrigger(float timespan, Trigger trigger) {
        this.timespan = timespan;
        this.trigger = trigger;
    }

    @Override
    public void update(float delta) {
        timespan -= delta;
        if(timespan < 0 && !triggered) {
            triggered = triggerAction();
        }
    }

    @Override
    protected boolean triggerAction() {
        return trigger.trigger();
    }

    /**
     * The interface Trigger.
     */
    public interface Trigger {
        /**
         * Trigger boolean.
         *
         * @return the boolean
         */
        boolean trigger();
    }
}
