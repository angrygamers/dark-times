package pl.polsl.angrygamers.screens.plot;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.DarkTimesApplication;
import pl.polsl.angrygamers.enums.GameScreenEnum;
import pl.polsl.angrygamers.objects.gameObjects.GameObject;
import pl.polsl.angrygamers.objects.gameObjects.Ghost;
import pl.polsl.angrygamers.screens.plot.triggers.DialogueTrigger;
import pl.polsl.angrygamers.screens.plot.triggers.MessageTrigger;
import pl.polsl.angrygamers.utils.ScreenManagerUtils;
import pl.polsl.angrygamers.utils.Size;
import pl.polsl.angrygamers.utils.TextureData;
import pl.polsl.angrygamers.utils.VectorUtils;
import pl.polsl.angrygamers.utils.dialogues.DialogueBuilder;
import pl.polsl.angrygamers.utils.maps.MapEnum;
import pl.polsl.angrygamers.utils.maps.MapRenderer;
import pl.polsl.angrygamers.utils.resources.BonfireFactory;
import pl.polsl.angrygamers.utils.resources.NPCFactory;

import java.util.ArrayList;
import java.util.List;

import static pl.polsl.angrygamers.utils.GlobalVariables.MIST_SHADER;
import static pl.polsl.angrygamers.utils.GlobalVariables.PLAYER;

/**
 * The type Chapter seven.
 */
public class ChapterSeven extends ChapterScreen {

    private float elapsedTime;
    private float runningHomeElapsedTime;
    private final float totalBlackeningTime = 4;
    private final List<ParticleEffect> particles;
    private boolean shouldRunHome = false;

    private final float newGhostTimer = 5;
    private float elapsedGhostTimer;
    private int ghostCount = 3;

    private boolean dead;

    private final List<Vector2> bonfirePositions = new ArrayList<Vector2>() {
        {
            add(new Vector2(548.0f, 3768.0f));
        }
    };
    private boolean messageShown;

    /**
     * Instantiates a new Chapter seven.
     */
    public ChapterSeven() {
        particles = new ArrayList<>();
    }

    @Override
    protected MapEnum getMap() {
        return MapEnum.CHAPTER_5_AND_6;
    }

    @Override
    protected Vector2 getPlayerInitialPosition() {
        return new Vector2(314.0f, 3914.0f);
    }

    @Override
    protected void addGameObjects(final List<GameObject> gameObjects) {
        final NPCFactory npcFactory = new NPCFactory();
        gameObjects.add(npcFactory.create(new Vector2(560.0f, 3868.0f)));

        elapsedTime = 0;

        addTrigger(new MessageTrigger(
            totalBlackeningTime,
            "Obudziles sie w jaskini znachora. Learstwo dla twojej chorej corki powinno byc juz gotowe"));

        addTrigger(new DialogueTrigger(new Vector2(560.0f, 3868.0f), 40.0f,
            new DialogueBuilder("Wstales, super! Lekarstwo gotowe.",
                "Super, biore to lekarstwo i biegne do corki", "Tak, dziekuje ci dobry czlowieku, dziekuje ci") {
                @Override
                public DialogueBuilder optionSelected(final int option) {
                    return new DialogueBuilder("Biegnij, biegnij! Byle szybko, lekarstwo ma krotki termin przydatnosci do spozycia",
                        "Ok, dziekuje, jestes wspanialym znachorem, Znachorze", "Hmmmmmmmmmmmmmmmmm... ok").onSelected(option1 -> {
                        shouldRunHome = true;
                        runningHomeElapsedTime = 0;
                        return null;
                    });
                }
            }).once());
    }

    @Override
    protected void renderGameObjects(final SpriteBatch batch, final float delta) {
        super.renderGameObjects(batch, delta);
        elapsedGhostTimer += delta;
        particles.forEach(particleEffect -> particleEffect.draw(batch, delta));
        if (elapsedTime < totalBlackeningTime) {
            elapsedTime += delta;
            setAmbientColor(new Color(1f, 2f, 33f, 0.1f - (totalBlackeningTime - elapsedTime) * 0.02f), delta);
            getConeLight().setDistance(1f);
            if (elapsedTime >= totalBlackeningTime) {
                setAmbientColor(new Color(1f, 2f, 33f, 0.1f), delta);
                getConeLight().setDistance(1000f);
            }
        }

        if (shouldRunHome) {
            runningHomeElapsedTime += delta;
            setAmbientColor(new Color(1f, 2f, 33f, 0.1f - runningHomeElapsedTime * 0.02f), delta);
            getConeLight().setDistance(1f);
            if (runningHomeElapsedTime >= 5f) {
                shouldRunHome = false;
                MapRenderer.getInstance().changeMap(MapEnum.CHAPTER_1);
                particles.forEach(ParticleEffect::dispose);
                getGameObjects().clear();
                setAmbientColor(new Color(1f, 2f, 33f, 0.1f), delta);
                getConeLight().setDistance(1000f);
                getRayHandler().removeAll();
                PLAYER.setPosition(new Vector2(476f, 3596f));
                addTrigger(new DialogueTrigger(new Vector2(476f, 3596f), 40.0f,
                    new DialogueBuilder("Akhem, ehghhghhhuuuuueeeeeEEEEEE *charczenie corki*",
                        "Nie boj sie coreczko, tatus zalatwil ci lekarstwo! Pij...", "Tu jest lekarstwo! Wszystko bedzie dobrze!") {
                        @Override
                        public DialogueBuilder optionSelected(final int option) {
                            return new DialogueBuilder("O nie... Spoznilem sie...").onSelected(option1 -> {
                                PLAYER.setPosition(new Vector2(2048, 2200));
                                setRayHandlerEnabled(false);
                                startActiveNight();
                                return null;
                            });
                        }
                    }).once());
            }
        }

        if (dead && !messageShown) {
            messageShown = true;
            addTrigger(new DialogueTrigger(new Vector2(PLAYER.getPosition().x, PLAYER.getPosition().y), 40.0f,
                new DialogueBuilder("Twoja corka nie przezyla nocy... To koniec...") {
                    @Override
                    public DialogueBuilder optionSelected(final int option) {
                        DarkTimesApplication.APPLICATION.setScreen(ScreenManagerUtils.getScreen(GameScreenEnum.MENU_SCREEN));
                        return null;
                    }
                }));
        }
    }

    @Override
    protected void renderActiveNight() {
        if (!MIST_SHADER.isActive()) {
            return;
        }

        if (elapsedGhostTimer >= newGhostTimer) {
            elapsedGhostTimer = 0;
            ghostCount++;
        }

        int currentGhosts = 0;
        for (final GameObject go : getGameObjects()) {
            if (go instanceof Ghost) {
                currentGhosts++;
            }
        }

        if (currentGhosts < ghostCount && !MIST_SHADER.isStopping()) {
            final Vector2 ghostPosition = VectorUtils.getRandomVectorInDistance(
                PLAYER.getPosition(), 300.0f);

            final Ghost ghost = new Ghost(new Size(100f, 100f),
                new TextureData("textures/enemy-sheet.png",
                    3, 5, 0.2f),
                ghostPosition, 1f + ((ghostCount - 3) / 10f), null);
            getGameObjects().add(ghost);
        }

        MIST_SHADER.displayMistShader();

        getGameObjects().removeIf(gameObject -> {
            if (gameObject instanceof Ghost) {
                return ((Ghost) gameObject).checkIsGhostDismissed();
            }
            return false;
        });

        if (MIST_SHADER.getMistProgress() < 0.15f && !dead) {
            MIST_SHADER.stopMistShader();
            dead = true;
        }

        if (MIST_SHADER.isStopping()) {
            getGameObjects().removeIf(gameObject -> gameObject instanceof Ghost);
        }
    }

    @Override
    protected void addLightSources(final RayHandler rayHandler) {
        for (final Vector2 bonfirePosition : bonfirePositions) {
            addBonfire(rayHandler, bonfirePosition);
        }
    }

    private void addBonfire(final RayHandler rayHandler, final Vector2 pos) {
        final PointLight p = new PointLight(rayHandler, 10, Color.FIREBRICK, 200.0f, pos.x, pos.y);
        p.setStaticLight(true);
        p.setSoft(true);

        final BonfireFactory bonfireFactory = new BonfireFactory();
        final ParticleEffect particleEffect = bonfireFactory.create(pos);
        particles.add(particleEffect);
    }

    @Override
    public void dispose() {
        super.dispose();
        particles.forEach(ParticleEffect::dispose);
    }

}
