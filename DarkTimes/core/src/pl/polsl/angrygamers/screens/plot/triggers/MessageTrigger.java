package pl.polsl.angrygamers.screens.plot.triggers;

import pl.polsl.angrygamers.utils.dialogues.DialogueManager;

/**
 * The type Message trigger.
 */
public class MessageTrigger extends ScheduledTrigger {

    /**
     * Instantiates a new Message trigger.
     *
     * @param timespan the timespan
     * @param message  the message
     */
    public MessageTrigger(float timespan, final String message) {
        super(timespan, () -> {
            final DialogueManager dialogueManager = DialogueManager.getInstance();
            if (!dialogueManager.isDisplaying()) {
                dialogueManager.displayDialogue(message);
                return true;
            } else {
                return false;
            }
        });
    }
}
