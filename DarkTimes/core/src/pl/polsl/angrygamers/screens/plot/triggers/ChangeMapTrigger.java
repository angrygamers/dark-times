package pl.polsl.angrygamers.screens.plot.triggers;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.DarkTimesApplication;
import pl.polsl.angrygamers.enums.GameScreenEnum;
import pl.polsl.angrygamers.utils.ScreenManagerUtils;

/**
 * The type Change map trigger.
 */
public class ChangeMapTrigger extends GeofenceTrigger {

    private final GameScreenEnum gameScreen;

    /**
     * Instantiates a new Change map trigger.
     *
     * @param position   the position
     * @param gameScreen the game screen
     */
    public ChangeMapTrigger(Vector2 position, GameScreenEnum gameScreen) {
        super(position, 250.0f);
        this.gameScreen = gameScreen;
        this.once();
    }

    @Override
    protected boolean triggerAction() {
        final Screen nextScreen = ScreenManagerUtils.getScreen(gameScreen);
        DarkTimesApplication.APPLICATION.setScreen(nextScreen);
        return true;
    }
}
