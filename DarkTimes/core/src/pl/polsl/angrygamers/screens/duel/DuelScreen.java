package pl.polsl.angrygamers.screens.duel;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import pl.polsl.angrygamers.DarkTimesApplication;
import pl.polsl.angrygamers.enums.GameScreenEnum;
import pl.polsl.angrygamers.objects.duel.Attack;
import pl.polsl.angrygamers.objects.duel.DuelManager;
import pl.polsl.angrygamers.objects.duel.VisualDuellist;
import pl.polsl.angrygamers.screens.OrthogonalScreen;
import pl.polsl.angrygamers.utils.ScreenManagerUtils;

import java.util.List;
import java.util.Random;

/**
 * The type Duel screen.
 */
public class DuelScreen extends OrthogonalScreen {

    private static final Float HUD_CENTER_DISTANCE = 300.0f;
    private static final Float HUD_BOTTOM_DISTANCE = 150.0f;
    private static final Float HUD_SCALE_FACTOR = 2.0f;
    private static final Float HP_LABEL_ANIMATION_DURATION = 2.0f;

    private static final Float PADDING = 15.0f;

    private final DuelManager duelManager;
    private final ParticleEffect hurtParticles;
    private final VisualDuellist leftDuellist;
    private final VisualDuellist rightDuellist;
    private final DuellistInfoBar leftDuellistInfoBar;
    private final DuellistInfoBar rightDuellistInfoBar;
    private final Table attacksTable;
    private final ScrollPane scrollPane;
    private GameScreenEnum lastScreen;

    private DuelScreen(final VisualDuellist player1, final VisualDuellist player2) {
        super();
        this.duelManager = new DuelManager(this, player1, player2);
        this.leftDuellist = player1;
        this.rightDuellist = player2;

        final Texture backgroundTexture = new Texture(Gdx.files.internal("background/finalNight.png"));
        setBackground(backgroundTexture);

        final Skin skin = DarkTimesApplication.APPLICATION.getSkin();
        leftDuellistInfoBar = new DuellistInfoBar(skin, leftDuellist);
        rightDuellistInfoBar = new DuellistInfoBar(skin, rightDuellist, true);

        attacksTable = new Table(skin);
        attacksTable.pad(PADDING);
        attacksTable.padRight(PADDING * 2);
        attacksTable.top();
        scrollPane = new ScrollPane(attacksTable, skin);
        scrollPane.setVisible(false);

        stage.addActor(leftDuellistInfoBar);
        stage.addActor(rightDuellistInfoBar);
        stage.addActor(scrollPane);
        stage.addActor(duelManager);
        hurtParticles = skin.get("hurt_particles", ParticleEffect.class);
    }

    /**
     * Instantiates a new Duel screen.
     *
     * @param leftDuellist  the left duellist
     * @param rightDuellist the right duellist
     * @param lastScreen    the last screen
     */
    public DuelScreen(final VisualDuellist leftDuellist, final VisualDuellist rightDuellist, final GameScreenEnum lastScreen) {
        this(leftDuellist, rightDuellist);
        this.lastScreen = lastScreen;
    }

    @Override
    public void resize(final int width, final int height) {
        super.resize(width, height);
        updatePositions(width, height);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        updatePositions((int) stage.getWidth(), (int) stage.getHeight());
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                duelManager.startDuel();
            }
        }, 1.0f);
    }

    @Override
    protected void onBackgroundDrawn() {
        renderDuellists();
    }

    private void renderDuellists() {
        final float deltaTime = Gdx.graphics.getDeltaTime();
        final Sprite leftDuellistHudSprite = leftDuellist.getHudSprite();
        final Sprite rightDuellistHudSprite = rightDuellist.getHudSprite();

        final Batch batch = stage.getBatch();
        batch.begin();

        leftDuellistHudSprite.draw(batch);
        rightDuellistHudSprite.draw(batch);
        hurtParticles.draw(batch, deltaTime);

        batch.end();
    }

    private void updatePositions(final int width, final int height) {
        leftDuellistInfoBar.setPosition(0 + PADDING, height - leftDuellistInfoBar.getHeight() - PADDING);
        rightDuellistInfoBar.setPosition(width - rightDuellistInfoBar.getWidth() - PADDING, height - rightDuellistInfoBar.getHeight() - PADDING);

        scrollPane.setPosition(PADDING, PADDING);
        scrollPane.setHeight(height / 3.0f);

        updateHuds();
    }

    private void updateHuds() {
        final Sprite leftDuellistHudSprite = leftDuellist.getHudSprite();
        final Sprite rightDuellistHudSprite = rightDuellist.getHudSprite();

        final int widthCenter = Gdx.graphics.getWidth() / 2;

        leftDuellistHudSprite.setScale(HUD_SCALE_FACTOR);
        rightDuellistHudSprite.setScale(-HUD_SCALE_FACTOR, HUD_SCALE_FACTOR);
        leftDuellistHudSprite.setPosition(
                widthCenter - HUD_CENTER_DISTANCE,
                HUD_BOTTOM_DISTANCE);
        rightDuellistHudSprite.setPosition(
                widthCenter + HUD_CENTER_DISTANCE,
                HUD_BOTTOM_DISTANCE);
    }

    /**
     * Show attacks pane.
     *
     * @param duellist the duellist
     */
    public void showAttacksPane(final VisualDuellist duellist) {

        // Enemy behaviour;
        if (duellist == rightDuellist) {
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    final Random random = new Random();
                    final int attackIndex = random.nextInt(duellist.getAvailableAttacks().size());
                    duelManager.onAttackChosen(duellist.getAvailableAttacks().get(attackIndex));
                }
            }, 0.5f);
            return;
        }

        final Skin skin = DarkTimesApplication.APPLICATION.getSkin();

        final List<Attack> availableAttacks = duellist.getAvailableAttacks();
        for (final Attack attack : availableAttacks) {
            final AttackInfoWidget attackInfoWidget = new AttackInfoWidget(skin, attack);
            attacksTable.add(attackInfoWidget).top().padBottom(20.0f).fillX();

            attackInfoWidget.addListener(new ClickListener() {
                @Override
                public void clicked(final InputEvent event, final float x, final float y) {
                    super.clicked(event, x, y);
                    duelManager.onAttackChosen(attack);
                    hideAttacksPane();
                }
            });

            attacksTable.row();
        }
        scrollPane.pack();
        scrollPane.setVisible(true);
    }

    private void hideAttacksPane() {
        attacksTable.clearChildren();
        scrollPane.setVisible(false);
    }

    /**
     * On attack fail.
     *
     * @param duellist the duellist
     * @param attack   the attack
     */
    public void onAttackFail(final VisualDuellist duellist, final Attack attack) {
        if (duellist == leftDuellist) {
            showDisappearingDialog("Nieudany atak", "Atak '" + attack.getName() + "' sie nie powiodl.");
        }
    }

    private void showDisappearingDialog(final String title, final String message) {
        final Dialog dialog = new Dialog(title, DarkTimesApplication.APPLICATION.getSkin());
        dialog.text(message);
        dialog.button("OK");
        dialog.getContentTable().pad(20.0f);
        dialog.pack();
        dialog.show(stage);

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                dialog.hide();
            }
        }, 2L);
    }

    /**
     * On attack success.
     *
     * @param attackedDuellist the attacked duellist
     * @param damage           the damage
     */
    public void onAttackSuccess(final VisualDuellist attackedDuellist, final float damage) {
        final Sprite attackedHud = attackedDuellist == leftDuellist ?
                leftDuellist.getHudSprite() : rightDuellist.getHudSprite();

        final float x = attackedHud.getX() + attackedHud.getWidth() * attackedHud.getScaleX() / 3.0f;
        final float y = attackedHud.getY() + attackedHud.getHeight() * attackedHud.getScaleY() / 2.0f;
        hurtParticles.setPosition(x, y);
        hurtParticles.reset();

        showDamagedHpLabel(damage, attackedHud, x, y);
    }

    private void showDamagedHpLabel(final float damage, final Sprite attackedHud, final float x, final float y) {
        final Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = DarkTimesApplication.APPLICATION.getSkin().getFont("small-font");
        final Label hpLabel = new Label(String.format("-%.2f hp", damage), labelStyle);
        hpLabel.setColor(Color.valueOf("8a0303"));
        hpLabel.setPosition(x, y + attackedHud.getHeight());

        final AlphaAction fadeAction = new AlphaAction();
        fadeAction.setAlpha(0.2f);
        fadeAction.setDuration(HP_LABEL_ANIMATION_DURATION);
        hpLabel.addAction(fadeAction);

        final MoveByAction goDownAction = new MoveByAction();
        goDownAction.setAmountY(-40.0f);
        goDownAction.setDuration(2.0f);
        hpLabel.addAction(goDownAction);

        stage.addActor(hpLabel);

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                hpLabel.remove();
            }
        }, 2.0f);
    }

    /**
     * On duelist defeated.
     *
     * @param defeatedDuellist the defeated duelist
     */
    public void onDuellistDefeated(final VisualDuellist defeatedDuellist) {
        final String title = defeatedDuellist == leftDuellist ? "Zostales pokonany..."
                : "Pokonales swojego groznego przeciwnika - " + rightDuellist.getName();

        final Dialog dialog = new Dialog(title, DarkTimesApplication.APPLICATION.getSkin());
        final TextButton b = new TextButton("OK", DarkTimesApplication.APPLICATION.getSkin());
        b.addListener(event -> {
            if (defeatedDuellist == leftDuellist) {
                DarkTimesApplication.APPLICATION.setScreen(ScreenManagerUtils.getScreen(GameScreenEnum.MENU_SCREEN));
                this.dispose();
            } else {
                DarkTimesApplication.APPLICATION.setScreen(ScreenManagerUtils.getScreen(lastScreen));
                this.dispose();
            }
            return false;
        });
        dialog.button(b);
        dialog.getContentTable().pad(20.0f);
        dialog.pack();
        dialog.show(stage);
    }
}
