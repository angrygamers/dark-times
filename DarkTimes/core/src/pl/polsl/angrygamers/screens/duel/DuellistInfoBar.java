package pl.polsl.angrygamers.screens.duel;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import pl.polsl.angrygamers.DarkTimesApplication;
import pl.polsl.angrygamers.objects.duel.VisualDuellist;

/**
 * The type Duelist info bar.
 */
public class DuellistInfoBar extends Table {

    private static final float INTERNAL_TABLE_LEFT_PADDING = 5.0f;
    private static final float INTERNAL_TABLE_RIGHT_PADDING = 5.0f;
    private static final float INTERNAL_TABLE_IMAGE_SPACING = 10.f;
    private static final float IMAGE_TOP_PADDING = 5.0f;
    private static final float IMAGE_BOTTOM_PADDING = 5.0f;

    private final Float IMAGE_SIZE = 64.0f;

    private final ShapeRenderer shapeRenderer;
    private final VisualDuellist duellist;
    private Label label;
    private HealthBar healthBar;
    private final Skin skin;

    /**
     * Instantiates a new Duelist info bar.
     *
     * @param skin     the skin
     * @param duellist the duellist
     */
    public DuellistInfoBar(Skin skin, VisualDuellist duellist) {
        this(skin, duellist, false);
    }

    /**
     * Instantiates a new Duelist info bar.
     *
     * @param skin     the skin
     * @param duellist the duellist
     * @param reversed the reversed
     */
    public DuellistInfoBar(Skin skin, VisualDuellist duellist, boolean reversed) {
        super(DarkTimesApplication.APPLICATION.getSkin());
        this.skin = skin;
        this.shapeRenderer = new ShapeRenderer(10);
        this.duellist = duellist;
        this.setBackground("default-round");

        setUp(reversed);
        this.pack();
    }

    private void setUp(boolean reversed) {
        this.top();
        Table table = createInternalTable();
        Image image = new Image(duellist.getIconTexture());
        image.pack();

        if (reversed) {
            this.add(table)
                    .padLeft(INTERNAL_TABLE_LEFT_PADDING)
                    .padRight(INTERNAL_TABLE_IMAGE_SPACING);
            this.add(image)
                    .size(IMAGE_SIZE)
                    .padTop(IMAGE_TOP_PADDING)
                    .padBottom(IMAGE_BOTTOM_PADDING)
                    .padRight(INTERNAL_TABLE_RIGHT_PADDING);
        } else {
            this.add(image)
                    .size(IMAGE_SIZE)
                    .padTop(IMAGE_TOP_PADDING)
                    .padBottom(IMAGE_BOTTOM_PADDING)
                    .padLeft(INTERNAL_TABLE_LEFT_PADDING);
            this.add(table)
                    .padLeft(INTERNAL_TABLE_IMAGE_SPACING)
                    .padRight(INTERNAL_TABLE_RIGHT_PADDING);
        }
    }

    private Table createInternalTable() {
        Table table = new Table();
        this.label = new Label(duellist.getName(), skin);
        this.label.setAlignment(Align.left);
        this.healthBar = new HealthBar(shapeRenderer, duellist);
        table.add(label).fillX().padBottom(10.0f);
        table.row();
        table.add(healthBar);
        return table;
    }
}
