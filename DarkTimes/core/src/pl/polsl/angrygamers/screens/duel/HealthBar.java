package pl.polsl.angrygamers.screens.duel;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import pl.polsl.angrygamers.objects.duel.VisualDuellist;

/**
 * The type Health bar.
 */
public class HealthBar extends Widget {

    private final ShapeRenderer shapeRenderer;
    private final VisualDuellist duellist;
    private float targetHealth = 1.0f;
    private float displayedHealth = targetHealth;

    /**
     * Instantiates a new Health bar.
     *
     * @param shapeRenderer the shape renderer
     * @param duellist      the duellist
     */
    public HealthBar(ShapeRenderer shapeRenderer, VisualDuellist duellist) {
        this.shapeRenderer = shapeRenderer;
        this.duellist = duellist;
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
    }

    @Override
    public float getPrefWidth() {
        return 200;
    }

    @Override
    public float getPrefHeight() {
        return 20;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        checkForHealthUpdate();
        drawCurrentState(batch);
    }

    private void checkForHealthUpdate() {
        float hp = duellist.getHp();
        if(hp != targetHealth) {
            this.clearActions();
            HealthAnimationAction healthAnimationAction = new HealthAnimationAction(targetHealth, hp);
            this.addAction(healthAnimationAction);
            targetHealth = hp;
        } else {
            this.displayedHealth = hp;
        }
    }

    private void drawCurrentState(Batch batch) {
        float x = this.getX();
        float y = this.getY();
        float width = this.getWidth();
        float height = this.getHeight();

        float displayedHealth = this.displayedHealth;
        float progressWidth = displayedHealth * width;
        Color color = getColor(displayedHealth);
        Color complimentaryColor = getComplimentaryColor(color);

        batch.end();
        shapeRenderer.setProjectionMatrix(this.getStage().getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(x, y, width, height);
        shapeRenderer.setColor(color);
        shapeRenderer.rect(x, y + (height / 2), progressWidth, height / 2);
        shapeRenderer.setColor(complimentaryColor);
        shapeRenderer.rect(x, y, progressWidth, height / 2);

        shapeRenderer.end();
        batch.begin();
    }

    private Color getColor(float health) {
        if(health > 0.8f) {
            return Color.GREEN;
        } else if(health > 0.6f) {
            return Color.YELLOW;
        } else if(health > 0.35f) {
            return Color.ORANGE;
        } else {
            return Color.RED;
        }
    }

    private Color getComplimentaryColor(Color color) {
        return new Color(
                color.r > 0 ? 175 / 255.f : 0,
                color.g > 0 ? 175 / 255.f : 0,
                color.b > 0 ? 175 / 255.f : 0,
                1
        );
    }

    private class HealthAnimationAction extends TemporalAction {

        private final float from;
        private final float to;

        /**
         * Instantiates a new Health animation action.
         *
         * @param from the from
         * @param to   the to
         */
        public HealthAnimationAction(float from, float to) {
            this.from = from;
            this.to = to;
        }

        @Override
        protected void update(float percent) {
            float update = (to - from) * percent;
            displayedHealth = from + update;
        }
    }
}