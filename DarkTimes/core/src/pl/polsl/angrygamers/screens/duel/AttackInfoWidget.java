package pl.polsl.angrygamers.screens.duel;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import pl.polsl.angrygamers.objects.animations.AnimationsState;
import pl.polsl.angrygamers.objects.duel.Attack;

import java.util.Random;

/**
 * The type Attack info widget.
 */
public class AttackInfoWidget extends Button {

    private static final Float INTERNAL_PADDING_VERTICAL = 15.0f;
    private static final Float INTERNAL_PADDING_HORIZONTAL = 15.0f;
    private static final Float WIDTH = 325.0f;
    private static final Float IMAGE_SIZE = 36.0f;

    private Random random;
    private Skin skin;
    private Sprite diceSprite;
    private float delta;
    private float animationDelta;

    /**
     * Instantiates a new Attack info widget.
     *
     * @param skin   the skin
     * @param attack the attack
     */
    public AttackInfoWidget(Skin skin, Attack attack) {
        super(skin);
        this.setBackground("default-round");
        this.skin = skin;
        this.random = new Random();
        this.delta = random.nextFloat();

        Label nameLabel = new Label(attack.getName(), skin);
        nameLabel.setColor(Color.GOLD);
        nameLabel.setAlignment(Align.left);

        Label.LabelStyle smallFontLabelStyle = new Label.LabelStyle();
        smallFontLabelStyle.font = skin.getFont("small-font");
        Label descriptionLabel = new Label(attack.getDescription(), smallFontLabelStyle);
        descriptionLabel.setAlignment(Align.left);
        descriptionLabel.setWrap(true);

        pad(INTERNAL_PADDING_VERTICAL, INTERNAL_PADDING_HORIZONTAL,
                INTERNAL_PADDING_VERTICAL, INTERNAL_PADDING_HORIZONTAL);
        add(nameLabel).top().fillX().padBottom(10.0f).width(WIDTH).maxWidth(WIDTH);
        row();
        add(descriptionLabel).left().fillX().width(WIDTH).maxWidth(WIDTH);
        row();

        addAttackRangeRow(skin, attack, smallFontLabelStyle);
        addAttackChancesRow(skin, attack, smallFontLabelStyle);

        pack();
    }

    private void addAttackRangeRow(Skin skin, Attack attack, Label.LabelStyle smallFontLabelStyle) {
        TextureRegion heartTexture = skin.getRegion("heart");
        Image heartImage = new Image(heartTexture);

        Label attackRangeLabel = new Label(String.format("%.2f - %.2f",
                attack.getRange().getFrom(), attack.getRange().getUntil()), smallFontLabelStyle);
        attackRangeLabel.setColor(Color.RED);
        attackRangeLabel.setAlignment(Align.left);

        Table attackRangeTable = new Table();
        attackRangeTable.add(heartImage).size(IMAGE_SIZE).maxWidth(IMAGE_SIZE);
        attackRangeTable.add(attackRangeLabel).padLeft(5.0f).left().expandX();
        add(attackRangeTable).padTop(10.0f).fillX().width(WIDTH);
        attackRangeTable.pack();
        row();
    }

    private void addAttackChancesRow(Skin skin, Attack attack, Label.LabelStyle smallFontLabelStyle) {
        AnimationsState diceAnimation = skin.get("dice_animation", AnimationsState.class);
        TextureRegion textureRegion = diceAnimation.getKeyFrame(animationDelta);
        diceSprite = new Sprite(textureRegion);
        SpriteDrawable spriteDrawable = new SpriteDrawable(diceSprite);
        Image diceImage = new Image(spriteDrawable);

        Label attackChanceLabel = new Label(String.format("%.2f %%", attack.getChance()), smallFontLabelStyle);
        attackChanceLabel.setColor(Color.valueOf("b794f6"));
        attackChanceLabel.setAlignment(Align.left);

        Table attackChanceTable = new Table();
        attackChanceTable.add(diceImage).size(IMAGE_SIZE).maxWidth(IMAGE_SIZE);
        attackChanceTable.add(attackChanceLabel).padLeft(5.0f).left().expandX();
        add(attackChanceTable).padTop(10.0f).fillX().width(WIDTH);
        attackChanceTable.pack();
        row();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        AnimationsState diceAnimation = skin.get("dice_animation", AnimationsState.class);
        TextureRegion textureRegion = diceAnimation.getKeyFrame(this.animationDelta);
        diceSprite.setRegion(textureRegion);
        super.draw(batch, parentAlpha);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        this.delta += delta;
        if(this.delta >= 1.0f) {
            this.delta = 0.0f;
            this.animationDelta = random.nextFloat() * 2.5f;
        }
    }
}
