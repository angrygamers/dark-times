package pl.polsl.angrygamers.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import pl.polsl.angrygamers.DarkTimesApplication;
import pl.polsl.angrygamers.enums.GameScreenEnum;
import pl.polsl.angrygamers.objects.CursorCircleMistShader;
import pl.polsl.angrygamers.objects.Menu;
import pl.polsl.angrygamers.objects.MenuItem;
import pl.polsl.angrygamers.utils.ScreenManagerUtils;

import java.util.ArrayList;
import java.util.List;

import static pl.polsl.angrygamers.PlayerInput.PLAYER_INPUT_SESSION;


/**
 * The type Menu screen.
 */
public class MenuScreen extends ScreenAdapter implements Menu.MenuListener {

    private static final String MENU_BACKGROUND_PATH = "brick-moss-subsea.jpg";

    private static final int PLAY_BUTTON_ID = 0;
    private static final int SETTINGS_BUTTON_ID = 1;
    private static final int EXIT_BUTTON_ID = 2;

    private final Stage stage;
    private Image backgroundImage;
    private final OrthographicCamera camera;
    private Menu menu;
    private final DarkTimesApplication game;

    /**
     * Instantiates a new Menu screen.
     *
     * @param game the game
     */
    public MenuScreen(final DarkTimesApplication game) {
        super();
        this.game = game;
        camera =  new OrthographicCamera();
        camera.zoom = 0.5f;
        final Viewport viewport = new ScalingViewport(Scaling.stretch, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), camera);
        final SpriteBatch batch = new SpriteBatch(3, new CursorCircleMistShader());
        stage = new Stage(viewport, batch);
    }

    @Override
    public void show() {
        System.out.println("Shown menuScreen");
        PLAYER_INPUT_SESSION.setGame(game);
        final InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(PLAYER_INPUT_SESSION);
        multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer);

        backgroundImage = createBackgroundImage();
        stage.addActor(backgroundImage);

        menu = createMenu();
        menu.addMenuListener(this);
        stage.addActor(menu);
    }

    private Image createBackgroundImage() {
        final Texture texture = new Texture(Gdx.files.internal(MENU_BACKGROUND_PATH));
        texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);

        final int width = stage.getViewport().getScreenWidth();
        final int height = stage.getViewport().getScreenHeight();
        final TextureRegion textureRegion = new TextureRegion(texture);
        textureRegion.setRegion(0,0, width, height);

        final TextureRegionDrawable textureRegionDrawable = new TextureRegionDrawable(textureRegion);
        final Image backgroundImage = new Image();
        backgroundImage.setDrawable(textureRegionDrawable);
        backgroundImage.setSize(width, height);
        return backgroundImage;
    }

    private Menu createMenu() {
        final List<MenuItem> menuItems = new ArrayList<>();
        menuItems.add(new MenuItem(PLAY_BUTTON_ID, "PLAY"));
//        menuItems.add(new MenuItem(SETTINGS_BUTTON_ID, "SETTINGS"));
        menuItems.add(new MenuItem(EXIT_BUTTON_ID, "EXIT"));
        return new Menu(menuItems);
    }

    @Override
    public void render(final float delta) {
        super.render(delta);

        Gdx.gl.glClearColor(0,0,0,0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(final int width, final int height) {
        stage.getViewport().update(width, height);
        stage.getCamera().viewportWidth = width;
        stage.getCamera().viewportHeight = height;

        menu.updateButtonSizes();

        camera.update();
    }

    @Override
    public void dispose() {
        menu.dispose();
        stage.dispose();
    }

    @Override
    public void onMenuItemClicked(final MenuItem menuItem) {
        final int id = menuItem.getId();
        switch (id) {
            case PLAY_BUTTON_ID:
                game.setScreen(ScreenManagerUtils.getScreen(GameScreenEnum.CHAPTER_ONE));
                this.dispose();
                break;
            case EXIT_BUTTON_ID:
                Gdx.app.exit();
                break;
            default:
                System.out.println("Clicked: " + menuItem.getText());
                break;
        }
    }
}

