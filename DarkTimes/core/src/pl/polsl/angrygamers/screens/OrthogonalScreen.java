package pl.polsl.angrygamers.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * The type Orthogonal screen.
 */
public class OrthogonalScreen extends ScreenAdapter {

    /**
     * The Stage.
     */
    protected Stage stage;
    private Camera camera;
    private Texture backgroundTexture;

    /**
     * Instantiates a new Orthogonal screen.
     */
    public OrthogonalScreen() {
        camera =  new OrthographicCamera();
        Viewport viewport = new ScalingViewport(Scaling.stretch, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), camera);
        Batch batch = createBatch();
        stage = new Stage(viewport, batch);
    }

    /**
     * Sets background.
     *
     * @param texture the texture
     */
    protected void setBackground(Texture texture) {
        backgroundTexture = texture;
    }

    /**
     * Create batch batch.
     *
     * @return the batch
     */
    protected Batch createBatch() {
        return new SpriteBatch(10);
    }

    @Override
    public void resize(final int width, final int height) {
        stage.getViewport().update(width, height);
        stage.getCamera().viewportWidth = width;
        stage.getCamera().viewportHeight = height;
        camera.update();
    }

    @Override
    public void render(float delta) {
        drawBackground();
        onBackgroundDrawn();
        stage.act(delta);
        stage.draw();
    }

    private void drawBackground() {
        Batch batch = stage.getBatch();
        batch.begin();
        batch.draw(backgroundTexture, 0.0f, 0.0f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.end();
    }

    /**
     * On background drawn.
     */
    protected void onBackgroundDrawn() {
        // hook
    }
}
