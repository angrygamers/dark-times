package pl.polsl.angrygamers.enums;

/**
 * The enum Item use.
 */
public enum ItemUse {
    /**
     * Wearable no effect item use.
     */
    WEARABLE_NO_EFFECT,
    /**
     * Wearable speed plus 10 item use.
     */
    WEARABLE_SPEED_PLUS10,
    /**
     * Wearable plus 25 item use.
     */
    WEARABLE_PLUS25,
    /**
     * Wearable plus 50 item use.
     */
    WEARABLE_PLUS50,
    /**
     * Restore full health item use.
     */
    RESTORE_FULL_HEALTH,
    /**
     * Restore health 25 item use.
     */
    RESTORE_HEALTH25,
    /**
     * Restore health 50 item use.
     */
    RESTORE_HEALTH50,
    /**
     * Mission item item use.
     */
    MISSION_ITEM,
    /**
     * Potion item item use.
     */
    POTION_ITEM
}
