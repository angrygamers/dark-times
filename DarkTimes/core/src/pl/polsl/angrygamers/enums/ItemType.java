package pl.polsl.angrygamers.enums;

/**
 * Enum, that determines types of object Player can collect.
 * CONSUMABLE - Object that Player can use
 * WEARABLE - Object that Player can put on
 * MISSION_ITEM - Object that is necessary to complete mission - cannot be used in other way
 */
public enum  ItemType {
    /**
     * Consumable item type.
     */
    CONSUMABLE,
    /**
     * Wearable item type.
     */
    WEARABLE,
    /**
     * Mission item item type.
     */
    MISSION_ITEM
}
