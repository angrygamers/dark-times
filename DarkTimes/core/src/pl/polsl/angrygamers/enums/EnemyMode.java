package pl.polsl.angrygamers.enums;

/**
 * Enum, that determines, if enemy see player.
 * NORMAL - Enemy continue movement path
 * TERITORIAL_ANGRY - Enemy attacks player if he's in close distance
 * ANGRY - Enemy is chasing the player
 */
public enum EnemyMode {
    /**
     * Normal enemy mode.
     */
    NORMAL,
    /**
     * Teritorial angry enemy mode.
     */
    TERITORIAL_ANGRY,
    /**
     * Angry enemy mode.
     */
    ANGRY,
}
