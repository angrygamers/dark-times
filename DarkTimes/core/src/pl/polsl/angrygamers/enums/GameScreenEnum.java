package pl.polsl.angrygamers.enums;

/**
 * The enum Game screen enum.
 */
public enum GameScreenEnum {
    /**
     * Menu screen game screen enum.
     */
    MENU_SCREEN,
    /**
     * Game screen game screen enum.
     */
    GAME_SCREEN,
    /**
     * Chapter one game screen enum.
     */
    CHAPTER_ONE,
    /**
     * Chapter one duel game screen enum.
     */
    CHAPTER_ONE_DUEL,
    /**
     * Chapter two game screen enum.
     */
    CHAPTER_TWO,
    /**
     * Chapter three game screen enum.
     */
    CHAPTER_THREE,
    /**
     * Chapter four game screen enum.
     */
    CHAPTER_FOUR,
    /**
     * Chapter five and six game screen enum.
     */
    CHAPTER_FIVE_AND_SIX,
    /**
     * Chapter seven game screen enum.
     */
    CHAPTER_SEVEN,
    /**
     * Active night screen game screen enum.
     */
    ACTIVE_NIGHT_SCREEN,
}
