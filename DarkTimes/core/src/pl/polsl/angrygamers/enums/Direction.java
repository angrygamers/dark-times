package pl.polsl.angrygamers.enums;

/**
 * Enum, that determine movement direction
 */
public enum Direction {
    /**
     * Up direction.
     */
    UP,
    /**
     * Up right direction.
     */
    UP_RIGHT,
    /**
     * Right direction.
     */
    RIGHT,
    /**
     * Down right direction.
     */
    DOWN_RIGHT,
    /**
     * Down direction.
     */
    DOWN,
    /**
     * Down left direction.
     */
    DOWN_LEFT,
    /**
     * Left direction.
     */
    LEFT,
    /**
     * Up left direction.
     */
    UP_LEFT
}
