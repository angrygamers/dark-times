package pl.polsl.angrygamers.shaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

import static pl.polsl.angrygamers.utils.GlobalVariables.SPRITE_BATCH;

/**
 * The type Mist shader.
 */
public class MistShaderImpl implements MistShader {

    private boolean active;
    private float mistProgress;
    private final ShaderProgram shader;

    private float step;
    private float progressLeft;
    private float hit;
    private boolean stopping;

    /**
     * Instantiates a new Mist shader.
     */
    public MistShaderImpl() {
        this.active = false;

        shader = new ShaderProgram(
            Gdx.files.internal("shaders/mist.vert").readString(),
            Gdx.files.internal("shaders/mist.frag").readString());
        System.out.println(shader.getLog());

        shader.begin();
        shader.setUniformf("u_hit", 0.0f);
        shader.end();
    }

    @Override
    public void startMistShader() {
        step = 0;
        progressLeft = 0;
        hit = 0;
        SPRITE_BATCH.setShader(shader);
        active = true;
        mistProgress = 1.2f;
        // smooth intro effect
        new Thread(() -> {
            while (mistProgress > 0.92f) {
                mistProgress -= (mistProgress - 0.9f) * 0.02f;
                try {
                    Thread.sleep((long)(Gdx.graphics.getDeltaTime() * 1000));
                } catch (final InterruptedException ignored) {}
            }
            mistProgress = 0.9f;
        }).start();
    }

    @Override
    public void stopMistShader() {
        stopping = true;
        // smooth outro effect
        new Thread(() -> {
            while (mistProgress < 1.18f) {
                mistProgress += (1.2f - mistProgress) * 0.02f;
                try {
                    Thread.sleep((long)(Gdx.graphics.getDeltaTime() * 1000));
                } catch (final InterruptedException ignored) {}
            }
            active = false;
            stopping = false;
            SPRITE_BATCH.setShader(null);
        }).start();
    }

    @Override
    public float getMistProgress() {
        return mistProgress;
    }

    @Override
    public void progressMist(final float progressPercentage, final int millis) {
        this.hit = 1.0f;
        // calculate number of frames that progress is to be divided into based on amount of milliseconds
        this.step = progressPercentage / (millis / (Gdx.graphics.getDeltaTime() * 1000));
        this.progressLeft = progressPercentage;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void displayMistShader() {
        if (progressLeft > step) {
            progressLeft -= step;
            mistProgress -= step;
        }

        if (mistProgress < 0.1f) {
            mistProgress = 0.1f;
        }
        gotHitEffect();

        shader.begin();
        shader.setUniformf("u_hit", hit);
        shader.setUniformf("u_outerRadius", mistProgress);
        shader.end();
    }

    @Override
    public void resize(final int width, final int height) {
        shader.begin();
        shader.setUniformf("u_resolution", width, height);
        shader.end();
    }

    @Override
    public void dispose() {
        shader.dispose();
    }

    @Override
    public boolean isStopping() {
        return stopping;
    }

    private void gotHitEffect() {
        if (hit > 0.01f) {
            hit -= 0.009f;
            if (hit <= 0.0f) {
                hit = 0.001f;
            }
        }
    }
}
