package pl.polsl.angrygamers.shaders;

/**
 * The interface Mist shader.
 */
public interface MistShader {

    /**
     * Start mist shader.
     */
    void startMistShader();

    /**
     * Stop mist shader.
     */
    void stopMistShader();

    /**
     * Gets mist progress.
     *
     * @return the mist progress
     */
    float getMistProgress();

    /**
     * Progress mist.
     *
     * @param progressPercentage the progress percentage
     * @param millis             the millis
     */
    void progressMist(float progressPercentage, int millis);

    /**
     * Is active boolean.
     *
     * @return the boolean
     */
    boolean isActive();

    /**
     * Display mist shader.
     */
    void displayMistShader();

    /**
     * Resize.
     *
     * @param width  the width
     * @param height the height
     */
    void resize(int width, int height);

    /**
     * Dispose.
     */
    void dispose();

    /**
     * Is stopping boolean.
     *
     * @return the boolean
     */
    boolean isStopping();

}
