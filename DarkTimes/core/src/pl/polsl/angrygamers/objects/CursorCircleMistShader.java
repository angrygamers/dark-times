package pl.polsl.angrygamers.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import pl.polsl.angrygamers.utils.Coordinate;
import pl.polsl.angrygamers.utils.CoordinateSystemUtil;

/**
 * The type circle Cursor mist shader.
 */
public class CursorCircleMistShader extends ShaderProgram {

    private static final String SHADER_MIST_VERT_PATH = "shaders/point_torch.vert";
    private static final String SHADER_MIST_FRAG_PATH = "shaders/point_torch.frag";
    private static final String OUTER_RADIUS = "u_outerRadius";
    private static final String RESOLUTION = "u_resolution";
    private static final String POSITION = "u_position";

    private static final Float OUTER_RADIUS_NORMALIZED = 0.25f;

    /**
     * Instantiates a new Cursor circle mist shader.
     */
    public CursorCircleMistShader() {
        super(Gdx.files.internal(SHADER_MIST_VERT_PATH), Gdx.files.internal(SHADER_MIST_FRAG_PATH));
    }

    @Override
    public void begin() {
        super.begin();
        float width = Gdx.graphics.getWidth();
        float height = Gdx.graphics.getHeight();
        float x = (float)Gdx.input.getX();
        float y = (float)Gdx.input.getY();
        Coordinate normalizedCoordinate = CoordinateSystemUtil.fromTouchToNormalizedOrtho(x, y);

        setUniformf(OUTER_RADIUS, OUTER_RADIUS_NORMALIZED);
        setUniformf(RESOLUTION, width, height);
        setUniformf(POSITION, normalizedCoordinate.getX(), normalizedCoordinate.getY());
    }
}
