package pl.polsl.angrygamers.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import pl.polsl.angrygamers.enums.ItemType;
import pl.polsl.angrygamers.enums.ItemUse;
import pl.polsl.angrygamers.utils.Size;


/**
 * The type inventory Item.
 */
public class Item {

    private ItemType type;
    private ItemUse usage;
    private String name;
    private String description;
    private int cost;
    private boolean equipped;
    private boolean inInventory;
    private Vector2 position;
    private Sprite sprite;
    private Image image;
    private Size size;
    private Rectangle spriteRectangle;

    /**
     * Instantiates a new Item.
     *
     * @param position    the position
     * @param size        the size
     * @param texture     the texture
     * @param type        the type
     * @param usage       the usage
     * @param name        the name
     * @param description the description
     */
    public Item(final Vector2 position, final Size size, final Texture texture, ItemType type, ItemUse usage, String name, String description) {
        this.position = position;
        this.type = type;
        this.usage = usage;
        this.name = name;
        this.description = description;
        sprite = new Sprite(texture);
        sprite.setSize(size.getWidth(), size.getHeight());
        sprite.setCenter(size.getWidth() / 2, size.getHeight() / 2);
        sprite.setOriginCenter();
        sprite.setOriginBasedPosition(position.x + sprite.getOriginX(), position.y + sprite.getOriginY());
        position.x = sprite.getX();
        position.y = sprite.getY();
        spriteRectangle = new Rectangle(position.x, position.y, sprite.getWidth(), sprite.getHeight());
        this.image = new Image(sprite);
    }


    /**
     * Take item.
     */
    void takeItem() {
        this.setInInventory(true);
        this.spriteRectangle.set(0, 0, 0, 0);
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public ItemType getType() {
        return type;
    }

    /**
     * Sets type of Item.
     *
     * @param type the type
     */
    public void setType(ItemType type) {
        this.type = type;
    }

    /**
     * Gets usage of Item.
     *
     * @return the usage
     */
    public ItemUse getUsage() {
        return usage;
    }

    /**
     * Sets usage of Item.
     *
     * @param usage the usage
     */
    public void setUsage(ItemUse usage) {
        this.usage = usage;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets cost.
     *
     * @return the cost
     */
    public int getCost() {
        return cost;
    }

    /**
     * Sets cost.
     *
     * @param cost the cost
     */
    public void setCost(int cost) {
        this.cost = cost;
    }

    /**
     * Is equipped boolean.
     *
     * @return the boolean
     */
    public boolean isEquipped() {
        return equipped;
    }

    /**
     * Sets equipped.
     *
     * @param equipped the equipped
     */
    public void setEquipped(boolean equipped) {
        this.equipped = equipped;
    }

    /**
     * Gets position.
     *
     * @return the position
     */
    public Vector2 getPosition() {
        return position;
    }

    /**
     * Sets position.
     *
     * @param position the position
     */
    public void setPosition(Vector2 position) {
        this.position = position;
    }

    /**
     * Gets item sprite.
     *
     * @return the sprite
     */
    public Sprite getSprite() {
        return sprite;
    }

    /**
     * Sets item sprite.
     *
     * @param sprite the sprite
     */
    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    /**
     * Check if item Is in inventory.
     *
     * @return the boolean
     */
    public boolean isInInventory() {
        return inInventory;
    }

    /**
     * Sets item is in inventory.
     *
     * @param inInventory the in inventory
     */
    public void setInInventory(boolean inInventory) {
        this.inInventory = inInventory;
    }

    /**
     * Gets size.
     *
     * @return the size
     */
    public Size getSize() {
        return size;
    }

    /**
     * Sets size.
     *
     * @param size the size
     */
    public void setSize(Size size) {
        this.size = size;
    }

    /**
     * Gets item rectangle sprite.
     *
     * @return the sprite rectangle
     */
    public Rectangle getSpriteRectangle() {
        return spriteRectangle;
    }

    /**
     * Sets item rectangle sprite.
     *
     * @param spriteRectangle the sprite rectangle
     */
    public void setSpriteRectangle(Rectangle spriteRectangle) {
        this.spriteRectangle = spriteRectangle;
    }

    /**
     * Gets image.
     *
     * @return the image
     */
    public Image getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image the image
     */
    public void setImage(Image image) {
        this.image = image;
    }
}
