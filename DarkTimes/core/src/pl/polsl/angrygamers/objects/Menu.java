package pl.polsl.angrygamers.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.List;

import static com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

/**
 * The type Menu.
 */
public class Menu extends WidgetGroup {

    private final Table menuTable;
    private final List<MenuListener> menuListeners = new ArrayList<>();
    private final FreeTypeFontGenerator generator;

    /**
     * Instantiates a new Menu.
     *
     * @param menuItems the menu items
     */
    public Menu(final List<MenuItem> menuItems) {
        menuTable = new Table();
        menuTable.setFillParent(true);
        menuTable.center();

        generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/prstart.ttf"));

        fillTable(menuItems);
        this.addActor(menuTable);
        this.setFillParent(true);
    }

    private void fillTable(final List<MenuItem> menuItems) {
        final TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
        final FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = Gdx.graphics.getHeight() / 12;
        buttonStyle.font = generator.generateFont(parameter);

        for(final MenuItem menuItem : menuItems) {
            final TextButton button = new TextButton(menuItem.getText(), buttonStyle);
            menuTable.add(button);
            menuTable.row();

            button.addListener(new ClickListener() {
                @Override
                public void clicked(final InputEvent event, final float x, final float y) {
                    onMenuItemClicked(menuItem);
                    super.clicked(event, x, y);
                }
            });
        }
    }

    /**
     * Add menu listener.
     *
     * @param menuListener the menu listener
     */
    public void addMenuListener(final MenuListener menuListener) {
        menuListeners.add(menuListener);
    }

    /**
     * Remove menu listener.
     *
     * @param menuListener the menu listener
     */
    public void removeMenuListener(final MenuListener menuListener) {
        menuListeners.remove(menuListener);
    }

    private void onMenuItemClicked(final MenuItem menuItem) {
        menuListeners.forEach(l -> l.onMenuItemClicked(menuItem));
    }

    /**
     * Update button sizes.
     */
    public void updateButtonSizes() {
        final TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
        final FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = Gdx.graphics.getHeight() / 12;
        buttonStyle.font = generator.generateFont(parameter);

        ((TextButton)menuTable.getCells().get(0).getActor()).setStyle(buttonStyle);
        ((TextButton)menuTable.getCells().get(1).getActor()).setStyle(buttonStyle);
//        ((TextButton)menuTable.getCells().get(2).getActor()).setStyle(buttonStyle);
    }

    /**
     * The Menu listener interface.
     */
    public interface MenuListener {
        /**
         * On menu item clicked.
         *
         * @param menuItem the menu item
         */
        void onMenuItemClicked(MenuItem menuItem);
    }

    /**
     * Dispose menu.
     */
    public void dispose() {
        generator.dispose();
    }
}
