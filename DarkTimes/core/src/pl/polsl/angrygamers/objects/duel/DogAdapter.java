package pl.polsl.angrygamers.objects.duel;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import pl.polsl.angrygamers.objects.animations.AnimatedBehaviour;
import pl.polsl.angrygamers.objects.gameObjects.Player;
import pl.polsl.angrygamers.utils.Range;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Dog adapter.
 */
public class DogAdapter implements VisualDuellist {

    private static final String BITE = "Grrr";
    private static final String SCRATCH = "Hutszzz";
    private static final Float DEFAULT_MAX_HP = 50.0f;

    private final Player player;
    private final AnimatedBehaviour animatedBehaviour;
    private final Texture icon;
    private Float hp;
    private final Float maxHp;
    private final String name;

    /**
     * Instantiates a new Dog adapter.
     *
     * @param player            the player
     * @param icon              the icon
     * @param animatedBehaviour the animated behaviour
     */
    public DogAdapter(final Player player, final Texture icon, final AnimatedBehaviour animatedBehaviour) {
        this(player, icon, animatedBehaviour, DEFAULT_MAX_HP);
    }

    private DogAdapter(final Player player, final Texture icon, final AnimatedBehaviour animatedBehaviour, final float maxHp) {
        if (maxHp <= 0.0) {
            throw new IllegalArgumentException("MaxHp cannot be equal or less than 0!");
        }

        this.player = player;
        this.icon = icon;
        this.animatedBehaviour = animatedBehaviour;
        this.maxHp = maxHp;
        this.hp = maxHp;
        this.name = "Wsciekly pies";
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean isIdle() {
        return animatedBehaviour.isInDefaultState();
    }

    @Override
    public Texture getIconTexture() {
        return icon;
    }

    @Override
    public Sprite getHudSprite() {
        return animatedBehaviour.getSprite();
    }

    @Override
    public float getHp() {
        return this.hp / DEFAULT_MAX_HP;
    }

    @Override
    public void attack(final Attack attack) {
        if(attack.getName().equals(BITE)) {
            animatedBehaviour.pushState("fighting-fists");
        } else {
            animatedBehaviour.pushState("fighting-fists");
        }
    }

    @Override
    public void attackMissed(final Attack attack) {
        animatedBehaviour.pushState("missed");
    }

    @Override
    public void attacked(final float damage) {
        final float hp = this.hp - damage;
        this.hp = com.badlogic.gdx.math.MathUtils.clamp(hp, 0.0f, maxHp);

        if(this.hp == 0) {
            animatedBehaviour.setDefaultState("defeated");
        } else {
            animatedBehaviour.pushState("hit");
        }
    }

    @Override
    public List<Attack> getAvailableAttacks() {
        final List<Attack> attacks = new ArrayList<>();
        final Attack attack1 = new DummyAttack(BITE, "Chomp chomp...",
                new Range(9.0f, 13.0f), 0.6f);
        final Attack attack2 = new DummyAttack(SCRATCH, "Scratch scratch...",
                new Range(4.0f, 7.0f), 0.8f);

        attacks.add(attack1);
        attacks.add(attack2);

        return attacks;
    }
}
