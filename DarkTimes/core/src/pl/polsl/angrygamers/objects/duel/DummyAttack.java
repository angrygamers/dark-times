package pl.polsl.angrygamers.objects.duel;

import pl.polsl.angrygamers.utils.Range;

/**
 * The type Dummy attack.
 */
public class DummyAttack implements Attack {

    private String name;
    private String description;
    private Range range;
    private float chance;

    /**
     * Instantiates a new Dummy attack.
     *
     * @param name        the name
     * @param description the description
     * @param range       the range
     * @param chance      the chance
     */
    public DummyAttack(String name, String description, Range range, float chance) {
        this.name = name;
        this.description = description;
        this.range = range;
        this.chance = chance;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Range getRange() {
        return range;
    }

    @Override
    public float getChance() {
        return chance;
    }
}
