package pl.polsl.angrygamers.objects.duel;

import pl.polsl.angrygamers.utils.Range;

/**
 * Attack form representing interface.
 */
public interface Attack {

    /**
     * Gets name.
     *
     * @return Attack name.
     */
    String getName();

    /**
     * Gets description.
     *
     * @return Attack description.
     */
    String getDescription();

    /**
     * Gets range.
     *
     * @return Attack strength range.
     */
    Range getRange();

    /**
     * Gets chance.
     *
     * @return Change to hit opponent.
     */
    float getChance();
}
