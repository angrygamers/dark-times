package pl.polsl.angrygamers.objects.duel;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Duelist visual representation.
 */
public interface VisualDuellist extends Duellist {

    /**
     * Gets icon texture.
     *
     * @return icon object texture.
     */
    Texture getIconTexture();

    /**
     * Gets hud sprite.
     *
     * @return object Sprite.
     */
    Sprite getHudSprite();
}
