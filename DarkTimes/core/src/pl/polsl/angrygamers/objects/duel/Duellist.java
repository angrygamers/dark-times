package pl.polsl.angrygamers.objects.duel;

import java.util.List;

/**
 * Represents duelist object.
 */
public interface Duellist {

    /**
     * Gets name.
     *
     * @return duelist name.
     */
    String getName();

    /**
     * Is idle boolean.
     *
     * @return is duelist in idle state.
     */
    boolean isIdle();

    /**
     * Gets hp.
     * Health points are in range 0.0 - 1.0, where 0.0 means death, 1.0 means no damage.
     * @return health points.
     */
    float getHp();

    /**
     * Attack.
     *
     * @param attack Attack to perform.
     */
    void attack(Attack attack);

    /**
     * Attack that missed opponent.
     *
     * @param attack Attack to perform.
     */
    void attackMissed(Attack attack);

    /**
     * Method, that is executed when duelist is attacked.
     *
     * @param damage damage value (different according to type of Attack).
     */
    void attacked(float damage);

    /**
     * Gets available attacks.
     *
     * @return list of available Attacks.
     */
    List<Attack> getAvailableAttacks();
}
