package pl.polsl.angrygamers.objects.duel;

import com.badlogic.gdx.scenes.scene2d.Actor;
import pl.polsl.angrygamers.screens.duel.DuelScreen;
import pl.polsl.angrygamers.utils.Range;

import java.util.Random;

/**
 * The type Duel manager.
 */
public class DuelManager extends Actor {

    private final DuelScreen duelScreen;
    private final VisualDuellist duellist1;
    private final VisualDuellist duellist2;
    private final Random random;
    private VisualDuellist attackingDuellist;
    private Runnable onIdleAction;

    /**
     * Instantiates a new Duel manager.
     *
     * @param duelScreen the duel screen
     * @param duellist1  the duellist 1
     * @param duellist2  the duellist 2
     */
    public DuelManager(DuelScreen duelScreen, VisualDuellist duellist1, VisualDuellist duellist2) {
        this.duelScreen = duelScreen;
        this.duellist1 = duellist1;
        this.duellist2 = duellist2;
        this.random = new Random();
        attackingDuellist = duellist1;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        final Runnable onIdleAction = this.onIdleAction;
        if (onIdleAction != null && getAttackingDuellist().isIdle()) {
            onIdleAction.run();
            this.onIdleAction = null;
        }
    }

    /**
     * Start duel.
     */
    public void startDuel() {
        showAvailableAttacks(attackingDuellist);
    }

    private void showAvailableAttacks(VisualDuellist duellist) {
        duelScreen.showAttacksPane(duellist);
    }

    /**
     * On attack chosen.
     *
     * @param attack the attack
     */
    public void onAttackChosen(final Attack attack) {
        final Float damage = diceRollAttack(attack);
        final VisualDuellist attackingDuellist = getAttackingDuellist();

        if (damage == null) {
            attackMissed(attack, attackingDuellist);
        } else {
            attackSucceed(attack, damage, attackingDuellist);
        }
    }

    private void attackMissed(Attack attack, VisualDuellist attackingDuellist) {
        attackingDuellist.attackMissed(attack);
        duelScreen.onAttackFail(attackingDuellist, attack);
        onIdleAction = () -> {
            toggleAttackingDuellist();
            showAvailableAttacks(getAttackingDuellist());
        };
    }

    private void attackSucceed(Attack attack, final Float damage, final VisualDuellist attackingDuellist) {
        attackingDuellist.attack(attack);
        onIdleAction = () -> {
            final VisualDuellist defendingDuellist = getDefendingDuellist();
            defendingDuellist.attacked(damage);
            duelScreen.onAttackSuccess(defendingDuellist, damage);

            if (defendingDuellist.getHp() > 0) {
                toggleAttackingDuellist();
                showAvailableAttacks(defendingDuellist);
            } else {
                duelScreen.onDuellistDefeated(defendingDuellist);
            }
        };
    }

    private VisualDuellist getAttackingDuellist() {
        return attackingDuellist;
    }

    private VisualDuellist getDefendingDuellist() {
        return attackingDuellist == duellist1 ? duellist2 : duellist1;
    }

    private void toggleAttackingDuellist() {
        attackingDuellist = attackingDuellist == duellist1 ? duellist2 : duellist1;
    }

    private Float diceRollAttack(Attack attack) {
        boolean attackSuccess = random.nextFloat() <= attack.getChance();
        return attackSuccess ? diceRollAttackValue(attack.getRange()) : null;
    }

    private Float diceRollAttackValue(Range range) {
        float factor = range.getUntil() - range.getFrom();
        return (float) (this.random.nextGaussian() + 1.0) * factor + range.getFrom();
    }
}
