package pl.polsl.angrygamers.objects.duel;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import pl.polsl.angrygamers.objects.animations.AnimatedBehaviour;
import pl.polsl.angrygamers.objects.gameObjects.Player;
import pl.polsl.angrygamers.utils.Range;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The type Duellist adapter.
 */
public class DuellistAdapter implements VisualDuellist {

    private static final String SWORD_FIGHT = "Uderzenie mieczem";
    private static final String FISTS_FIGHT = "Siniak pod okiem";
    private static final Float DEFAULT_MAX_HP = 50.0f;

    private final Player player;
    private final AnimatedBehaviour animatedBehaviour;
    private final Texture icon;
    private Float hp;
    private Float maxHp;
    private String name;

    /**
     * Instantiates a new Duellist adapter.
     *
     * @param player            the player
     * @param icon              the icon
     * @param animatedBehaviour the animated behaviour
     */
    public DuellistAdapter(Player player, Texture icon, AnimatedBehaviour animatedBehaviour) {
        this(player, icon, animatedBehaviour, DEFAULT_MAX_HP);
    }

    /**
     * Instantiates a new Duellist adapter.
     *
     * @param player            the player
     * @param icon              the icon
     * @param animatedBehaviour the animated behaviour
     * @param maxHp             the max hp
     */
    public DuellistAdapter(Player player, Texture icon, AnimatedBehaviour animatedBehaviour, float maxHp) {
        if (maxHp <= 0.0) {
            throw new IllegalArgumentException("MaxHp cannot be equal or less than 0!");
        }

        this.player = player;
        this.icon = icon;
        this.animatedBehaviour = animatedBehaviour;
        this.maxHp = maxHp;
        this.hp = maxHp;
        this.name = "Bot nr " + new Random().nextInt(9) + 1;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean isIdle() {
        return animatedBehaviour.isInDefaultState();
    }

    @Override
    public Texture getIconTexture() {
        return icon;
    }

    @Override
    public Sprite getHudSprite() {
        return animatedBehaviour.getSprite();
    }

    @Override
    public float getHp() {
        return this.hp / DEFAULT_MAX_HP;
    }

    @Override
    public void attack(Attack attack) {
        if(attack.getName().equals(SWORD_FIGHT)) {
            animatedBehaviour.pushState("fighting");
        } else {
            animatedBehaviour.pushState("fighting-fists");
        }
    }

    @Override
    public void attackMissed(Attack attack) {
        animatedBehaviour.pushState("missed");
    }

    @Override
    public void attacked(float damage) {
        float hp = this.hp - damage;
        this.hp = com.badlogic.gdx.math.MathUtils.clamp(hp, 0.0f, maxHp);

        if(this.hp == 0) {
            animatedBehaviour.setDefaultState("defeated");
        } else {
            animatedBehaviour.pushState("hit");
        }
    }

    @Override
    public List<Attack> getAvailableAttacks() {
        List<Attack> attacks = new ArrayList<>();
        Attack attack1 = new DummyAttack(SWORD_FIGHT, "Miecz jest potezny...",
                new Range(10.0f, 14.0f), 0.4f);
        Attack attack2 = new DummyAttack(FISTS_FIGHT, "o-O",
                new Range(4.0f, 5.0f), 0.9f);

        attacks.add(attack1);
        attacks.add(attack2);

        return attacks;
    }
}
