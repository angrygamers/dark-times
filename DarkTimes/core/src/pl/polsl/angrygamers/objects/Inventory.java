package pl.polsl.angrygamers.objects;

import com.badlogic.gdx.ScreenAdapter;

import java.util.ArrayList;

/**
 * The type Inventory.
 */
public class Inventory extends ScreenAdapter {

    private static final int NUM_SLOTS = 10;
    /**
     * Is inventory overfilled.
     */
    public boolean tooFull;

    private ArrayList<Item> items;

    /**
     * Instantiates a new Inventory.
     */
    public Inventory() {
        items = new ArrayList<>();
    }

    /**
     * Add item.
     *
     * @param item the item
     */
    public void addItem(Item item) {
        if (items.size() < NUM_SLOTS) {
            items.add(item);
            item.takeItem();
        } else {
            tooFull = true;
        }
    }

    /**
     * Get number of free slots.
     *
     * @return the int
     */
    public int getFreeSlotsNumber(){
        return 6 - items.size();
    }

    /**
     * Gets inventory items.
     *
     * @return the items
     */
    public ArrayList<Item> getItems() {
        return items;
    }
}
