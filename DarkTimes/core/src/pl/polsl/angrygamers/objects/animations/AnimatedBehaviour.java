package pl.polsl.angrygamers.objects.animations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Queue;

/**
 * The type Animated behaviour.
 */
public class AnimatedBehaviour implements Disposable {

    private Sprite sprite;
    private AnimationsState animationsState;
    private String defaultState;
    private Queue<String> statesQueue;
    private float deltaTime;

    /**
     * Instantiates a new Animated behaviour.
     *
     * @param animationsState the animations state
     * @param defaultState    the default state
     */
    public AnimatedBehaviour(AnimationsState animationsState, String defaultState) {
        this.animationsState = animationsState;
        this.defaultState = defaultState;
        this.statesQueue = new Queue<>();
        this.animationsState.setState(defaultState);
        this.sprite = new Sprite(this.animationsState.getKeyFrame(0));
        this.sprite.setSize(150.0f, 130.0f);
    }

    /**
     * Push state.
     *
     * @param name the name
     */
    public void pushState(String name) {
        statesQueue.addLast(name);
    }

    /**
     * Sets default state.
     *
     * @param defaultState the default state
     */
    public void setDefaultState(String defaultState) {
        this.defaultState = defaultState;
    }

    private void ensureAnimationUpToDate() {
        deltaTime += Gdx.graphics.getDeltaTime();

        Animation<TextureRegion> animation = animationsState.getCurrentAnimation();
        boolean animationFinished = animation.isAnimationFinished(deltaTime);
        if(animationFinished) {
            onAnimationFinished();
        }

        sprite.setRegion(animationsState.getKeyFrame(deltaTime, true));
    }

    private void onAnimationFinished() {
        deltaTime = 0.0f;
        if(!statesQueue.isEmpty()) {
            String nextAnimation = statesQueue.removeFirst();
            animationsState.setState(nextAnimation);
        } else {
            animationsState.setState(defaultState);
        }
    }

    /**
     * Gets sprite.
     *
     * @return the sprite
     */
    public Sprite getSprite() {
        ensureAnimationUpToDate();
        return sprite;
    }

    @Override
    public void dispose() {
        animationsState.dispose();
    }

    /**
     * Is in default state boolean.
     *
     * @return the boolean
     */
    public boolean isInDefaultState() {
        return animationsState.getState().equals(defaultState) && statesQueue.isEmpty();
    }
}
