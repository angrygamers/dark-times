package pl.polsl.angrygamers.objects.animations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;

import java.util.*;

/**
 * The type Animations state.
 */
public class AnimationsState implements Disposable {

    private static final String DEFAULT_STATE_NAME = "DEFAULT_STATE";

    private static List<StateEntry> createDefaultStates(int framesCount) {
        ArrayList<StateEntry> animations = new ArrayList<>();
        animations.add(new StateEntry(DEFAULT_STATE_NAME, 0, framesCount - 1));
        return animations;
    }

    private static final Float DEFAULT_FRAME_DURATION = 0.025f;

    private final Texture texture;
    private final Map<String, Animation<TextureRegion>> statesMap;
    private String stateName;

    /**
     * Instantiates a new Animations state.
     *
     * @param texturePath the texture path
     * @param rows        the rows
     * @param columns     the columns
     */
    public AnimationsState(String texturePath, int rows, int columns) {
        this(texturePath, rows, columns, rows * columns);
    }

    /**
     * Instantiates a new Animations state.
     *
     * @param texturePath the texture path
     * @param rows        the rows
     * @param columns     the columns
     * @param framesCount the frames count
     */
    public AnimationsState(String texturePath, int rows, int columns, int framesCount) {
        this(texturePath, rows, columns, framesCount, createDefaultStates(framesCount));
        setState(DEFAULT_STATE_NAME);
    }

    /**
     * Instantiates a new Animations state.
     *
     * @param texturePath      the texture path
     * @param rows             the rows
     * @param columns          the columns
     * @param framesCount      the frames count
     * @param animationEntries the animation entries
     */
    public AnimationsState(String texturePath, int rows, int columns, int framesCount, List<StateEntry> animationEntries) {
        if (rows <= 0) {
            throw new IllegalArgumentException("Rows count should be greater than 0!");
        }
        if (columns <= 0) {
            throw new IllegalArgumentException("Columns count should be greater than 0!");
        }

        this.texture = new Texture(Gdx.files.internal(texturePath));
        statesMap = new HashMap<String, Animation<>>();
        stateName = null;
        TextureRegion[] frames = createFrames(texture, rows, columns, framesCount);
        fillMap(frames, animationEntries);
    }

    /**
     * Sets state.
     *
     * @param stateName the state name
     */
    void setState(String stateName) {
        if (!statesMap.containsKey(stateName)) {
            throw new IllegalStateException("State " + stateName + " was not found!");
        }
        this.stateName = stateName;
    }

    /**
     * Gets state.
     *
     * @return the state
     */
    String getState() {
        return stateName;
    }

    /**
     * Gets current animation.
     *
     * @return the current animation
     */
    public Animation<TextureRegion> getCurrentAnimation() {
        return statesMap.get(this.stateName);
    }

    /**
     * Sets frame duration.
     *
     * @param frameDuration the frame duration
     */
    public void setFrameDuration(float frameDuration) {
        for(Map.Entry<String, Animation<TextureRegion>> entry : statesMap.entrySet()) {
            entry.getValue().setFrameDuration(frameDuration);
        }
    }

    private TextureRegion[] createFrames(Texture texture, int rows, int columns, int framesCount) {
        if (framesCount <= 0) {
            throw new IllegalArgumentException("Frames count should be greater than 0!");
        }

        TextureRegion[][] tmp = TextureRegion.split(texture, texture.getWidth() / columns,
                texture.getHeight() / rows);
        TextureRegion[] frames = new TextureRegion[framesCount];

        int index = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns && index < framesCount; j++) {
                frames[index++] = tmp[i][j];
            }
        }

        return frames;
    }

    private void fillMap(TextureRegion[] frames, List<StateEntry> animationEntries) {
        for (StateEntry entry : animationEntries) {
            if (entry.endIndexInclusive >= frames.length) {
                throw new IllegalArgumentException("Entry's end index cannot be equal or greater than frames count!");
            }

            TextureRegion[] animationFrames = Arrays.copyOfRange(frames, entry.startIndexInclusive, entry.endIndexInclusive);
            Animation animation = new Animation<>(DEFAULT_FRAME_DURATION, animationFrames);
            statesMap.put(entry.name, animation);
        }
    }

    /**
     * Gets key frame.
     *
     * @param stateTime the state time
     * @return the key frame
     */
    public TextureRegion getKeyFrame(float stateTime) {
        return getStateAnimation().getKeyFrame(stateTime);
    }

    /**
     * Gets key frame.
     *
     * @param deltaTime the delta time
     * @param looping   the looping
     * @return the key frame
     */
    public TextureRegion getKeyFrame(float deltaTime, boolean looping) {
        return getStateAnimation().getKeyFrame(deltaTime, looping);
    }

    private Animation<TextureRegion> getStateAnimation() {
        if(stateName == null) {
            throw new IllegalStateException("State cannot be null!");
        }
        return statesMap.get(stateName);
    }

    @Override
    public void dispose() {
        texture.dispose();
    }

    /**
     * Timing animations state.
     *
     * @param v the v
     * @return the animations state
     */
    public AnimationsState timing(float v) {
        statesMap.values().forEach(textureRegionAnimation ->
                textureRegionAnimation.setFrameDuration(v));
        return this;
    }

    /**
     * The type State entry.
     */
    public static class StateEntry {
        /**
         * The Name.
         */
        final String name;
        /**
         * The Start index inclusive.
         */
        final int startIndexInclusive;
        /**
         * The End index inclusive.
         */
        final int endIndexInclusive;

        /**
         * Instantiates a new State entry.
         *
         * @param name                the name
         * @param startIndexInclusive the start index inclusive
         * @param count               the count
         */
        public StateEntry(String name, int startIndexInclusive, int count) {
            if (startIndexInclusive < 0) {
                throw new IllegalArgumentException("Start index must be greater than 0!");
            }
            if (count < 0) {
                throw new IllegalArgumentException("Count must be greater than 0!");
            }

            this.name = name;
            this.startIndexInclusive = startIndexInclusive;
            this.endIndexInclusive = startIndexInclusive + count - 1;
        }
    }
}
