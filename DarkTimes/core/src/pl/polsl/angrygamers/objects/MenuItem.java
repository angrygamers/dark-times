package pl.polsl.angrygamers.objects;

/**
 * The type Menu item.
 */
public class MenuItem {
    private int id;
    private String text;

    /**
     * Instantiates a new Menu item.
     *
     * @param id   the id
     * @param text the text
     */
    public MenuItem(int id, String text) {
        this.id = id;
        this.text = text;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * Sets text.
     *
     * @param text the text
     */
    public void setText(String text) {
        this.text = text;
    }
}
