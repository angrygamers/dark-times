package pl.polsl.angrygamers.objects.gameObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.enums.Direction;
import pl.polsl.angrygamers.utils.CircularArrayList;
import pl.polsl.angrygamers.utils.Size;
import pl.polsl.angrygamers.utils.TextureData;

import java.util.List;

import static pl.polsl.angrygamers.utils.GlobalVariables.DIRECTION_CHANGE_INTERVAL;

/**
 * The type Autonomous game object.
 */
class AutonomousGameObject extends GameObject {
    private final CircularArrayList<Direction> movementPath;
    /**
     * The Current direction value.
     */
    Direction currentDirection;
    private Boolean visible = false;

    private Long start = 0L;

    /**
     * Instantiates a new Autonomous game object.
     *
     * @param position     the position
     * @param speed        the speed
     * @param size         the size
     * @param textureData  the texture data
     * @param movementPath the movement path
     */
    AutonomousGameObject(final Vector2 position, final Float speed, final Size size, final TextureData textureData, final CircularArrayList<Direction> movementPath) {
        super(position, speed, size, textureData);
        this.movementPath = movementPath;
    }

    /**
     * Instantiates a new Autonomous game object.
     *
     * @param position       the position
     * @param speed          the speed
     * @param size           the size
     * @param horizontalSize the horizontal size
     * @param verticalSize   the vertical size
     * @param textures       the textures
     * @param movementPath   the movement path
     */
    AutonomousGameObject(final Vector2 position, final Float speed, final Size size, final Size horizontalSize, final Size verticalSize, final List<TextureData> textures, final CircularArrayList<Direction> movementPath) {
        super(position, speed, size, horizontalSize, verticalSize, textures);
        this.movementPath = movementPath;
    }

    /**
     * Update autonomous movement.
     */
    void updateAutoMovement(){
        if(movementPath.isEmpty()) {
            return;
        }

        currentDirection = movementPath.get();
        if(start > DIRECTION_CHANGE_INTERVAL){
            currentDirection = movementPath.next();
            start = 0L;
        }
        start++;
        updateMovement(currentDirection);
    }

    private Animation<TextureRegion> getAnimationBasedOnCurrentDirection() {
        if (currentDirection == Direction.UP || currentDirection == Direction.UP_RIGHT) {
            if (visible) {
                return animationUp;
            }
            return animationUpBorder;
        } else if (currentDirection == Direction.RIGHT || currentDirection == Direction.DOWN_RIGHT) {
            if (visible) {
                return animationRight;
            }
            return animationRightBorder;
        } else if (currentDirection == Direction.DOWN || currentDirection == Direction.DOWN_LEFT) {
            if (visible) {
                return animationDown;
            }
            return animationDownBorder;
        } else {
            if (visible) {
                return animationLeft;
            }
            return animationLeftBorder;
        }
    }

    private Sprite getSpriteBasedOnCurrentDirection() {
        if (currentDirection == Direction.UP || currentDirection == Direction.UP_RIGHT) {
            if (visible) {
                return spriteUp;
            }
            return spriteUpBorder;
        } else if (currentDirection == Direction.RIGHT || currentDirection == Direction.DOWN_RIGHT) {
            if (visible) {
                return spriteRight;
            }
            return spriteRightBorder;
        } else if (currentDirection == Direction.DOWN || currentDirection == Direction.DOWN_LEFT) {
            if (visible) {
                return spriteDown;
            }
            return spriteDownBorder;
        } else {
            if (visible) {
                return spriteLeft;
            }
            return spriteLeftBorder;
        }
    }

    @Override
    public void draw(final SpriteBatch batch) {
        deltaTime += Gdx.graphics.getDeltaTime();
        if (sprite != null) {
            sprite.setRegion(animation.getKeyFrame(deltaTime, true));
            sprite.setOriginBasedPosition(getPosition().x, getPosition().y);
            sprite.draw(batch);
        } else {
            final Sprite sprite = getSpriteBasedOnCurrentDirection();
            sprite.setRegion(getAnimationBasedOnCurrentDirection().getKeyFrame(deltaTime, true));
            sprite.setOriginBasedPosition(getPosition().x, getPosition().y);
            sprite.draw(batch);
        }
    }

    @Override
    public Sprite getSprite() {
        if (sprite != null) {
            return sprite;
        } else {
            return getSpriteBasedOnCurrentDirection();
        }
    }

    /**
     * Is visible boolean.
     *
     * @return the boolean
     */
    public Boolean isVisible() {
        return visible;
    }

    /**
     * Sets visible boolean.
     *
     * @param visible the visible
     * @return the visible
     */
    public AutonomousGameObject setVisible(final Boolean visible) {
        this.visible = visible;
        return this;
    }
}
