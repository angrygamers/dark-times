package pl.polsl.angrygamers.objects.gameObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.enums.Direction;
import pl.polsl.angrygamers.utils.Size;
import pl.polsl.angrygamers.utils.TextureData;
import pl.polsl.angrygamers.utils.maps.MapRenderer;
import pl.polsl.angrygamers.utils.maps.MapUtils;

import java.util.List;

/**
 * The type Game object.
 */
public class GameObject {
    /**
     * The Delta time value.
     */
    float deltaTime;

    /**
     * The main Animation object.
     */
// Objects used
    Animation<TextureRegion> animation;
    /**
     * The direction up Animation object.
     */
    Animation<TextureRegion> animationUp;
    /**
     * The direction right Animation object.
     */
    Animation<TextureRegion> animationRight;
    /**
     * The direction down Animation object.
     */
    Animation<TextureRegion> animationDown;
    /**
     * The direction left Animation object.
     */
    Animation<TextureRegion> animationLeft;
    /**
     * The direction up border Animation object.
     */
    Animation<TextureRegion> animationUpBorder;
    /**
     * The direction  right borderAnimation object.
     */
    Animation<TextureRegion> animationRightBorder;
    /**
     * The direction down borderAnimation object.
     */
    Animation<TextureRegion> animationDownBorder;
    /**
     * The direction left border Animation object.
     */
    Animation<TextureRegion> animationLeftBorder;
    private Vector2 position;
    /**
     * The Speed.
     */
    Float speed;
    /**
     * The Sprite of game object.
     */
    final Sprite sprite;
    /**
     * The up Sprite.
     */
    Sprite spriteUp;
    /**
     * The right Sprite.
     */
    Sprite spriteRight;
    /**
     * The down Sprite.
     */
    Sprite spriteDown;
    /**
     * The left Sprite.
     */
    Sprite spriteLeft;
    /**
     * The up border Sprite.
     */
    Sprite spriteUpBorder;
    /**
     * The right border Sprite.
     */
    Sprite spriteRightBorder;
    /**
     * The down border Sprite.
     */
    Sprite spriteDownBorder;
    /**
     * The left border Sprite.
     */
    Sprite spriteLeftBorder;
    private final Rectangle spriteRectangle;
    private final Texture texture;
    private final Texture textureUp;
    private final Texture textureRight;
    private final Texture textureDown;
    private final Texture textureLeft;
    private Texture textureUpBorder;
    private Texture textureRightBorder;
    private Texture textureDownBorder;
    private Texture textureLeftBorder;

    private boolean touched;

    /**
     * Instantiates a new Game object.
     *
     * @param position       the position
     * @param speed          the speed
     * @param size           the size
     * @param horizontalSize the horizontal size
     * @param verticalSize   the vertical size
     * @param textures       the textures
     */
    GameObject(final Vector2 position, final Float speed, final Size size, final Size horizontalSize, final Size verticalSize, final List<TextureData> textures) {
        this.position = position;
        this.speed = speed;
        animation = null;
        texture = null;
        sprite = null;

        textureUp = new Texture(textures.get(0).getTexturePath());
        TextureRegion[][] tmp = TextureRegion.split(textureUp, textureUp.getWidth()/textures.get(0).getCols(),
            textureUp.getHeight() / textures.get(0).getRows());
        TextureRegion[] frames = new TextureRegion[textures.get(0).getRows()*textures.get(0).getCols()];
        int index = 0;
        for (int i = 0; i < textures.get(0).getRows(); i++) {
            for (int j = 0; j < textures.get(0).getCols(); j++) {
                frames[index++] = tmp[i][j];
            }
        }
        animationUp = new Animation<>(textures.get(0).getFrameRate(), frames);

        textureRight = new Texture(textures.get(1).getTexturePath());
        tmp = TextureRegion.split(textureRight, textureRight.getWidth()/textures.get(1).getCols(),
            textureRight.getHeight() / textures.get(1).getRows());
        frames = new TextureRegion[textures.get(1).getRows()*textures.get(1).getCols()];
        index = 0;
        for (int i = 0; i < textures.get(1).getRows(); i++) {
            for (int j = 0; j < textures.get(1).getCols(); j++) {
                frames[index++] = tmp[i][j];
            }
        }
        animationRight = new Animation<>(textures.get(1).getFrameRate(), frames);

        textureDown = new Texture(textures.get(2).getTexturePath());
        tmp = TextureRegion.split(textureDown, textureDown.getWidth()/textures.get(2).getCols(),
            textureDown.getHeight() / textures.get(2).getRows());
        frames = new TextureRegion[textures.get(2).getRows()*textures.get(2).getCols()];
        index = 0;
        for (int i = 0; i < textures.get(2).getRows(); i++) {
            for (int j = 0; j < textures.get(2).getCols(); j++) {
                frames[index++] = tmp[i][j];
            }
        }
        animationDown = new Animation<>(textures.get(2).getFrameRate(), frames);

        textureLeft = new Texture(textures.get(3).getTexturePath());
        tmp = TextureRegion.split(textureLeft, textureLeft.getWidth()/textures.get(3).getCols(),
            textureLeft.getHeight() / textures.get(3).getRows());
        frames = new TextureRegion[textures.get(3).getRows()*textures.get(3).getCols()];
        index = 0;
        for (int i = 0; i < textures.get(3).getRows(); i++) {
            for (int j = 0; j < textures.get(3).getCols(); j++) {
                frames[index++] = tmp[i][j];
            }
        }
        animationLeft = new Animation<>(textures.get(3).getFrameRate(), frames);

        spriteUp = new Sprite(animationUp.getKeyFrame(0));
        spriteUp.setSize(verticalSize.getWidth(), verticalSize.getHeight());
        spriteUp.setCenter(verticalSize.getWidth()/2, verticalSize.getHeight()/2);
        spriteUp.setOriginCenter();
        spriteUp.setOriginBasedPosition(position.x + spriteUp.getOriginX(), position.y + spriteUp.getOriginY());

        spriteRight = new Sprite(animationRight.getKeyFrame(0));
        spriteRight.setSize(horizontalSize.getWidth(), horizontalSize.getHeight());
        spriteRight.setCenter(horizontalSize.getWidth()/2, horizontalSize.getHeight()/2);
        spriteRight.setOriginCenter();
        spriteRight.setOriginBasedPosition(position.x + spriteRight.getOriginX(), position.y + spriteRight.getOriginY());

        spriteDown = new Sprite(animationDown.getKeyFrame(0));
        spriteDown.setSize(verticalSize.getWidth(), verticalSize.getHeight());
        spriteDown.setCenter(verticalSize.getWidth()/2, verticalSize.getHeight()/2);
        spriteDown.setOriginCenter();
        spriteDown.setOriginBasedPosition(position.x + spriteDown.getOriginX(), position.y + spriteDown.getOriginY());

        spriteLeft = new Sprite(animationLeft.getKeyFrame(0));
        spriteLeft.setSize(horizontalSize.getWidth(), horizontalSize.getHeight());
        spriteLeft.setCenter(horizontalSize.getWidth()/2, horizontalSize.getHeight()/2);
        spriteLeft.setOriginCenter();
        spriteLeft.setOriginBasedPosition(position.x + spriteLeft.getOriginX(), position.y + spriteLeft.getOriginY());

        if (textures.size() == 8) {
            textureUpBorder = new Texture(textures.get(4).getTexturePath());
            tmp = TextureRegion.split(textureUpBorder, textureUpBorder.getWidth()/textures.get(4).getCols(),
                textureUpBorder.getHeight() / textures.get(4).getRows());
            frames = new TextureRegion[textures.get(4).getRows()*textures.get(4).getCols()];
            index = 0;
            for (int i = 0; i < textures.get(4).getRows(); i++) {
                for (int j = 0; j < textures.get(4).getCols(); j++) {
                    frames[index++] = tmp[i][j];
                }
            }
            animationUpBorder = new Animation<>(textures.get(4).getFrameRate(), frames);

            textureRightBorder = new Texture(textures.get(5).getTexturePath());
            tmp = TextureRegion.split(textureRightBorder, textureRightBorder.getWidth()/textures.get(5).getCols(),
                textureRightBorder.getHeight() / textures.get(5).getRows());
            frames = new TextureRegion[textures.get(5).getRows()*textures.get(5).getCols()];
            index = 0;
            for (int i = 0; i < textures.get(5).getRows(); i++) {
                for (int j = 0; j < textures.get(5).getCols(); j++) {
                    frames[index++] = tmp[i][j];
                }
            }
            animationRightBorder = new Animation<>(textures.get(5).getFrameRate(), frames);

            textureDownBorder = new Texture(textures.get(6).getTexturePath());
            tmp = TextureRegion.split(textureDownBorder, textureDownBorder.getWidth()/textures.get(6).getCols(),
                textureDownBorder.getHeight() / textures.get(6).getRows());
            frames = new TextureRegion[textures.get(6).getRows()*textures.get(6).getCols()];
            index = 0;
            for (int i = 0; i < textures.get(6).getRows(); i++) {
                for (int j = 0; j < textures.get(6).getCols(); j++) {
                    frames[index++] = tmp[i][j];
                }
            }
            animationDownBorder = new Animation<>(textures.get(6).getFrameRate(), frames);

            textureLeftBorder = new Texture(textures.get(7).getTexturePath());
            tmp = TextureRegion.split(textureLeftBorder, textureLeftBorder.getWidth()/textures.get(7).getCols(),
                textureLeftBorder.getHeight() / textures.get(7).getRows());
            frames = new TextureRegion[textures.get(7).getRows()*textures.get(7).getCols()];
            index = 0;
            for (int i = 0; i < textures.get(7).getRows(); i++) {
                for (int j = 0; j < textures.get(7).getCols(); j++) {
                    frames[index++] = tmp[i][j];
                }
            }
            animationLeftBorder = new Animation<>(textures.get(7).getFrameRate(), frames);

            spriteUpBorder = new Sprite(animationUpBorder.getKeyFrame(0));
            spriteUpBorder.setSize(verticalSize.getWidth(), verticalSize.getHeight());
            spriteUpBorder.setCenter(verticalSize.getWidth()/2, verticalSize.getHeight()/2);
            spriteUpBorder.setOriginCenter();
            spriteUpBorder.setOriginBasedPosition(position.x + spriteUpBorder.getOriginX(), position.y + spriteUpBorder.getOriginY());

            spriteRightBorder = new Sprite(animationRightBorder.getKeyFrame(0));
            spriteRightBorder.setSize(horizontalSize.getWidth(), horizontalSize.getHeight());
            spriteRightBorder.setCenter(horizontalSize.getWidth()/2, horizontalSize.getHeight()/2);
            spriteRightBorder.setOriginCenter();
            spriteRightBorder.setOriginBasedPosition(position.x + spriteRightBorder.getOriginX(), position.y + spriteRightBorder.getOriginY());

            spriteDownBorder = new Sprite(animationDownBorder.getKeyFrame(0));
            spriteDownBorder.setSize(verticalSize.getWidth(), verticalSize.getHeight());
            spriteDownBorder.setCenter(verticalSize.getWidth()/2, verticalSize.getHeight()/2);
            spriteDownBorder.setOriginCenter();
            spriteDownBorder.setOriginBasedPosition(position.x + spriteDownBorder.getOriginX(), position.y + spriteDownBorder.getOriginY());

            spriteLeftBorder = new Sprite(animationLeftBorder.getKeyFrame(0));
            spriteLeftBorder.setSize(horizontalSize.getWidth(), horizontalSize.getHeight());
            spriteLeftBorder.setCenter(horizontalSize.getWidth()/2, horizontalSize.getHeight()/2);
            spriteLeftBorder.setOriginCenter();
            spriteLeftBorder.setOriginBasedPosition(position.x + spriteLeftBorder.getOriginX(), position.y + spriteLeftBorder.getOriginY());
        }

        position.x = spriteUp.getX();
        position.y = spriteUp.getY();

        spriteRectangle = new Rectangle(position.x, position.y, size.getWidth(), size.getHeight());
    }

    /**
     * Instantiates a new Game object.
     *
     * @param position    the position
     * @param speed       the speed
     * @param size        the size
     * @param textureData the texture data
     */
    GameObject(final Vector2 position, final Float speed, final Size size, final TextureData textureData) {
        this.position = position;
        this.speed = speed;
        texture = new Texture(textureData.getTexturePath());

        final TextureRegion[] frames = TextureData.getFrames(texture, textureData);
        animation = new Animation<>(textureData.getFrameRate(), frames);

        textureUp = null;
        textureRight = null;
        textureDown = null;
        textureLeft = null;
        textureUpBorder = null;
        textureRightBorder = null;
        textureDownBorder = null;
        textureLeftBorder = null;

        animationUp = null;
        animationRight = null;
        animationDown = null;
        animationLeft = null;

        spriteUp = null;
        spriteRight = null;
        spriteDown = null;
        spriteLeft = null;

        sprite = new Sprite(animation.getKeyFrame(0));
        sprite.setSize(size.getWidth(), size.getHeight());
        sprite.setCenter(size.getWidth()/2, size.getHeight()/2);
        sprite.setOriginCenter();
        sprite.setOriginBasedPosition(position.x + sprite.getOriginX(), position.y + sprite.getOriginY());
        position.x = sprite.getX();
        position.y = sprite.getY();

        spriteRectangle = new Rectangle(position.x, position.y, size.getWidth(), size.getHeight());
    }

    /**
     * Update movement.
     *
     * @param direction the direction
     */
    void updateMovement(final Direction direction){
        switch(direction){
            case UP:
                if (!collidesWithTerrain(position.x, position.y + speed)) {
                    position.y += speed;
                }
                break;
            case UP_LEFT:
                if (!collidesWithTerrain(position.x - speed / 2, position.y + speed / 2)) {
                    position.y += speed / 2;
                    position.x -= speed / 2;
                }
                break;
            case DOWN:
                if (!collidesWithTerrain(position.x, position.y - speed)) {
                    position.y -= speed;
                }
                break;
            case DOWN_LEFT:
                if (!collidesWithTerrain(position.x - speed / 2, position.y - speed / 2)) {
                    position.y -= speed / 2;
                    position.x -= speed / 2;
                }
                break;
            case RIGHT:
                if (!collidesWithTerrain(position.x + speed, position.y)) {
                    position.x += speed;
                }
                break;
            case UP_RIGHT:
                if (!collidesWithTerrain(position.x + speed / 2, position.y + speed / 2)) {
                    position.x += speed / 2;
                    position.y += speed / 2;
                }
                break;
            case DOWN_RIGHT:
                if (!collidesWithTerrain(position.x + speed / 2, position.y - speed / 2)) {
                    position.x += speed / 2;
                    position.y -= speed / 2;
                }
                break;
            case LEFT:
                if (!collidesWithTerrain(position.x - speed, position.y)) {
                    position.x -= speed;
                }
                break;
        }
    }

    /**
     * Collides with terrain boolean.
     *
     * @param x the x
     * @param y the y
     * @return the boolean
     */
    protected boolean collidesWithTerrain(final float x, final float y) {
        final TiledMap map = MapRenderer.getInstance().getMap();
        final MapObjects collisionLayer = MapUtils.getCollisionLayer(map);
        spriteRectangle.setPosition(x - spriteRectangle.width / 2, y - spriteRectangle.height / 2);
        for (final RectangleMapObject rectangle : collisionLayer.getByType(RectangleMapObject.class)) {
            if (Intersector.overlaps(rectangle.getRectangle(), spriteRectangle)) {
                return true;
            }
        }

        final Polygon playerPolygon = new Polygon(new float[] { 0, 0, spriteRectangle.width, 0, spriteRectangle.width,
            spriteRectangle.height, 0, spriteRectangle.height});
        playerPolygon.setPosition(spriteRectangle.x, spriteRectangle.y);
        for (final PolygonMapObject polygon : collisionLayer.getByType(PolygonMapObject.class)) {
            if (Intersector.overlapConvexPolygons(polygon.getPolygon(), playerPolygon)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Draw on the scene.
     *
     * @param batch the batch
     */
    public void draw(final SpriteBatch batch){
        deltaTime += Gdx.graphics.getDeltaTime();
        sprite.setRegion(animation.getKeyFrame(deltaTime, true));
        sprite.setOriginBasedPosition(getPosition().x, getPosition().y);
        sprite.draw(batch);
    }

    /**
     * Gets position.
     *
     * @return the position
     */
    public Vector2 getPosition() {
        return position;
    }

    /**
     * Sets position.
     *
     * @param position the position
     * @return the position
     */
    public GameObject setPosition(final Vector2 position) {
        this.position = position;
        return this;
    }

    /**
     * Set position.
     *
     * @param x the x
     * @param y the y
     */
    void setPosition(final float x, final float y){
        position.x = x;
        position.y = y;
        sprite.setOriginBasedPosition(x,y);
    }

    /**
     * Gets speed.
     *
     * @return the speed
     */
    public Float getSpeed() {
        return speed;
    }

    /**
     * Sets speed.
     *
     * @param speed the speed
     */
    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    /**
     * Dispose all textures.
     */
    public void dispose(){
        if (this.texture != null) this.texture.dispose();
        if (this.textureUp != null) this.textureUp.dispose();
        if (this.textureRight != null) this.textureRight.dispose();
        if (this.textureDown != null) this.textureDown.dispose();
        if (this.textureLeft != null) this.textureLeft.dispose();
        if (this.textureUpBorder != null) this.textureUpBorder.dispose();
        if (this.textureRightBorder != null) this.textureRightBorder.dispose();
        if (this.textureDownBorder != null) this.textureDownBorder.dispose();
        if (this.textureLeftBorder != null) this.textureLeftBorder.dispose();
    }

    /**
     * Get rotation float.
     *
     * @return the float
     */
    public Float getRotation(){
        return sprite.getRotation();
    }

    /**
     * Rotate.
     *
     * @param degrees the degrees
     */
    public void rotate(final Float degrees){
        if(sprite.getRotation() > 360.0f){
            sprite.setRotation(sprite.getRotation() - 360.0f);
        }
        sprite.rotate(degrees);
    }

    /**
     * Collides with object boolean.
     *
     * @param gameObject the game object
     * @return the boolean
     */
    Boolean collidesWithObject(final GameObject gameObject){
        if (this.sprite != null) {
            return Intersector.intersectRectangles(this.sprite.getBoundingRectangle(), gameObject.sprite.getBoundingRectangle(), new Rectangle()) || gameObject.sprite.getBoundingRectangle().contains(gameObject.sprite.getBoundingRectangle());
        }
        return Intersector.intersectRectangles(this.spriteUp.getBoundingRectangle(), gameObject.sprite.getBoundingRectangle(), new Rectangle()) || this.spriteUp.getBoundingRectangle().contains(gameObject.sprite.getBoundingRectangle()) ||
            Intersector.intersectRectangles(this.spriteRight.getBoundingRectangle(), gameObject.sprite.getBoundingRectangle(), new Rectangle()) || this.spriteRight.getBoundingRectangle().contains(gameObject.sprite.getBoundingRectangle()) ||
            Intersector.intersectRectangles(this.spriteDown.getBoundingRectangle(), gameObject.sprite.getBoundingRectangle(), new Rectangle()) || this.spriteDown.getBoundingRectangle().contains(gameObject.sprite.getBoundingRectangle()) ||
            Intersector.intersectRectangles(this.spriteLeft.getBoundingRectangle(), gameObject.sprite.getBoundingRectangle(), new Rectangle()) || this.spriteLeft.getBoundingRectangle().contains(gameObject.sprite.getBoundingRectangle());
    }

    /**
     * Is touched boolean.
     *
     * @return the boolean
     */
    boolean isTouched() {
        return touched;
    }

    /**
     * Sets touched.
     *
     * @param touched the touched
     */
    public void setTouched(final boolean touched) {
        this.touched = touched;
    }

    /**
     * Gets sprite.
     *
     * @return the sprite
     */
    public Sprite getSprite() {
        return sprite;
    }

    /**
     * Gets sprite rectangle.
     *
     * @return the sprite rectangle
     */
    public Rectangle getSpriteRectangle() {
        return spriteRectangle;
    }
}
