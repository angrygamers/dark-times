package pl.polsl.angrygamers.objects.gameObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.enums.Direction;
import pl.polsl.angrygamers.enums.EnemyMode;
import pl.polsl.angrygamers.utils.CircularArrayList;
import pl.polsl.angrygamers.utils.Size;
import pl.polsl.angrygamers.utils.TextureData;

/**
 * The type Ghost.
 */
public class Ghost extends Enemy {

    private static final double AMPLITUDE = 20.0;
    private static final double FREQUENCY = 0.1;

    private float spriteAlpha = 1f;
    private Integer sinX = 0;
    private Vector2 savedPlayerPosition = new Vector2();

    private double slopeFactor;
    private double plotOffset;
    private final ParticleEffect playerHitParticleEffect = new ParticleEffect();
    private final ParticleEffect destroyedGhostParticleEffect = new ParticleEffect();
    private boolean playerHit = false;
    private boolean dismissGhost = false;

    /**
     * Instantiates a new Ghost.
     *
     * @param size         the size
     * @param textureData  the texture data
     * @param position     the position
     * @param speed        the speed
     * @param movementPath the movement path
     */
    public Ghost(final Size size, final TextureData textureData, final Vector2 position, final Float speed, final CircularArrayList<Direction> movementPath) {
        super(size, textureData, position, speed, movementPath);
        enemyMode = EnemyMode.ANGRY;
        playerHitParticleEffect.load(Gdx.files.internal("particles/dismiss.particle"), Gdx.files.internal(""));
        playerHitParticleEffect.start();
        destroyedGhostParticleEffect.load(Gdx.files.internal("particles/destroy.particle"),Gdx.files.internal(""));
        destroyedGhostParticleEffect.start();
    }

    @Override
    protected void updateAngryMovement(final Player player) {
        if(collidesWithObject(player)){
            this.playerHit = true;
        }
        sineMovementInit(player.getPosition());
        if(isPlayerOnLeft(player.getPosition())){
            sprite.translateX(-speed);
        } else{
            sprite.translateX(speed);
        }

        final double countedSinY = (slopeFactor*sinX + AMPLITUDE*Math.sin(FREQUENCY*sinX) + plotOffset);
        sinX++;
        setPosition(sprite.getX() + sprite.getOriginX(), (float) countedSinY + sprite.getOriginY());
    }

    private void sineMovementInit(final Vector2 playerPosition){
        if(!playerPosition.equals(savedPlayerPosition)){
            if(isPlayerOnLeft(playerPosition)){
                slopeFactor = (getPosition().y - playerPosition.y) / (getPosition().x - playerPosition.x);
                plotOffset = Gdx.graphics.getHeight() - getPosition().y;
            } else {
                slopeFactor = ((playerPosition.y - getPosition().y) / (playerPosition.x - getPosition().x));
                plotOffset = getPosition().y;
            }
            sinX = 0;
            savedPlayerPosition = new Vector2(playerPosition);
        }
    }

    /**
     * Check is ghost dismissed boolean.
     *
     * @return the boolean
     */
    public boolean checkIsGhostDismissed(){
        return dismissGhost;
    }

    /**
     * Check is player attacked boolean.
     *
     * @return the boolean
     */
    public boolean checkIsPlayerAttacked(){
        return playerHit;
    }

    @Override
    public void draw(final SpriteBatch batch) {
        super.draw(batch);
        if(isTouched()){
            setTouched(updateParticleEffect(destroyedGhostParticleEffect, batch));
        } else if(playerHit){
            playerHit = updateParticleEffect(playerHitParticleEffect, batch);
        }
    }

    private boolean updateParticleEffect(final ParticleEffect particleEffect, final SpriteBatch batch){
        particleEffect.getEmitters().first().setPosition(getPosition().x, getPosition().y);
        particleEffect.update(Gdx.graphics.getDeltaTime());
        this.sprite.setAlpha(spriteAlpha);
        if(spriteAlpha >= 0f)
            // Wartość została dopasowana do efektu particle, tak, by zanikanie wyglądało najlepiej.
            spriteAlpha-=0.02f;
        particleEffect.draw(batch);
        if(particleEffect.isComplete()){
            particleEffect.reset();
            dismissGhost = true;
            dispose();
            return false;
        }
        return true;
    }
}
