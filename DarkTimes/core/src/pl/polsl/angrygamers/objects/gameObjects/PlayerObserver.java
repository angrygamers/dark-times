package pl.polsl.angrygamers.objects.gameObjects;

/**
 * The interface Player observer.
 */
public interface PlayerObserver {
    /**
     * Update.
     *
     * @param player the player
     */
    void update(final Player player);
}
