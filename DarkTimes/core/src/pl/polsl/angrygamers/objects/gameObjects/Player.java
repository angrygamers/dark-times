package pl.polsl.angrygamers.objects.gameObjects;

import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.enums.Direction;
import pl.polsl.angrygamers.utils.Size;
import pl.polsl.angrygamers.utils.TextureData;

import static pl.polsl.angrygamers.PlayerInput.PLAYER_INPUT_SESSION;

/**
 * The type Player.
 */
public class Player extends GameObject {

    private boolean moving;
    private Direction direction;

    private int health;
    private int strength;

    private boolean itemEquipped;


    /**
     * Instantiates a new Player.
     *
     * @param position    the position
     * @param speed       the speed
     * @param size        the size
     * @param textureData the texture data
     */
    public Player(final Vector2 position, Float speed, final Size size, TextureData textureData) {
        super(position, speed, size, textureData);

        this.moving = false;
        this.direction = null;
        PLAYER_INPUT_SESSION.setPlayer(this);
        health = 100;
        strength = 50;
        itemEquipped = false;
    }

    /**
     * Update.
     */
    public void update() {
        if (moving) {
            updateMovement(direction);
        }
    }

    /**
     * Is moving boolean.
     *
     * @return the boolean
     */
    public boolean isMoving() {
        return moving;
    }

    /**
     * Sets moving.
     *
     * @param moving the moving
     */
    public void setMoving(final boolean moving) {
        this.moving = moving;
    }

    /**
     * Gets direction.
     *
     * @return the direction
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * Sets direction.
     *
     * @param direction the direction
     */
    public void setDirection(final Direction direction) {
        this.direction = direction;
    }

    public GameObject setPosition(Vector2 position) {
        this.setPosition(position.x, position.y);
        return this;
    }

    /**
     * Gets health.
     *
     * @return the health
     */
    public int getHealth() {
        return health;
    }

    /**
     * Sets health.
     *
     * @param health the health
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * Gets strength.
     *
     * @return the strength
     */
    public int getStrength() {
        return strength;
    }

    /**
     * Sets strength.
     *
     * @param strength the strength
     */
    public void setStrength(int strength) {
        this.strength = strength;
    }

    /**
     * Is item equipped boolean.
     *
     * @return the boolean
     */
    public boolean isItemEquipped() {
        return itemEquipped;
    }

    /**
     * Sets item equipped.
     *
     * @param itemEquipped the item equipped
     */
    public void setItemEquipped(boolean itemEquipped) {
        this.itemEquipped = itemEquipped;
    }
}
