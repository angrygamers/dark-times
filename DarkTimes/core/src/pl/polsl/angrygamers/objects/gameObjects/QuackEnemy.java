package pl.polsl.angrygamers.objects.gameObjects;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.enums.EnemyMode;
import pl.polsl.angrygamers.utils.CircularArrayList;
import pl.polsl.angrygamers.utils.Size;
import pl.polsl.angrygamers.utils.TextureData;

/**
 * The type Quack enemy.
 */
public class QuackEnemy extends Enemy {

    /**
     * The Shape renderer.
     */
    final ShapeRenderer shapeRenderer = new ShapeRenderer();

    /**
     * Instantiates a new Quack enemy.
     *
     * @param position the position
     */
    public QuackEnemy(Vector2 position) {
        super(new Size(90f, 98.24f),
                new TextureData("textures/knige_attack.png", 4, 4, 0.5f),
                position,
                10.0f, new CircularArrayList<>());
        setEnemyMode(EnemyMode.ANGRY);
    }

    @Override
    Boolean collidesWithObject(GameObject gameObject) {
        return false;
    }
}
