package pl.polsl.angrygamers.objects.gameObjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.enums.Direction;
import pl.polsl.angrygamers.utils.CircularArrayList;
import pl.polsl.angrygamers.utils.Size;
import pl.polsl.angrygamers.utils.TextureData;

/**
 * The type Npc.
 */
public class NPC extends AutonomousGameObject implements PlayerObserver {

    private static final float PLAYER_INTERACTION_DISTANCE = 300.0f;
    private static final float ROTATION_STEP = 3.5f;
    private float targetRotation = 0.0f;

    /**
     * Instantiates a new Npc.
     *
     * @param position     the position
     * @param speed        the speed
     * @param size         the size
     * @param textureData  the texture data
     * @param movementPath the movement path
     */
    public NPC(Vector2 position, Float speed, Size size, TextureData textureData, CircularArrayList<Direction> movementPath) {
        super(position, speed, size, textureData, movementPath);
    }

    @Override
    public void update(Player player) {
        final float distance = player.getPosition().dst(this.getPosition());

        if(distance <= PLAYER_INTERACTION_DISTANCE) {
            final Vector2 rotationVector = player.getPosition().cpy().sub(this.getPosition());
            this.targetRotation = rotationVector.angle();
        }
        update();
    }

    @Override
    public void draw(SpriteBatch batch) {
        float rotationDiff = getRotationDiff();
        if(Math.abs(rotationDiff) > ROTATION_STEP) {
            if(rotationDiff > 0.0f) {
                sprite.setRotation(sprite.getRotation() + ROTATION_STEP);
            } else {
                sprite.setRotation(sprite.getRotation() - ROTATION_STEP);
            }
        }
        super.draw(batch);
    }

    private float getRotationDiff() {
        float clockwiseDiff = targetRotation - sprite.getRotation();
        float counterClockwiseDiff = targetRotation - (sprite.getRotation() + 360.0f);

        return Math.min(clockwiseDiff, counterClockwiseDiff);
    }

    /**
     * Update.
     */
    public void update(){
        updateAutoMovement();
    }
}
