package pl.polsl.angrygamers.objects.gameObjects;

import com.badlogic.gdx.math.Vector2;
import pl.polsl.angrygamers.enums.Direction;
import pl.polsl.angrygamers.enums.EnemyMode;
import pl.polsl.angrygamers.utils.CircularArrayList;
import pl.polsl.angrygamers.utils.Size;
import pl.polsl.angrygamers.utils.TextureData;

import java.util.List;

import static pl.polsl.angrygamers.PlayerInput.PLAYER_INPUT_SESSION;

/**
 * The type Enemy.
 */
public class Enemy extends AutonomousGameObject implements PlayerObserver {

    /**
     * The Enemy mode.
     */
    EnemyMode enemyMode = EnemyMode.NORMAL;
    private boolean playerHit = false;

    /**
     * Instantiates a new Enemy.
     *
     * @param size         the size
     * @param textureData  the texture data
     * @param position     the position
     * @param speed        the speed
     * @param movementPath the movement path
     */
    public Enemy(final Size size, final TextureData textureData, final Vector2 position, final Float speed, final CircularArrayList<Direction> movementPath) {
        super(position, speed, size, textureData, movementPath);
        PLAYER_INPUT_SESSION.setEnemy(this);
    }

    /**
     * Instantiates a new Enemy.
     *
     * @param size           the size
     * @param horizontalSize the horizontal size
     * @param verticalSize   the vertical size
     * @param textures       the textures
     * @param position       the position
     * @param speed          the speed
     * @param movementPath   the movement path
     */
    public Enemy(final Size size, final Size horizontalSize, final Size verticalSize, final List<TextureData> textures, final Vector2 position, final Float speed, final CircularArrayList<Direction> movementPath) {
        super(position, speed, size, horizontalSize, verticalSize, textures, movementPath);
        PLAYER_INPUT_SESSION.setEnemy(this);
    }

    @Override
    public void update(final Player player){
        switch (enemyMode) {
            case NORMAL:
                updateAutoMovement();
                break;
            case TERITORIAL_ANGRY:
                float distance = player.getPosition().dst(this.getPosition());
                if(distance > 250.0f) {
                    updateAutoMovement();
                } else {
                    updateAngryMovement(player);
                }
                break;
            case ANGRY:
                updateAngryMovement(player);
                break;
        }
    }

    /**
     * Update angry state movement.
     *
     * @param player the player
     */
    protected void updateAngryMovement(final Player player) {
       final Vector2 playerPosition = player.getPosition();

        this.playerHit = collidesWithObject(player);

        if(isPlayerOnLeft(playerPosition)){
            if(isPlayerLower(playerPosition)){
                currentDirection = Direction.DOWN_LEFT;
            }else if(isPlayerHigher(playerPosition)){
                currentDirection = Direction.UP_LEFT;
            }else{
                currentDirection = Direction.LEFT;
            }
        }else if(playerPosition.x > this.getPosition().x){
            if(isPlayerLower(playerPosition)){
                currentDirection = Direction.DOWN_RIGHT;
            }else if(isPlayerHigher(playerPosition)){
                currentDirection = Direction.UP_RIGHT;
            }else{
                currentDirection = Direction.RIGHT;
            }
        } else {
            if(isPlayerLower(playerPosition)){
                currentDirection = Direction.DOWN;
            }else if(isPlayerHigher(playerPosition)){
                currentDirection = Direction.UP;
            }else{
                return;
            }
        }
        updateMovement(currentDirection);
    }

    /**
     * Toggle enemy mode.
     */
    public void toggleMode(){
        enemyMode = enemyMode.equals(EnemyMode.NORMAL) ? EnemyMode.ANGRY : EnemyMode.NORMAL;
    }

    /**
     * Sets enemy mode.
     *
     * @param enemyMode the enemy mode
     * @return the enemy mode
     */
    public Enemy setEnemyMode(final EnemyMode enemyMode) {
        this.enemyMode = enemyMode;
        return this;
    }

    /**
     * Gets enemy mode.
     *
     * @return the enemy mode
     */
    public EnemyMode getEnemyMode() {
        return enemyMode;
    }

    /**
     * Is player hit boolean.
     *
     * @return the boolean
     */
    public boolean isPlayerHit() {
        return playerHit;
    }

    private Boolean isPlayerHigher(final Vector2 playerPosition){
        return playerPosition.y > this.getPosition().y;
    }

    private  Boolean isPlayerLower(final Vector2 playerPosition){
        return playerPosition.y < this.getPosition().y;
    }

    /**
     * Is player on left boolean.
     *
     * @param playerPosition the player position
     * @return the boolean
     */
    Boolean isPlayerOnLeft(final Vector2 playerPosition) {
        return playerPosition.x < this.getPosition().x;
    }

    /**
     * Is player on right boolean.
     *
     * @param playerPosition the player position
     * @return the boolean
     */
    Boolean isPlayerOnRight(final Vector2 playerPosition) {
        return playerPosition.x > this.getPosition().x;
    }
}
