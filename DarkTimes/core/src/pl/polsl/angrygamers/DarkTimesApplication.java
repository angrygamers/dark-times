package pl.polsl.angrygamers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import pl.polsl.angrygamers.enums.GameScreenEnum;
import pl.polsl.angrygamers.utils.ScreenManagerUtils;
import pl.polsl.angrygamers.utils.resources.ResourceManager;

import static pl.polsl.angrygamers.utils.GlobalVariables.PLAYER;
import static pl.polsl.angrygamers.utils.GlobalVariables.SPRITE_BATCH;

/**
 * The type Dark times application.
 */
public class DarkTimesApplication extends Game {

    /**
     * The constant APPLICATION.
     */
    public static DarkTimesApplication APPLICATION;

    // don't remove this, static init race condition
    private SpriteBatch batch;
    private ResourceManager resourceManager;
    private Music music;

    /**
     * Gets skin.
     *
     * @return the skin
     */
    public Skin getSkin() {
        return resourceManager.getSkin();
    }

    @Override
    public void create() {
        APPLICATION = this;
        batch = SPRITE_BATCH;
        music = Gdx.audio.newMusic(Gdx.files.internal("sounds/background_music.mp3"));
        music.play();
        music.setLooping(true);
        resourceManager = new ResourceManager();
        ScreenManagerUtils.init(this);
        setScreen(ScreenManagerUtils.getScreen(GameScreenEnum.MENU_SCREEN));
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void resize(final int width, final int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        PLAYER.dispose();
        SPRITE_BATCH.dispose();
    }
}