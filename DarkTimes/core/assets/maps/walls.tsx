<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="walls" tilewidth="64" tileheight="64" tilecount="9" columns="3">
 <image source="img/walls.png" width="192" height="192"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" x="-0.242424" y="-2.22045e-16" width="13.2424" height="64"/>
   <object id="3" x="12.4545" y="0.121212" width="51.5152" height="12.7273"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index">
   <object id="1" x="-0.0795455" y="0.0113636" width="13.0909" height="64.25"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index">
   <object id="1" x="-0.272727" y="0.181818" width="64.3182" height="3.68182"/>
  </objectgroup>
 </tile>
</tileset>
