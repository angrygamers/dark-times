<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="rocks" tilewidth="64" tileheight="64" tilecount="42" columns="2">
 <image source="img/rocks.png" width="128" height="1400"/>
 <tile id="34">
  <objectgroup draworder="index">
   <object id="1" x="34.1818" y="9.63636">
    <polygon points="0,0 -16.7273,9.63636 -17.6364,16.9091 -8,27.0909 2.90909,27.0909 14,20.3636 10.3636,15.6364 13.6364,12.9091 9.45455,2"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="35">
  <objectgroup draworder="index">
   <object id="1" x="35.6364" y="10.9091">
    <polygon points="0,0 -12.9091,3.27273 -18.3636,7.27273 -18.3636,14.9091 -15.8182,21.2727 -11.8182,26.9091 8.72727,23.0909 8,20 11.2727,19.4545 12.1818,8.72727 8.90909,1.63636 4.54545,-0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="36">
  <objectgroup draworder="index">
   <object id="1" x="27.8182" y="11.8182">
    <polygon points="0,0 -7.81818,4.90909 -9.63636,11.2727 -8.72727,18.7273 -3.09091,21.8182 6.54545,27.2727 10.9091,26.3636 12.1818,21.0909 16.5455,22.1818 20.5455,17.2727 18.9091,12.7273 21.4545,9.63636 18.5455,2.72727 9.63636,0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="37">
  <objectgroup draworder="index">
   <object id="1" x="23.6364" y="10.5455">
    <polygon points="0,0 -3.63636,8.54545 -7.63636,14.3636 -2.72727,21.2727 4.36364,25.8182 7.27273,28.7273 12,25.6364 15.6364,27.6364 20.7273,22.7273 24.5455,17.2727 23.2727,8.36364 12.3636,1.63636 7.45455,-1.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="38">
  <objectgroup draworder="index">
   <object id="1" x="27.4545" y="8">
    <polygon points="0,0 -9.63636,8.90909 -9.45455,18.3636 -3.81818,29.4545 2.18182,29.2727 7.09091,31.4545 13.4545,27.0909 17.6364,27.6364 20.3636,22.3636 20,16.9091 10.7273,3.45455 7.45455,0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="39">
  <objectgroup draworder="index">
   <object id="1" x="33.8182" y="8.18182">
    <polygon points="0,0 -11.6364,3.81818 -15.8182,12 -14.7273,24.9091 -9.09091,26.7273 -5.81818,32.1818 1.81818,30.7273 7.63636,29.8182 11.2727,24.5455 10.1818,8.72727 4,-0.181818"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
