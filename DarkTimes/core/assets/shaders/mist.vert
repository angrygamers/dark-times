//combined projection and view matrix
uniform mat4 u_projTrans;
uniform float u_outerRadius;

//"in" attributes from our SpriteBatch
attribute vec3 a_position;
attribute vec4 a_color;
attribute vec2 a_texCoord0;

//"out" varyings to our fragment shader
varying vec4 v_color;
varying vec2 v_texCoords;
varying float v_outerRadius;

void main() {
    v_color = a_color;
    v_texCoords = a_texCoord0;
    v_outerRadius = u_outerRadius;
    gl_Position = u_projTrans * vec4(a_position, 1.0);
}