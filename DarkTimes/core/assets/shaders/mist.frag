#ifdef GL_ES
precision mediump float;
#endif

//texture 0
uniform sampler2D u_texture;
uniform vec2 u_resolution;
uniform float u_hit;

//"in" attributes from our vertex shader
varying vec4 v_color;
varying vec2 v_texCoords;
varying float v_outerRadius;

const float softness = .30, intensity = .85;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords) * v_color;

    vec2 relativePosition = gl_FragCoord.xy / u_resolution - .5;
     relativePosition.x *= u_resolution.x / u_resolution.y;
    float len = length(relativePosition);
    float vignette = smoothstep(v_outerRadius, v_outerRadius - softness, len);
    float vignetteBlack = smoothstep(v_outerRadius, v_outerRadius - softness, len);

    // blue tint
    color.r = mix(color.r, color.r * vignette, intensity);
    color.g = mix(color.g, color.g * vignette, intensity);

    // red middle with u_hit intensity
    color.g = mix(color.g, color.g * (1 - vignette), u_hit);
    color.b = mix(color.b, color.b * (1 - vignette), u_hit);

    color.rgb = mix(color.rgb, color.rgb * vignetteBlack, intensity);

    gl_FragColor = color;
}