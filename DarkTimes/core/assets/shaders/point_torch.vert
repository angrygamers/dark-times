//combined projection and view matrix
uniform mat4 u_projTrans;

//"in" attributes from our SpriteBatch
attribute vec4 a_position;
attribute vec4 a_color;
attribute vec2 a_texCoord0;

uniform vec2 u_position;
uniform float u_outerRadius;

//"out" varyings to our fragment shader
varying vec4 v_color;
varying vec2 v_texCoords;
varying float v_outerRadius;
varying vec2 v_position;

void main() {
    v_color = a_color;
    v_texCoords = a_texCoord0;
    v_outerRadius = u_outerRadius;
    v_position = u_position;
    gl_Position = u_projTrans * a_position;
}