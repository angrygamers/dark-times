#ifdef GL_ES
precision mediump float;
#endif

//texture 0
uniform sampler2D u_texture;
uniform vec2 u_resolution;

//"in" attributes from our vertex shader
varying vec4 v_color;
varying vec2 v_texCoords;
varying float v_outerRadius;
varying vec2 v_position;

const float softness = .15, intensity = .90;

void main() {

    vec4 color = texture2D(u_texture, v_texCoords) * v_color;
    vec2 position = v_position;

    vec2 st = gl_FragCoord.xy / u_resolution;
    vec2 dist = st - position;
    dist.x *= u_resolution.x / u_resolution.y;

    float len = length(dist);
    float vignetteBlack = smoothstep(v_outerRadius, v_outerRadius - softness, len);

    color.rgb = mix(color.rgb, color.rgb * vignetteBlack, intensity);

    gl_FragColor = color;
}